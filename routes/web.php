<?php

Auth::routes();
Route::get('/', 'HomeController@index')->name('home');

Route::group(['middleware' => ['auth']], function() {
   Route::resource('roles','RoleController');
   Route::resource('users','UserController');
   Route::resource('products','ProductController');
});
//Rotas Resource :: CRUD 
// -- AUTENTICACAO
Route::resource('usuarios','UsuariosController')->middleware(['auth']); 
Route::get('usuarios_pesquisar','UsuariosController@pesquisar')->name('usuarios.pesquisar')->middleware(['auth']);
Route::post('usuarios_pesquisar','UsuariosController@pesquisar')->name('usuarios.pesquisar')->middleware(['auth']);

// -- TABELA DE FATURAMENTO
Route::resource('tabelafaturamento','AgendamentoController')->middleware(['auth']);  
    //-- Especialidades
Route::resource('especialidades','EspecialidadeController')->middleware(['auth']);
Route::get('especialidades_pesquisar','EspecialidadeController@pesquisar')->name('especialidades.pesquisar')->middleware(['auth']);
Route::post('especialidades_pesquisar','EspecialidadeController@pesquisar')->name('especialidades.pesquisar')->middleware(['auth']);
Route::get('especialidades_pdf','EspecialidadeController@pdf')->name('especialidades.pdf')->middleware(['auth']);


//-- Categorias
Route::resource('categorias','CategoriaController')->middleware(['auth']);
Route::get('categorias_pesquisar','CategoriaController@pesquisar')->name('categorias.pesquisar')->middleware(['auth']);
Route::post('categorias_pesquisar','CategoriaController@pesquisar')->name('categorias.pesquisar')->middleware(['auth']);
Route::get('categorias_pdf','CategoriaController@pdf')->name('categorias.pdf')->middleware(['auth']);


   //-- Unidades
Route::resource('unidades','UnidadeController')->middleware(['auth']);
Route::get('unidades_pesquisar','UnidadeController@pesquisar')->name('unidades.pesquisar')->middleware(['auth']);
Route::post('unidades_pesquisar','UnidadeController@pesquisar')->name('unidades.pesquisar')->middleware(['auth']);
Route::get('unidades_pdf','UnidadeController@pdf')->name('unidades.pdf')->middleware(['auth']);

   //-- Convenios
Route::resource('convenios','ConvenioController')->middleware(['auth']);
Route::get('convenios_pesquisar','ConvenioController@pesquisar')->name('convenios.pesquisar')->middleware(['auth']);
Route::post('convenios_pesquisar','ConvenioController@pesquisar')->name('convenios.pesquisar')->middleware(['auth']);
Route::get('convenios_pdf','ConvenioController@pdf')->name('convenios.pdf')->middleware(['auth']);
   
   //-- Conselhos
Route::resource('conselhos','ConselhoController')->middleware(['auth']);
Route::get('conselhos_pesquisar','ConselhoController@pesquisar')->name('conselhos.pesquisar')->middleware(['auth']);
Route::post('conselhos_pesquisar','ConselhoController@pesquisar')->name('conselhos.pesquisar')->middleware(['auth']);
Route::get('conselhos_pdf','ConselhoController@pdf')->name('conselhos.pdf')->middleware(['auth']);
   
   //-- Prestadores
Route::resource('prestadores','PrestadorController')->middleware(['auth']);
Route::get('prestadores_pesquisar','PrestadorController@pesquisar')->name('prestadores.pesquisar')->middleware(['auth']);
Route::post('prestadores_pesquisar','PrestadorController@pesquisar')->name('prestadores.pesquisar')->middleware(['auth']);
Route::get('prestadores_pdf','PrestadorController@pdf')->name('prestadores.pdf')->middleware(['auth']);

   //-- Pacientes 
   Route::get('todospacientes','PacienteController@todospacientes')->middleware(['auth']);
   Route::post('novopaciente/salvar','PacienteController@novopaciente')->middleware(['auth']);
   Route::resource('pacientes','PacienteController')->middleware(['auth']);
   Route::get('pacientes_pesquisar','PacienteController@pesquisar')->name('pacientes.pesquisar')->middleware(['auth']);
   Route::get('checarpaciente/{nome?}','PacienteController@checarpaciente')->middleware(['auth']);
   Route::post('pacientes_pesquisar','PacienteController@pesquisar')->name('pacientes.pesquisar')->middleware(['auth']);
   Route::get('pacientes_pdf','PacienteController@pdf')->name('pacientes.pdf')->middleware(['auth']);
   Route::get('listarpacienteconvenios/{paciente}','PacienteController@listarpacienteconvenios')->name('listarpacienteconvenios')->middleware(['auth']);
   Route::get('removerpacienteconvenios/{paciente}/convenio/{convenio}','PacienteController@removerpacienteconvenios')->name('removerpacienteconvenios')->middleware(['auth']);
   Route::get('adicionarpacienteconvenios/{paciente}/convenio/{convenio}','PacienteController@adicionarpacienteconvenios')->name('adicionarpacienteconvenios')->middleware(['auth']);

//-- Situação Agenda
Route::resource('situacaoagenda','SituacaoAgendaController')->middleware(['auth']);
Route::get('situacaoagenda_pesquisar','SituacaoAgendaController@pesquisar')->name('situacaoagenda.pesquisar')->middleware(['auth']);
Route::post('situacaoagenda_pesquisar','SituacaoAgendaController@pesquisar')->name('situacaoagenda.pesquisar')->middleware(['auth']);
Route::get('situacaoagenda_pdf','SituacaoAgendaController@pdf')->name('situacaoagenda.pdf')->middleware(['auth']);

//-- Tipo Bloqueio
Route::resource('tipobloqueio','TipoBloqueioController')->middleware(['auth']);
Route::get('tipobloqueio_pesquisar','TipoBloqueioController@pesquisar')->name('tipobloqueio.pesquisar')->middleware(['auth']);
Route::post('tipobloqueio_pesquisar','TipoBloqueioController@pesquisar')->name('tipobloqueio.pesquisar')->middleware(['auth']);
Route::get('tipobloqueio_pdf','TipoBloqueioController@pdf')->name('tipobloqueio.pdf')->middleware(['auth']);

//-- Tipo Agenda
Route::resource('tipoagenda','TipoAgendaController')->middleware(['auth']);
Route::get('tipoagenda_pesquisar','TipoAgendaController@pesquisar')->name('tipoagenda.pesquisar')->middleware(['auth']);
Route::post('tipoagenda_pesquisar','TipoAgendaController@pesquisar')->name('tipoagenda.pesquisar')->middleware(['auth']);
Route::get('tipoagenda_pdf','TipoAgendaController@pdf')->name('tipoagenda.pdf');

//-- Tipo Atendimento
Route::resource('tipoatendimento','TipoAtendimentoController')->middleware(['auth']);
Route::get('tipoatendimento_pesquisar','TipoAtendimentoController@pesquisar')->name('tipoatendimento.pesquisar')->middleware(['auth']);
Route::post('tipoatendimento_pesquisar','TipoAtendimentoController@pesquisar')->name('tipoatendimento.pesquisar')->middleware(['auth']);
Route::get('tipoatendimento_pdf','TipoAtendimentoController@pdf')->name('tipoatendimento.pdf')->middleware(['auth']);

//-- Carater de Atendimento
Route::resource('carateratendimento','CaraterAtendimentoController')->middleware(['auth']);
Route::get('carateratendimento_pesquisar','CaraterAtendimentoController@pesquisar')->name('carateratendimento.pesquisar')->middleware(['auth']);
Route::post('carateratendimento_pesquisar','CaraterAtendimentoController@pesquisar')->name('carateratendimento.pesquisar')->middleware(['auth']);
Route::get('carateratendimento_pdf','CaraterAtendimentoController@pdf')->name('carateratendimento.pdf')->middleware(['auth']);

//-- Indicacao de Acidente
Route::resource('indicacaoacidente','IndicacaoAcidenteController')->middleware(['auth']);
Route::get('indicacaoacidente_pesquisar','IndicacaoAcidenteController@pesquisar')->name('indicacaoacidente.pesquisar')->middleware(['auth']);
Route::post('indicacaoacidente_pesquisar','IndicacaoAcidenteController@pesquisar')->name('indicacaoacidente.pesquisar')->middleware(['auth']);
Route::get('indicacaoacidente_pdf','IndicacaoAcidenteController@pdf')->name('indicacaoacidente.pdf')->middleware(['auth']);

//-- Tipo de Consulta
Route::resource('tipoconsulta','TipoConsultaController')->middleware(['auth']);
Route::get('tipoconsulta_pesquisar','TipoConsultaController@pesquisar')->name('tipoconsulta.pesquisar')->middleware(['auth']);
Route::post('tipoconsulta_pesquisar','TipoConsultaController@pesquisar')->name('tipoconsulta.pesquisar')->middleware(['auth']);
Route::get('tipoconsulta_pdf','TipoConsultaController@pdf')->name('tipoconsulta.pdf')->middleware(['auth']);

//-- FIM CRUD

//FUNCIONALIDADES 
  //-- ALOCACAO DE PRESTADORES
Route::resource('alocacao','AlocacaoController')->middleware(['auth']);
Route::post('alocacaodel', 'AlocacaoController@alocacaoDel')->name('alocacaodel')->middleware(['auth']);
Route::post('alocacaopost', 'AlocacaoController@alocacaoPost')->name('alocacaopost')->middleware(['auth']);
Route::get('listarprestadores/{prestador}/unidade/{unidade}','AlocacaoController@listarprestadores')->name('listarprestadores')->middleware(['auth']);
Route::get('listarespecialidade/{unidade}/prestador/{prestador}','AlocacaoController@listarespecialidade')->name('listarespecialidade')->middleware(['auth']);

  // -- GERAR EXPEDIENTE DO PRESTADOR
Route::resource('expediente','ExpedienteController')->middleware(['auth']); 
Route::get('listarprestador/{unidade}','ExpedienteController@listarprestador')->name('listarprestadores')->middleware(['auth']);
Route::get('listarexpediente/{unidade}/prestador/{prestador}/especialidade/{especialidade}','ExpedienteController@listarexpediente')->name('listarexpediente')->middleware(['auth']);
Route::post('expedientepost', 'ExpedienteController@expedientepost')->name('expedientepost')->middleware(['auth']);
Route::post('expedientedel', 'ExpedienteController@expedientedel')->name('expedientedel')->middleware(['auth']);
Route::get('agenda/generator/{dtinicio}/{dtfinal}/{intervalo}/{semana}','ExpedienteController@agendagenerator')->name('agendagenerator')->middleware(['auth']);

  // -- AGENDAMENTO
Route::resource('agenda','AgendamentoController')->middleware(['auth']);  
Route::get('listaragendaespecialidade/{unidade}','AgendamentoController@listaragendaespecialidade')->name('listaragendaespecialidade')->middleware(['auth']);
Route::get('listaragendaprestador/{unidade}/especialidade/{especialidade}','AgendamentoController@listaragendaprestador')->name('listaragendaprestador')->middleware(['auth']);
Route::get('listarcalendario/{unidade}/prestador/{prestador}/especialidade/{especialidade}','AgendamentoController@listarcalendario')->name('listarcalendario')->middleware(['auth']);
Route::get('buscardadosagenda/{unidade}/especialidade/{especialidade}/prestador/{prestador}/data/{data}','AgendamentoController@buscardadosagenda')->name('buscardadosagenda')->middleware(['auth']);
Route::get('acoeshorarios/limpar/{id}', 'AgendamentoController@limpar')->name('limpar')->middleware(['auth']);
Route::get('acoeshorarios/bloquear/{id}', 'AgendamentoController@bloquear')->name('bloquear')->middleware(['auth']);
Route::get('acoeshorarios/confirmar/{id}', 'AgendamentoController@confirmar')->name('confirmar')->middleware(['auth']);
Route::get('acoeshorarios/agendar/{id}/paciente/{paciente}/tipo/{tipo}', 'AgendamentoController@agendar')->name('agendar')->middleware(['auth']);
Route::post('ausentaragenda', 'AgendamentoController@ausentaragenda')->name('ausentaragenda')->middleware(['auth']);
Route::post('encaixe', 'AgendamentoController@encaixe')->name('encaixe')->middleware(['auth']);

// -- GERAR ATENDIMENTO
// -- JSON
Route::get('consulta/tipoatendimento', 'AtendimentoController@tipoatendimento')->name('tipoatendimento')->middleware(['auth']);
Route::get('consulta/agenda/{id}', 'AtendimentoController@consultaagenda')->name('consultaagenda')->middleware(['auth']);
Route::get('consulta/convenio/{id}', 'AtendimentoController@convenio')->name('convenio')->middleware(['auth']);
Route::get('consulta/procedimentos/agenda/{agenda}/convenio/{convenio}', 'AtendimentoController@procedimentos')->name('procedimentos')->middleware(['auth']);
Route::post('atenderpaciente', 'AtendimentoController@atenderpaciente')->name('atenderpaciente')->middleware(['auth']);

// -- LISTA DE ESPERA ATENDIMENTO
Route::resource('listaespera','ListaEsperaController')->middleware(['auth']); 
Route::post('listaespera_pesquisar', 'ListaEsperaController@pesquisar')->name('listaespera.pesquisar')->middleware(['auth']);

// -- PRONTUARIO
Route::post('atestadopdf','ProntuarioController@atestadopdf')->middleware(['auth']);
Route::get('pedidoexamepdf/{idexame}','ProntuarioController@pedidoexamepdf')->middleware(['auth']);
Route::get('atendimento/listaexames/{idpaciente}', 'ProntuarioController@listaexames')->middleware(['auth']);
Route::post('atendimento/salvarexames', 'ProntuarioController@salvarexames')->middleware(['auth']);
Route::get('atendimento/exames/{idtipoexames}', 'ProntuarioController@getexames')->middleware(['auth']);
Route::get('atendimento/diagnostico/{idpaciente}', 'ProntuarioController@getdiagnostico')->middleware(['auth']);
Route::post('diagnostico/salvar', 'ProntuarioController@salvardiagnostico')->middleware(['auth']);
Route::post('evolucaos/salvar', 'ProntuarioController@salvarevolucaos')->middleware(['auth']);
Route::get('atendimento/evolucaos/{idpaciente}', 'ProntuarioController@getevolucaos')->middleware(['auth']);
Route::post('anamnese/salvar', 'ProntuarioController@salvaranamnese')->middleware(['auth']);
Route::get('atendimento/anamnese/{idpaciente}', 'ProntuarioController@getanamnese')->middleware(['auth']);
Route::post('atendimento/iniciar', 'ProntuarioController@iniciar')->middleware(['auth']);
Route::post('atendimento/finalizar', 'ProntuarioController@finalizar')->middleware(['auth']);
Route::get('prontuario/{idPaciente?}', 'ProntuarioController@index')->middleware(['auth']);
Route::get('prontuario/atendimento/{idPaciente}', 'ProntuarioController@atendimentos')->middleware(['auth']);
Route::get('prontuario/dados/{idPaciente}', 'ProntuarioController@dados')->middleware(['auth']);
Route::resource('prontuario','ProntuarioController')->middleware(['auth']); 

