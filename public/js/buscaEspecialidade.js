$(document).ready(function(){
    $('#spinner').hide();
    $('#prestador').select2();
    $('#especialidade').select2();
    $('#prestador').change(function(){
      pesquisar();
      });
    $('#unidade').change(function(){
    pesquisar();
    });
   
}); 
$.ajaxSetup({
  headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});
$(".btn-submit").click(function(e){
    e.preventDefault();
    var prestador = $("#prestador option:selected").val();
    var unidade = $("#unidade option:selected").val();
    var especialidade = $("#especialidade option:selected").val();

    $.ajax({
      type:'POST',
      url:'/alocacaopost',
      data:{prestador:prestador, unidade:unidade, especialidade:especialidade},
      success:function(data){
        pesquisar();
      }   
  });
});
 // INICIO DAS FUNÇÕES
 // -- Pesquisar Especialidade
function pesquisar() {
    $('#spinner').show();
    $("thead tr td").remove();
      var prestador = $("#prestador").val();
      var unidade = $("#unidade").val();
        $.get("/listarprestadores/"+ prestador + "/unidade/" + unidade,function(data){
              var tam = data.length;
              for (i=0; i<tam; i++) {
              $('#tabelaEspecialidade').find('thead')
              .append("<tr><td>" + (data[i].nome) + "</td><td><a id='btnExcluir' onclick='deletarEspecialidade(" + data[i].id + ")' class='glyphicon glyphicon-trash'></td></tr>");
              }
      }).always(function(){
        $('#spinner').hide(); 
      });
  }
  
  function modalErro(error){
    $("#mensagem").text(error);
        $('#modal-erro').modal('show');
      setTimeout(function() {
          $('#modal-erro').modal('hide');
      }, 5000);
}

  // -- Deletar Especialidade
  function deletarEspecialidade(id) {
  $.ajax({
    type:'POST',
    url:'/alocacaodel',
    data:{alocacao:id},
    success:function(data){
      pesquisar();
    },error: function (xhr) {
      var errors = xhr.responseJSON;
      var error =  errors.warning;
      modalErro(error);
  }      
 });
  }



