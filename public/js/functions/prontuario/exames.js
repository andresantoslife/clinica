function exames() {
    esconderDiv();
    $('#exames').show(600);
    $('#titulo').html('<b>Exames</b>');
    listaExames();
   
  }

function pegaExames() {
    $("#tipoExames").change(function() {
        $('#Exames').empty();
         var idtipoExames = $("#tipoExames option:selected").val();
         $.get("/atendimento/exames/"+ idtipoExames,function(data){
            if(data.length > 0){
               var tam = data.length; 
               for (i=0; i<tam; i++) {  
                   $('#Exames')
                   .append("<option value='" + data[i].id + "'>" + data[i].nome + "</option>"); 
               } 
           }
          }); 
         
       });
}

function listaExames() {
     $("thead tr td").remove();
      // Função GET
      var idPaciente = $("#paciente option:selected").val();
      $.get("/atendimento/listaexames/"+ idPaciente,function(data){
        if(data.length > 0){ 
            var tam = data.length; 
            for (i=0; i<tam; i++) {  
           $('#tabelaExames').find('thead')
           .append("<tr><td width='100px'>" + data[i].data + "</td><td width='200px'>" + data[i].prestador + "</td><td>" + data[i].exame + "</td> <td width='50px'><a href='pedidoexamepdf/" + data[i].id + "' target='_blank' class='glyphicon glyphicon-print printExame'></a></td></tr>");
            } 
        }
        else {
            $('#tabelaExames').find('thead')
           .append("<tr><td> - </td><td> - </td><td> - </td></tr>");
            } 
     }); 
      // Fim GET
}


function exameSalvar() {
    $("#formExames").submit(function(event) {
        event.preventDefault();
          //INICIAR
          var idAtendimento = window.sessionStorage.getItem('idAtendimento');
          var exames_id = $("#Exames option:selected").val();
          var dataExame = $("#dataExame").val();
          var observacaoExame = $("#observacaoExame").val();
          var idPaciente = $("#paciente option:selected").val();
          $.ajax({
            type:'POST',
            url:'/atendimento/salvarexames', 
            data:{
              idAtendimento: idAtendimento,
              exames_id: exames_id,
              dataExame: dataExame,
              observacaoExame: observacaoExame,
              idPaciente: idPaciente
              },success:function(){
                 $("#observacaoExame").val('');
                 exames();  
            }   
         });
      });
    }