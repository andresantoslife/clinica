function anamnese() {
    esconderDiv();
    $('#anamnese').show(600);
    $('#titulo').html('<b>Anamnese</b>');
    $("#anamneseLimpar").click(function(event) {
      $('#anamneseForm').val('');
    });
      // Função GET
    $('#anamneseHist').empty();
    var idPaciente = $("#paciente option:selected").val();
      $.get("/atendimento/anamnese/"+ idPaciente,function(data){
        if(data.length > 0){
                var tam = data.length; 
                for (i=0; i<tam; i++) {  
                $('#anamneseHist').append((data[i].descricao)+'\n');      
            }  
        } 
    }); 
      // Fim GET
  }

  function anamneseSalvar() {
    $("#formanamnese").submit(function(event) {
      event.preventDefault();
      if ($("#anamneseSalvar").hasClass("disabled")) {
        /* alert('Você não pode Finalizar'); */
      } else {
        //INICIAR
        var idAtendimento = window.sessionStorage.getItem('idAtendimento');
        var anamneseForm = $('#anamneseForm').val();
        $.ajax({
          type:'POST',
          url:'/anamnese/salvar', 
          data:{
            idAtendimento : idAtendimento,
            anamneseForm : anamneseForm
            },success:function(data){
              //Atualizar Historico
              anamnese();
          }   
        }); 
              //FIM INICIAR
      }
    });
  }