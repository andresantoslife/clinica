$(document).ready(function(){
  Start();
});
$.ajaxSetup({
    headers: {

        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

    }
});
//Funcao Principal
function Start() {
  esconderDiv();
  pegaExames();
  anamneseSalvar();
  evolucaosSalvar();
  diagnosticoSalvar(); 
  exameSalvar();
  pegadadosPaciente();
  pegadadosAtendimento();
  selectAtestados();
  iniciar();
  finalizar();
 
}
//Fim da funcao

$('#paciente').change(function(){
    pegadadosPaciente();
    pegadadosAtendimento(); 
});

function pegadadosAtendimento() {
 var idPaciente = $("#paciente option:selected").val();
 $.get("/prontuario/atendimento/"+ idPaciente,function(data){
    if(data.length > 0){
        //Setar Session caso Tenho atendimento em aberto
        window.sessionStorage.setItem('idAtendimento', data[0].id);
        window.sessionStorage.setItem('status', data[0].status);
        window.sessionStorage.setItem('inicioatendimento', data[0].inicioatendimento);
        var relogio =  window.sessionStorage.getItem('inicioatendimento');
        
        //Existe atendimento 
        //Verificar Aberto e Aguardando 
        switch (window.sessionStorage.getItem('status')) {
          case 'aberto':
              habilitartodosComponentes();
              $('#iniciar').addClass('disabled');
              $('#atendimento').text('Atendimento em Aberto');
              cronometro(relogio, 'countup1');
              anamnese();
              
          break;

          case 'aguardando':
            desabilitartodosComponentes();
            $('#iniciar').removeClass('disabled');
            $('#atendimento').text('Aguardando Atendimento');
            clearTimeout(cronometro.interval);
            anamnese();
           
           break;

           case 'finalizado':
             desabilitartodosComponentes();
             $('#atendimento').text('Atendimento Finalizado');
             clearTimeout(cronometro.interval); 
             anamnese();
         
           break;
        }

    } else { //sem atendimento 
        window.sessionStorage.setItem('idAtendimento', null);
        window.sessionStorage.setItem('status', 'Não Existe Atendimento');
        $('#atendimento').text(window.sessionStorage.getItem('status'));
        clearTimeout(cronometro.interval);
        desabilitartodosComponentes();
        anamnese();
        

    }
}); 
}
function pegadadosPaciente(){
        var idPaciente = $("#paciente option:selected").val();
        $.get("/prontuario/dados/"+ idPaciente,function(data){
                if(typeof data !== 'undefined'){
                    //Setar Dados
                    $('#sexo').text(data[0].sexo);  
                    $('#nascimento').text(data[0].dtnascimento);
                } 
            }); 
}


function receituario() {
    esconderDiv();
    $('#receituario').show(600);
    $('#titulo').html('<b>Receituário</b>');
  }

  
function esconderDiv() {
    $('#anamnese').hide();
    $('#diagnostico').hide();
    $('#exames').hide();
    $('#receituario').hide();
    $('#evolucoes').hide();
    $('#atestado').hide();
}

function desabilitartodosComponentes() {
  $('#iniciar').addClass('disabled');
  $('#finalizar').addClass('disabled');
  $('.salvar').addClass('disabled');
  $('.limpar').addClass('disabled');
  $("#anamneseForm").attr("disabled", "disabled");
  $("#evolucoesForm").attr("disabled", "disabled");
  $('#formularioCid').hide();
  $('#divExames').hide();
  $('#formAtestado').hide();
  $('#atestado').hide();
  $('#comparecimentoPaciente').hide();
  $('#opcoesAtestado').hide();
}
function habilitartodosComponentes() {
  $('#opcoesAtestado').show();
  $('#comparecimentoPaciente').show();
  $('#show').hide();
  $('#formAtestado').show();
  $('#divExames').show();
  $('#iniciar').removeClass('disabled');
  $('#finalizar').removeClass('disabled');
  $('.salvar').removeClass('disabled');
  $('.limpar').removeClass('disabled');
  $("#anamneseForm").removeAttr("disabled", "disabled");
  $("#evolucoesForm").removeAttr("disabled", "disabled");
  $('#formularioCid').show();
}