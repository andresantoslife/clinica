function evolucaos() {
    esconderDiv();
    $('#evolucoes').show(600);
    $('#titulo').html('<b>Evoluções</b>');
    $("#evolucaosLimpar").click(function(event) {
        $('#evolucoesForm').val('');
    });
      // Função GET
    $('#evolucaosHist').empty();
    var idPaciente = $("#paciente option:selected").val();
      $.get("/atendimento/evolucaos/"+ idPaciente,function(data){
        if(data.length > 0){
                var tam = data.length; 
                for (i=0; i<tam; i++) {  
                $('#evolucaosHist').append((data[i].descricao)+'\n');      
            }  
        } 
    }); 
      // Fim GET
  }

  function evolucaosSalvar() {
     $("#formevolucoes").submit(function(event) {
      event.preventDefault();
      if ($("#evolucaosSalvar").hasClass("disabled")) {
        /* alert('Você não pode Finalizar'); */
      } else {
        //INICIAR
        var idAtendimento = window.sessionStorage.getItem('idAtendimento');
        var evolucaosForm = $('#evolucoesForm').val();
        $.ajax({
         type:'POST', 
          url:'/evolucaos/salvar',
          data:{
            idAtendimento: idAtendimento,
            evolucaosForm: evolucaosForm
            },success:function(data){
              //Atualizar Historico 
              evolucaos();
          }   
        }); 
              //FIM INICIAR
      }
    });
  }