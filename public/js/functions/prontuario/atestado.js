function atestado() {
    esconderDiv();
    trocaOpcoes();
    $('#atestado').show(600);
    $('#titulo').html('<b>Atestados</b>');
    var nomePaciente = $("#paciente option:selected").text();
    var idAtendimento = window.sessionStorage.getItem('idAtendimento');
    $('#atendimentoID').val(idAtendimento);
    $('#atendimentoCompID').val(idAtendimento);
    $('#pacienteAtestado').text('Sr(a) ' + nomePaciente);
    $("#imprimirAtestado").click(function(event) {
        event.preventDefault();
      });
    
  }

  function selectAtestados() {
    $("#opcoesAtestado").change(function(event) {
        trocaOpcoes();
      });
  }

  function trocaOpcoes(){
    var opcoesAtestado = $("#opcoesAtestado option:selected").val();
    opcoesAtestado == 1 ? $("#atestadoPaciente").show(500) : $("#atestadoPaciente").hide();
    opcoesAtestado == 2 ? $("#comparecimentoPaciente").show(500) : $("#comparecimentoPaciente").hide();
  }

  