function diagnostico() {
    esconderDiv();
    $('#diagnostico').show(600);
    $('#titulo').html('<b>Diagnóstico</b>');
    $("thead tr td").remove();
      // Função GET
    var idPaciente = $("#paciente option:selected").val();
      $.get("/atendimento/diagnostico/"+ idPaciente,function(data){
        if(data.length > 0){ 
            var tam = data.length; 
            for (i=0; i<tam; i++) {  
           $('#tabelaCID').find('thead')
           .append("<tr><td>" + data[i].codigo + "</td><td>" + data[i].descricao + "</td></tr>");
            } 
        }
        else {
            $('#tabelaCID').find('thead')
           .append("<tr><td> - </td><td> - </td></tr>");
            } 
     }); 
      // Fim GET
  }

  function diagnosticoSalvar() {
    $("#cidAdicionar").click(function(event) {
      if ($("#cidAdicionar").hasClass("disabled")) {
        /* alert('Você não pode Finalizar'); */
      } else {
        //INICIAR
        var idAtendimento = window.sessionStorage.getItem('idAtendimento');
        var cid = $("#cid option:selected").val();
        $.ajax({
         type:'POST', 
          url:'/diagnostico/salvar',
          data:{
            idAtendimento: idAtendimento,
            cid: cid
            },success:function(data){
              //Atualizar Historico
              diagnostico();
          }   
        }); 
              //FIM INICIAR
      }
    });
  }