 $(function() {
  agendamentoModal();
  ausentarModal();
  atenderModal();
    $.contextMenu({
        selector: '.btnAtender',
        trigger: 'left',
        callback: function(key, options) {
            var agendaID = $(this).val();
            var AgendaAcao = key; 
            acoesAgenda(agendaID, AgendaAcao);
        },
        items: {
            "Atender": {name: "Atender Paciente", icon: "fas fa-notes-medical"},
            "Agendar": {name: "Agendar Paciente", icon: "fas fa-calendar-alt"},
            "Confirmar": {name: "Confirmar Horário", icon: "far fa-check-square"},
            "Ausentar": {name: "Ausentar Paciente", icon: "fas fa-running"},
            "Bloquear": {name: "Bloquear Horário", icon: "fas fa-lock"},
            "Liberar": {name: "Liberar Horário", icon: "fas fa-remove-format"}
        }
    }); 
});


function acoesAgenda(idAgenda, acaoAgenda){
   //console.log(idAgenda, acaoAgenda); Debugar
        switch(acaoAgenda) {
            case 'Atender': 
                atenderPaciente(idAgenda); //atender.js
            break

            case 'Agendar': 
                agendarHorario(idAgenda);
            break

            case 'Confirmar': 
               confirmarHorario(idAgenda);
            break
            
            case 'Ausentar': 
                ausentarHorario(idAgenda);
            break

            case 'Bloquear': 
                bloquearHorario(idAgenda);
            break

            case 'Liberar': 
                limparHorario(idAgenda);
            break

        }
}

function agendarHorario(id) {
//Passando Id para Modal
 $('#idAgendaModal').val(id);
 $('#modal-agendar').modal('show')
}

function ausentarHorario(id) {
 $('#agendas_id').val(id);
 $('#modal-ausentar').modal('show')
}

function confirmarHorario(id) {
    $.get("/acoeshorarios/confirmar/"+ id,function(){
        var dataAgenda = window.sessionStorage.getItem('dtAgenda');
        buscarAgenda(dataAgenda);   
        carregarCalendario(dataAgenda);
    })
  }

function bloquearHorario(id) {
    $.get("/acoeshorarios/bloquear/"+ id,function(){
        var dataAgenda = window.sessionStorage.getItem('dtAgenda');
        buscarAgenda(dataAgenda);   
        carregarCalendario(dataAgenda);
    })
  }

function limparHorario(id) {
    $.get("/acoeshorarios/limpar/"+ id,function(){
        var dataAgenda = window.sessionStorage.getItem('dtAgenda');
        buscarAgenda(dataAgenda);
        carregarCalendario(dataAgenda);
    })
  }

function agendamentoModal(){
    $("#buttonModalAgendar").click(function(e){
    e.preventDefault();
    var tipo = $("#selectTipoConsulta option:selected").val();
    var paciente = $("#selectPaciente option:selected").val();
    var idAgendaModal = $("#idAgendaModal").val();
    console.log(idAgendaModal);
    $.get("/acoeshorarios/agendar/"+ idAgendaModal + "/paciente/" + paciente + "/tipo/" + tipo,function(){
        var dataAgenda = window.sessionStorage.getItem('dtAgenda');
        buscarAgenda(dataAgenda);   
        carregarCalendario(dataAgenda);  
        $('#modal-agendar').modal('hide');
        })   
     })
}

function ausentarModal(){
    $("#buttonModalAusentar").click(function(e){
    e.preventDefault();
    var agendas_id = $("#agendas_id").val();
    var observacao_modal = $("#observacao_modal").val();
    $.ajax({
        type:'POST',
        url:'/ausentaragenda',
        data:{"_token": $('meta[name="csrf-token"]').attr('content'),agendas_id:agendas_id, observacao_modal:observacao_modal},
        success:function(data){
            var dataAgenda = window.sessionStorage.getItem('dtAgenda');
            buscarAgenda(dataAgenda);   
            carregarCalendario(dataAgenda);  
            $("#observacao_modal").val('');
            $('#modal-ausentar').modal('hide'); 
         }   
       });
  })
}