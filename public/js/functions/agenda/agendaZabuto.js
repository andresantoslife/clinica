var elem = jQuery("#calendario");

//MASCARA DO CALENDARIO, EVENTOS E RETORNO COM FUNCAO
$(document).ready(function () {
  carregarCalendario();
});
$('#prestador').change(function(){
    carregarCalendario();
});

function carregarCalendario(mesAtual){
    if(mesAtual == undefined) { 	
    now = new Date; mesAtual = now.getMonth()+1; mes = now.getMonth()+1; ano = now.getFullYear(); } else {split = mesAtual.split('-'); ano = split[0]; mes = split[1];};
    var unidade = $("#unidade option:selected").val();
    var prestador = $("#prestador option:selected").val();
    var especialidade = $("#especialidade option:selected").val();
    //EVITA ERRO AO REINICIAR A AGENDA
    if (especialidade != '') { var varAjax = "/listarcalendario/"+ unidade + "/prestador/" + prestador + "/especialidade/" + especialidade + ""; } else { var varAjax = null;}
    //CONFIGURAÇÂO DO ZABUTO CALENDAR
    elem.empty().zabuto_calendar({language: "pt", month: mes, year: ano, cell_border: true,
    legend: [
        {type: "block", label: "Agenda Livre", classname: "green"},
        {type: "block", label: "Agenda Cheia", classname: "red"}
      ],
     ajax: {
      url: varAjax,
      modal: true
    },
        action: function () {
            return acaoAgenda(this.id, false);
        }
    });  
}
//FUNCAO DO CLIQUE NA AGENDA
function acaoAgenda(id) {
     var date = $("#" + id).data("date");
     window.sessionStorage.setItem('dtAgenda', date);
     buscarAgenda(date);

}

//FUNCAO CONSULTAR AGENDA
function buscarAgenda(dataAgenda) {
    $("thead tr td").remove();
    $('#spinner').show();
    $('#novoEncaixe').show();
    var unidade = $("#unidade option:selected").val();
    var especialidade = $("#especialidade option:selected").val();
    var prestador = $("#prestador option:selected").val();
    $.get("/buscardadosagenda/"+ unidade + "/especialidade/" + especialidade + "/prestador/" + prestador + "/data/" + dataAgenda,function(data){
        if(typeof data !== 'undefined'){
            $('#tabelabuscarDados').find('thead')
            .append("<tr><td><b>Horário</b></td><td><b>Paciente</b></td><td><b>Tipo</b></td><td><b>Situação</b></td><td><b>Opções</b></td></tr>");            
                var tam = data.length; 
                for (i=0; i<tam; i++) {  
                //Checar campos Nulos
                data[i].paciente == null ? data[i].paciente = " - " : data[i].paciente;
                data[i].tipo == null ? data[i].tipo = " - " : data[i].tipo;
                data[i].situacao == null ? data[i].situacao = " - " : data[i].situacao;  
               
                switch (data[i].situacao) {
                    case 'Livre':
                        data[i].situacao = "<span class='badge bg-green'>" + data[i].situacao + "</span>";
                        opcoes = "<button id='btnAtender' value='" + data[i].id + "' class='btn tbl-button btn-warning btnAtender'><i class='fas fa-bars'></i></button></span>";
                    break;
                    case 'Confirmado':
                        data[i].situacao = "<span class='badge bg-blue'>" + data[i].situacao + "</span>";
                        opcoes = "<button id='btnAtender' value='" + data[i].id + "' class='btn tbl-button btn-warning btnAtender'><i class='fas fa-bars'></i></button></span>";
                    break;
                    case 'Bloqueado':
                        data[i].situacao = "<span class='badge bg-red'>" + data[i].situacao + "</span>";
                        opcoes = "<button id='btnAtender' value='" + data[i].id + "' class='btn tbl-button btn-warning btnAtender'><i class='fas fa-bars'></i></button></span>";
                    break;
                    case 'Em Atendimento':
                        data[i].situacao = "<span class='badge bg-gray'>" + data[i].situacao + "</span>";
                        opcoes = "-";
                    break;
                    case 'Finalizado':
                        data[i].situacao = "<span class='badge bg-gray'>" + data[i].situacao + "</span>";
                        opcoes = "-";
                    break;
                    case 'Fila de Espera':
                        data[i].situacao = "<span class='badge bg-gray'>" + data[i].situacao + "</span>";
                        opcoes = "-";
                    break;
                    default:
                        data[i].situacao = "<span class='badge'>" + data[i].situacao + "</span>";
                        opcoes = "<button id='btnAtender' value='" + data[i].id + "' class='btn tbl-button btn-warning btnAtender'><i class='fas fa-bars'></i></button></span>";
                }
           
      
            $('#tabelabuscarDados').find('thead')
            .append("<tr><td>" + data[i].horario + "</td><td>" + data[i].paciente + "</td><td>" + data[i].tipo + "</td><td>" + data[i].situacao + "</td><td>"+ opcoes +"</td></tr>");            
            }  
        } 
            }).fail(function() {
                alert( "Atenção! Não foi possível buscar os dados da agenda." );
      }).always(function(){
        $('#spinner').hide(); 
      });
}





