function atenderPaciente(id){
    $('#agendasAtender_id').val(id);
    getAtender(id); 
    window.sessionStorage.setItem('idAgenda', id);
}

function getAtender(idAgenda) {
    //Limpar os Campos
    $("#procedimentos_atender").empty();
    $("#convenio_atender").empty();
    $("#tipoatendimento_atender").empty();
    $("#nrguia_atender").val('');
    $("#validadecarteirinha_atender").val('');
    $("#senhacarteirinha_atender").val('');
    $("#carteirinha_atender").val('');
  
        //get preenchendo dados cabecalho do form
        var nomePaciente = $("#nomePaciente");
        var horaPaciente = $("#horaPaciente");
        var paciente_id = $("#paciente_id");
    
    //fim dos gets
    $.get("/consulta/agenda/" + idAgenda,function(data){
    if(data.length > 0){
       var tam = data.length;  
       paciente_id.text(data[0].id);
       nomePaciente.text(data[0].nome); 
       horaPaciente.text(data[0].hora);   
       consultaSelect();
       consultaSelectConvenio(data[0].id);
                 //EXIBIR MODAL SE EXISTIR PACIENTE
                $('#modal-atender').modal('show');
            } 
        }
    ); 
    
} //Fim getAtender

function atenderModal(){
    //funcao Click buttonModalAtender
    $("#atendimentoPaciente").submit(function(e){
    e.preventDefault();
    //atrvariaveis
        //  -- inputs
    var agendasAtender_id = $("#agendasAtender_id").val();
    var indicacaoacidente_atender = $("#indicacaoacidente_atender").val();
    var carateratendimento_atender = $("#carateratendimento_atender").val();  
    var carteirinha_atender = $("#carteirinha_atender").val();
    var senhacarteirinha_atender = $("#senhacarteirinha_atender").val();
    var validadecarteirinha_atender = $("#validadecarteirinha_atender").val();
    var nrguia_atender = $("#nrguia_atender").val();
    var paciente_id = $("#paciente_id").text();

      //  -- select
    var rn_atender = $("#rn_atender option:selected").val(); 
    var unidade_atender = $("#unidade option:selected").val();
    var convenio_atender = $("#convenio_atender option:selected").val();
    var tipoatendimento_atender = $("#tipoatendimento_atender option:selected").val();
    var procedimentos_atender = $("#procedimentos_atender").val(); //remove selected
    //fim atrvariaveis
 
   $.ajax({
        type:'POST',
        url:'/atenderpaciente',
        data:{"_token": $('meta[name="csrf-token"]').attr('content'),
        paciente_id: paciente_id,
        unidade_atender: unidade_atender,
        agendasAtender_id: agendasAtender_id, 
        indicacaoacidente_atender:indicacaoacidente_atender,
        carateratendimento_atender: carateratendimento_atender,
        carteirinha_atender: carteirinha_atender,
        senhacarteirinha_atender: senhacarteirinha_atender,
        validadecarteirinha_atender: validadecarteirinha_atender,
        nrguia_atender: nrguia_atender,
        rn_atender: rn_atender,
        convenio_atender: convenio_atender,
        tipoatendimento_atender: tipoatendimento_atender,
        procedimentos_atender: procedimentos_atender
          },success:function(data){
            var dataAgenda = window.sessionStorage.getItem('dtAgenda');
            buscarAgenda(dataAgenda);   
            carregarCalendario(dataAgenda);  
            $('#modal-atender').modal('hide'); 
         }   
       }); 
    return false;
  })
}
// Tipo de Atendiemnto
function consultaSelect() {
    $.get("/consulta/tipoatendimento",function(data){
        if(typeof data !== 'undefined'){
                var tam = data.length; 
                for (i=0; i<tam; i++) {  
                $('#tipoatendimento_atender')
                .append("<option value='" + (data[i].id) + "'>" + (data[i].nome) + "</option>"); 
            }  
        } 
  
    }).fail(function() {
     alert( "Erro! Não foi possível buscar encontrar Tipo de Atendimento");
   });
}

// Convenios
function consultaSelectConvenio(idPaciente) {
    $.get("/consulta/convenio/" + idPaciente,function(data){
        if(typeof data !== 'undefined'){
                var tam = data.length; 
                for (i=0; i<tam; i++) {  
                $('#convenio_atender')
                .append("<option disabled hidden selected> Selectionar </option><option value='" + (data[i].id) + "'>" + (data[i].nome) + "</option>"); 
            }  
        } 
  
    }).fail(function() {
     alert( "Erro! Não foi possível buscar encontrar Convenio");
   });
}

// Procedimentos

    $('#convenio_atender').change(function(){
    var idAgendaSession = window.sessionStorage.getItem('idAgenda');
       var convenioatender = $("#convenio_atender option:selected").val();
        $.get("/consulta/procedimentos/agenda/" + idAgendaSession + "/convenio/" + convenioatender,function(data){
            if(typeof data !== 'undefined'){
                    var tam = data.length; 
                    for (i=0; i<tam; i++) {  
                    $('#procedimentos_atender')
                    .append("<option value='" + (data[i].id) + "'>" + (data[i].nome) + "</option>"); 
                     }  
            }  
        }); 
    }); 
