//INICIALIZAR
  
$(document).ready(function(){
    buscarPaciente();
    $('#spinner').hide();
    $('#novoEncaixe').hide();
    $('#unidade').change(function(){
      $("#especialidade").empty();
      $("#prestador").empty();
        buscaEspecialidade();
        });
    $('#especialidade').change(function(){
        $("#prestador").empty();
         buscaPrestador();
        });
});

function buscarPaciente() {
     $('#selectPaciente').empty(); //select agendar
     $('#encaixePaciente').empty(); //select encaixe
       $.get("/todospacientes",function(data){
            if(data.length > 0){
                    var tam = data.length; 
                    for (i=0; i<tam; i++) {  
                    if (data[i].catNome == null) {$catNome = 'Nulo'; } else { $catNome = data[i].catNome}
                      if (data[i].dtnasc == null) {$dtnasc = 'Nulo'; } else { $dtnasc = data[i].dtnasc}
                    $('#selectPaciente')
                    .append("<option value='" + data[i].id + "'>" + data[i].nome + " - " + $catNome +" - " + $dtnasc + "</option>");   
                     $('#encaixePaciente')
                    .append("<option value='" + data[i].id + "'>" + data[i].nome + " - " + $catNome +" - " + $dtnasc + "</option>");                     
                }  
            } 
        })
}



//PREENCHENDO SELECT2 DO BANCO DE DADOS TABELA RELACIONAMENTO CONVENIOS_PACIENTE
function buscaEspecialidade(){
    //var paciente = $("#pacienteSelect").val();
    var unidade = $("#unidade option:selected").val();
    $.get("/listaragendaespecialidade/"+ unidade,function(data){
            if(typeof data !== 'undefined'){
                    var tam = data.length; 
                    for (i=0; i<tam; i++) {  
                    $('#especialidade')
                    .append("<option disabled hidden selected> Selectionar </option><option value='" + (data[i].id) + "'>" + (data[i].nome) + "</option>");            
                }  
            } 
        }).fail(function() {
            alert( "Atenção! Não foi possível buscar a especialidade." );
          }); 
    }

function buscaPrestador(){
        //var paciente = $("#pacienteSelect").val();
        var unidade = $("#unidade option:selected").val();
        var especialidade = $("#especialidade option:selected").val();
        $.get("/listaragendaprestador/"+ unidade + "/especialidade/" + especialidade,function(data){
                if(typeof data !== 'undefined'){
                        var tam = data.length; 
                        for (i=0; i<tam; i++) {  
                        $('#prestador')
                        .append("<option disabled hidden selected> Selectionar </option><option value='" + (data[i].id) + "'>" + (data[i].nome) + "</option>");            
                    }  
                } 
            }).fail(function() {
                alert( "Atenção! Não foi possível buscar a especialidade." );
              }); 
        }



