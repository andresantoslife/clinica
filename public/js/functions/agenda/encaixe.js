$('#novoEncaixe').click(function() {
   $('#modal-encaixe').modal('show');
    var dataFormat = window.sessionStorage.getItem('dtAgenda');
    split = dataFormat.split('-');
    dataFormat = split[2] + "/" +split[1]+"/"+split[0];
    $('#labelEncaixe').html("<b> Encaixe para o dia " + dataFormat + "</b>");
});

$('#formEncaixe').submit(function(event){
    event.preventDefault();
    //Variaveis
    
    var unidade = $("#unidade option:selected").val();
    var especialidade = $("#especialidade option:selected").val();
    var prestador = $("#prestador option:selected").val();
    var dataAgenda = window.sessionStorage.getItem('dtAgenda');
    var encaixeHorario = $("#encaixeHorario").val();
    var encaixePaciente = $("#encaixePaciente option:selected").val();
    var encaixeTipoConsulta = $("#encaixeTipoConsulta option:selected").val();
    
              $.ajax({
                type:'POST',
                url:'/encaixe',
                data:{
                    "_token": $('meta[name="csrf-token"]').attr('content'),
                    unidade: unidade,
                    especialidade: especialidade,
                    prestador: prestador,
                    dataAgenda: dataAgenda,
                    encaixeHorario: encaixeHorario,
                    encaixePaciente: encaixePaciente,
                    encaixeTipoConsulta: encaixeTipoConsulta
                    },success:function(data){
                    //Fechar Modal
                    var dataAgenda = window.sessionStorage.getItem('dtAgenda');
                    buscarAgenda(dataAgenda);   
                    carregarCalendario(dataAgenda);  
                    $('#modal-encaixe').modal('hide');
                }   
            }); 
        
    
});
