//INICIALIZAR
$(document).ready(function(){
    $('#spinner').hide();
    $('#unidade').change(function(){
      $("#prestador").empty();
        buscaPrestador();
        });
    $('#prestador').change(function(){
      $("#especialidade").empty();
        buscaEspecialidade();
          });
    $('#especialidade').change(function(){
        pesquisar();
    });
});

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });

//PREENCHENDO SELECT2 DO BANCO DE DADOS TABELA RELACIONAMENTO CONVENIOS_PACIENTE
function buscaPrestador(){
    //var paciente = $("#pacienteSelect").val();
    var unidade = $("#unidade option:selected").val();
    $.get("/listarprestador/"+ unidade,function(data){
            if(typeof data !== 'undefined'){
                    var tam = data.length; 
                    for (i=0; i<tam; i++) {  
                    $('#prestador')
                    .append("<option disabled hidden selected> Selectionar </option><option value='" + (data[i].id) + "'>" + (data[i].nome) + "</option>");            
                }  
            } 
        }); 
    }
    function buscaEspecialidade(){
      var unidade = $("#unidade option:selected").val();
      var prestador = $("#prestador option:selected").val();
      $.get("/listarespecialidade/"+ unidade + "/prestador/" + prestador,function(data){
              if(typeof data !== 'undefined'){
                      var tam = data.length; 
                      for (i=0; i<tam; i++) {  
                      $('#especialidade')
                      .append("<option disabled hidden selected> Selectionar </option><option value='" + (data[i].id) + "'>" + (data[i].nome) + "</option>"); 
                  }  
              } 
        
          }).fail(function() {
           alert( "Erro! Não foi possível buscar o expediente." );
         });
      }

$(".btn-submit").click(function(e){
      e.preventDefault();
      var unidade = $("#unidade option:selected").val();
      var prestador = $("#prestador option:selected").val();
      var especialidade = $("#especialidade option:selected").val();
      var semana = $("#semana").val();
      var intervalo = $("#intervalo").val();
      var dtinicio = $("#dtinicio").val();
      var dtfinal = $("#dtfinal").val();
      var hinicio = $("#hinicio").val();
      var hfinal = $("#hfinal").val();
   
        $.ajax({
          type:'POST',
          url:'/expedientepost',
          data:{unidade:unidade, prestador:prestador, especialidade:especialidade,  semana:semana, intervalo:intervalo, dtinicio:dtinicio, dtfinal:dtfinal, hinicio:hinicio, hfinal:hfinal},
          success:function(data){
          pesquisar();
          }   
       });
      
});

function pesquisar() {
    $('#spinner').show();
    $("thead tr td").remove();
      var unidade = $("#unidade option:selected").val();
      var prestador = $("#prestador option:selected").val();
      var especialidade = $("#especialidade option:selected").val();
      var especialidade = $("#especialidade option:selected").val();
        $.get("/listarexpediente/"+ unidade + "/prestador/" + prestador + "/especialidade/" + especialidade,function(data){
              var tam = data.length;
              //console.log(tam);
              for (i=0; i<tam; i++) {
              // console.log(data.expediente[i].id);
            $('#tabelaExpediente').find('thead')
              .append("<tr><td>" + data[i].semana + "</td><td>" + data[i].intervalo + " Min(s)</td><td>" + data[i].dtinicio + " á " + data[i].dtfinal + "</td><td>" + data[i].hinicio + "-" + data[i].hfinal + "</td><td><a id='btnExcluir' onclick='deletarExpediente(" + data[i].expediente + ")' class='glyphicon glyphicon-trash'></td></tr>");
            } 
      }).always(function(){
        $('#spinner').hide(); 
      });
  }
function modalErro(error){
        $("#mensagem").text(error);
            $('#modal-erro').modal('show');
          setTimeout(function() {
              $('#modal-erro').modal('hide');
          }, 5000);
}
function deletarExpediente(id) {
  $.ajax({
    type:'POST',
    url:'/expedientedel',
    data:{expediente:id},
    success:function(data){
      pesquisar();
    },error: function (xhr) {
      var errors = xhr.responseJSON;
      var error =  errors.warning;
      modalErro(error);
  }   
 });
}