//INICIALIZAR
$(document).ready(function(){
    $('#spinner').hide();
    $("#convenioSelect").select2();
    convenios();
    convenioAdicionar(); 
    convenioTodos(); 
   
});


//PREENCHENDO SELECT2 DO BANCO DE DADOS TABELA RELACIONAMENTO CONVENIOS_PACIENTE
function convenioTodos(){
    var paciente = $("#pacienteSelect").val();
    $.get("/listarpacienteconvenios/"+ paciente,function(data){
            if(typeof data.convenioAll !== 'undefined'){
                    var tam = data.convenioAll.length; 
                    for (i=0; i<tam; i++) {  
                    $('#convenios')
                    .append("<option selected value='" + (data.convenioAll[i].id) + "'>" + (data.convenioAll[i].nome) + "</option>");
                    
                }  
            } 
        });
    
    }


function convenios(){
    $('#spinner').show();
    $("thead tr td").remove();
    var paciente = $("#pacienteSelect").val();
    $.get("/listarpacienteconvenios/"+ paciente,function(data){
            if(typeof data.convenioSelect !== 'undefined'){
                    var tam = data.convenioSelect.length; 
                    for (i=0; i<tam; i++) { 
                    $('#tabelaConvenio').find('thead')
                    .append("<tr><td>" + (data.convenioSelect[i].nome) + "</td><td><a id='btnExcluir' onclick='deletarConvenio(" + (data.convenioSelect[i].id) + ")' class='glyphicon glyphicon-trash'></td></tr>");
              }  
            } 
        }).always(function(){
            $('#spinner').hide(); 
          });
    
    }
//REMOVER RELACIONAMENTO CONVENIO TEMPO DE EXECUÇÃO, SE EXISTIR SELECT TEM REMOÇÃO
function deletarConvenio(id) {
    var paciente = $("#pacienteSelect").val();
    $.get("/removerpacienteconvenios/"+ paciente + "/convenio/" + id);
    convenios();
}   

function convenioAdicionar(){
    $("#acoes").click(function(e){
    e.preventDefault();
    var convenioOption = $("#convenios option:selected").val();
    var paciente = $("#pacienteSelect").val();
      $.get("/adicionarpacienteconvenios/"+ paciente + "/convenio/" + convenioOption,function(data){ 
        convenios();   
        })  
     })
}