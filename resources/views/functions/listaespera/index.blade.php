@extends('adminlte::page')

@section('title', 'AAPVR - Clínica')

@section('content_header')
    <h1>Lista de Espera</h1>
    <ol class="breadcrumb"> 
        <li><a href="/">Dashboard</a></li>
        <li><a href="">Lista de Espera</a></li>
    </ol>
@stop

@section('content')
<div class="box">
    <div class="box-header">
        
        <form action="{{ route('listaespera.pesquisar') }}"  method="POST" class="form-inline">
        @csrf
        <input name="dataPesquisar" id="dataPesquisar" type="date" class="form-control" value="<?php if(!empty($dadosdaAgendas[0]->data)) {echo $dadosdaAgendas[0]->data; } else {echo date('Y-m-d');} ?>" required />
        <button type="submit" class="btn btn-default"><i class="fa fa-search"></i> Pesquisar</button>
        </form> 
    </div>

    <div class="box-body">
        <table class="table table-striped">
            <tr>
               <th width="50px">Hora</th>
               <th width="160px">Paciente</th>
               <th width="60px">Convênio</th>        
               <th width="60px">Status</th>
               <th width="60px">Ações</th>
            </tr>
            @foreach($dadosdaAgendas as $dadosdaAgenda)
             <tr>
                 <td>
                    <?=$dadosdaAgenda->horario?>
                 </td>
                 <td>
                    <?=$dadosdaAgenda->paciente?>
                </td>
                <td>
                    <?=$dadosdaAgenda->convenio?>
                </td>
                <td>
                    <?php
                    switch($dadosdaAgenda->status)
                    {
                       case 'aguardando':
                       echo "<span class='badge bg-red'>Aguardando</span>
                         <td>
                         <a href='prontuario/$dadosdaAgenda->paciente_id' class='btn btn-primary btn-sm'> Atender </a>
                        </td>";
                       break;
                       case 'aberto':
                       echo "<span class='badge bg'>Em Atendimento</span><td>
                        <a href='prontuario/$dadosdaAgenda->paciente_id' class='btn btn-primary btn-sm'> Atender </a>
                        </td>";
                       break;
                       case 'pausado':
                       echo "<span class='badge bg-gray'>Pausado</span><td>
                        <a href='prontuario/$dadosdaAgenda->paciente_id' class='btn btn-primary btn-sm'> Atender </a>
                      </td>";
                       break;
                       case 'finalizado':
                       echo "<span class='badge bg-green'>Finalizado</span><td>
                        <a href='prontuario/$dadosdaAgenda->paciente_id' class='btn btn-secundary btn-sm'> Ver Mais.. </a>
                        </td>";
                       break;
                     }
                    ?>    
                 </td>
                
            </tr>
            @endforeach
        </table>
    
    </div>
</div>
@stop
