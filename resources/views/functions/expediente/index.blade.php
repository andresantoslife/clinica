@extends('adminlte::page')

@section('title', 'AAPVR - Clínica')

<meta name="csrf-token" content="{{ csrf_token() }}" />

@section('content_header')
    <h1>Funcionalidades :: Expediente</h1>
    <ol class="breadcrumb"> 
        <li><a href="/">Dashboard</a></li>
        <li><a href="">Expediente</a></li>
    </ol>
@stop

@section('content')
<!-- Modal -->
<div class="modal modal-warning fade" id="modal-erro" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-body">
          <p align="center"><b id="mensagem"></b></p>
      </div>
    </div>
  </div>
</div>


@foreach($errors->all() as $error)
<div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-ban"></i> Atenção</h4>
                {{ $error }}
              </div>
@endforeach
<div class="row">
<div class="col-md-6">
<div class="box">
<div class="box-header">

</div>

<div class="box-body">
      <form action="/expedientepost"  method="POST">
          @csrf
      <div class="form-group row">
      <div class="col-xs-10">    
                <label>* Unidade</label>
                <select name="unidade" id="unidade" class="form-control" required>
                  <option disabled hidden selected> Selectionar </option>
                        @foreach($unidades as $unidade)
                            <option value="{{ $unidade->id }}">{{ $unidade->nome }}</option>
                        @endforeach       
        </select> 
            </div> 
      </div>
      <div class="form-group row">
        <div class="col-xs-10">    
        <label>* Prestador</label>
                <select name="prestador" id="prestador" class="form-control" required>
                       
                                     
              </select> 
        </div>
      </div>
      <div class="form-group row">
        <div class="col-xs-10">    
        <label>* Especialidade</label>
                <select name="especialidade" id="especialidade" class="form-control" required>
                        
                                     
              </select> 
        </div>
      </div>
      <div class="form-group row">
        <div class="col-xs-5">    
         <label>* Dia da Semana</label>
                <select name="semana" id="semana" class="form-control" required>
                        <option disabled hidden selected> Selectionar </option>
                        <option value='1'> Segunda </option>  
                        <option value='2'> Terça </option>   
                        <option value='3'> Quarta </option>   
                        <option value='4'> Quinta </option>   
                        <option value='5'> Sexta </option>   
                        <option value='6'> Sábado </option>   
                        <option value='0'> Domingo </option>             
              </select> 
        </div>
        <div class="col-xs-5">  
          <label>* Intervalo em Minutos:</label>
          <div class="input-group date">
            <div class="input-group-addon">
              <i class="fa fa-clock"></i>
            </div>
            <input min='10' name="intervalo" type="number" class="form-control pull-right" id="intervalo" required>
          </div>
        </div>  <!-- /.COL-XS-4 -->
      </div>
      <div class="form-group row">
        <div class="col-xs-5">  
          <label>* Data Inicio:</label>
          <div class="input-group date">
            <div class="input-group-addon">
              <i class="fa fa-calendar"></i>
            </div>
            <input name="dtinicio" type="text" class="form-control pull-right" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask id="dtinicio" required>
          </div>
        </div>  <!-- /.COL-XS-4 -->

        <div class="col-xs-5">  
          <label>* Data Final:</label>
          <div class="input-group date">
            <div class="input-group-addon">
              <i class="fa fa-calendar"></i>
            </div>
            <input name="dtfinal" type="text" class="form-control pull-right" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask id="dtfinal" required>
          </div>
        </div>  <!-- /.COL-XS-4 -->
      </div> 

      <div class="form-group row">
        <div class="col-xs-5">  
          <label>*Hora Inicio do Expediente:</label>
          <div class="input-group date">
            <div class="input-group-addon">
              <i class="fa fa-clock"></i>
            </div>
            <input type="time" name="hinicio" class="form-control pull-right" id="hinicio" required>
          </div>
        </div>  <!-- /.COL-XS-4 -->

        <div class="col-xs-5">  
          <label>* Hora Final do Expediente:</label>
            <div class="input-group date">
              <div class="input-group-addon">
                <i class="fa fa-clock"></i>
              </div>
              <input type="time" name="hfinal" class="form-control pull-right" id="hfinal" required>
            </div>
        </div>  <!-- /.COL-XS-4 -->
      </div> 
        <button type="submit" class="btn btn-primary btn-submit"><span class="glyphicon glyphicon-save"></span> Adicionar</button>
      </form>
</div>
</div>
</div> <!--FIM col-md-8-->

<div class="col-md-6">
   <div class="box">
   <div class="box-header">
   <div align="center">
     <h4> Expediente do Prestador </h4>
     <img src="{{ asset('img/spinner.gif') }}" id="spinner"></img>
    </div>
   </div> <!--FIM header -->
   <div class="box-body">

 <div id="table" style="overflow:auto; max-height: 500px">
   <table class="table table-striped" id="tabelaExpediente">
        <thead> 
            <tr>
            <!-- Lista de Especialidades JQuery -->
            </tr>
        </thead>
    </table>
</div><!--id="table" -->

   </div> <!--box-body -->
  </div> <!--FIM box -->
 </div> <!--FIM col-md-8-->
</div> <!--FIM row -->
<script src="{{ asset('js/jquery.maskedinput.min.js') }}"></script>
<script src="{{ asset('js/functions/expediente/expedienteMask.js') }}"></script>
<script src="{{ asset('js/functions/expediente/expediente.js') }}"></script>
<script src="{{ asset('js/jquery.inputmask.js') }}"></script>
@stop

