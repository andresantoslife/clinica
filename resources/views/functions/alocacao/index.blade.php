@extends('adminlte::page')

@section('title', 'AAPVR - Clínica')

<meta name="csrf-token" content="{{ csrf_token() }}" />

@section('content_header')
    <h1>Funcionalidades :: Alocação</h1>
    <ol class="breadcrumb"> 
        <li><a href="/">Dashboard</a></li>
        <li><a href="">Alocação</a></li>
    </ol>
@stop

@section('content')
<!-- Modal -->
<div class="modal modal-warning fade" id="modal-erro" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-body">
          <p align="center"><b id="mensagem"></b></p>
      </div>
    </div>
  </div>
</div>

@foreach($errors->all() as $error)
<div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-ban"></i> Atenção</h4>
                {{ $error }}
              </div>
@endforeach
<div class="row">
<div class="col-md-6">
<div class="box">
<div class="box-header">
<a href="{{ route('alocacao.index') }}" class="btn btn-link"><span class="glyphicon glyphicon-chevron-left"></span> Voltar</a> 
</div>

<div class="box-body">
      <form action="/alocacao"  method="POST">
          @csrf
      <div class="form-group row">
      <div class="col-xs-10">    
                <label>Prestador</label>
                <select name="prestador" id="prestador" class="form-control">
                        <option disabled hidden selected> Selectionar </option>
                              @foreach($prestadores as $prestador)
                                  <option value="{{ $prestador->id }}">{{ $prestador->nome }}</option>
                              @endforeach       
                </select> 
            </div> 
      </div>
      <div class="form-group row">
        <div class="col-xs-5">    
        <label>Unidade</label>
                <select name="unidade" id="unidade" class="form-control">
                        <option disabled hidden selected> Selectionar </option>
                              @foreach($unidades as $unidade)
                                  <option value="{{ $unidade->id }}">{{ $unidade->nome }}</option>
                              @endforeach       
              </select> 
        </div>
      </div>
      <div class="form-group row">
        <div class="col-xs-10">  
            <label>*Especialidades</label>
                <select name="especialidade[]" id="especialidade" class="form-control" required> 
                              @foreach($especialidades as $especialidade)
                                  <option value="{{ $especialidade->id }}">{{ $especialidade->nome }}</option>
                              @endforeach       
              </select> 
        </div>
      </div>
        <button type="submit" class="btn btn-primary btn-submit"><span class="glyphicon glyphicon-save"></span> Adicionar</button>
      </form>
</div>
</div>
</div> <!--FIM col-md-8-->

<div class="col-md-6">
   <div class="box">
   <div class="box-header">
   <div align="center">
     <h4> Especialidades do Prestador </h4>
     <img src="{{ asset('img/spinner.gif') }}" id="spinner"></img>
    </div>
   </div> <!--FIM header -->
   <div class="box-body">

 <div id="table" style="overflow:auto; max-height: 500px">
   <table class="table table-striped" id="tabelaEspecialidade">
   <tbody>
      
    </tbody>
    <thead> 
        <tr>
        <!-- Lista de Especialidades JQuery -->
        </tr>
    </thead>
    </table>
</div><!--id="table" -->

   </div> <!--box-body -->
  </div> <!--FIM box -->
 </div> <!--FIM col-md-8-->
</div> <!--FIM row -->

<script src="{{ asset('js/buscaEspecialidade.js') }}"></script>
@stop

