@extends('adminlte::page')

@section('title', 'AAPVR - Clínica')

@section('content_header')
    <h1>Prontuário</h1>
    <ol class="breadcrumb"> 
        <li><a href="/">Dashboard</a></li>
        <li><a href="">Prontuário</a></li>
    </ol>
@stop
<meta name="csrf-token" content="{{ csrf_token() }}" />
@section('content')
<link rel="stylesheet" type="text/css" href="{{ asset('css/functions/prontuario/prontuario.css') }}">
       <!-- MEIO -->
<section class="content">
   <div class="row">
     <div class="col-md-4">  
            <!-- Profile Image -->
            <div class="box box-primary">
                <div class="box-body box-profile">
                  <h5 class="profile-username text-center" style="font-size: 14px">
                    <select name="paciente" id="paciente" class="form-control">
                      <?php 
                      //Somente um registro
                      if(!empty($SelectPaciente)) {
                      $SelectPaciente = $SelectPaciente[0];
                      echo "<option value='$SelectPaciente->id' selected>$SelectPaciente->nome</option>";
                      }
                      //Varios Registros
                      if(!empty($Pacientes)) {
                         foreach ($Pacientes as $Paciente) {
                          echo "<option value='$Paciente->id'>$Paciente->nome</option> ";
                         }
                      }
                      ?>
                        
                   </select> 
                 </h5>   
                  <ul class="list-group list-group-unbordered">
                    <li class="list-group-item">
                      <b>Sexo</b> <a class="pull-right" id="sexo"></a>
                    </li>
                    <li class="list-group-item">
                      <b>Nascimento</b> <a class="pull-right" id="nascimento"></a>
                    </li>
                    <li class="list-group-item">
                      <b>Atendimento</b> <a class="pull-right" id="atendimento"></a>
                    </li>
                    <li class="list-group-item" align="center">
                     <h5><i class="fa fa-clock" aria-hidden="true"></i> 
                      <div class="countup" id="countup1">
                      {{-- <span class="timeel days">00</span>
                        <span class="timeel timeRefDays">Dias</span> --}}
                        <span class="timeel hours">0</span>
                        <span class="timeel timeRefHours">Horas</span>
                        <span class="timeel minutes">0</span>
                        <span class="timeel timeRefMinutes">Minutos</span>
                        <span class="timeel seconds">0</span>
                        <span class="timeel timeRefSeconds">Segundos</span>
                      </div>
                    </h5>
                   
                     <button id="iniciar" type="button" class="btn btn-success">Iniciar</button>
                    <!-- <button type="button" class="btn btn-warning disabled">Pausar</button> -->
                     <button id="finalizar" type="button" class="btn btn-danger disabled">Finalizar</button>                                
                  
                  </li>
                 </ul>
                </div>
                <!-- /.box-body -->
              </div>
              <!-- /.box -->

       <div class="box box-solid">
        <div class="box-header with-border" align="center">
          <h3 class="box-title">Menu Principal</h3>
          <div class="box-tools">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
        </div>
        <div class="box-body no-padding">
          <ul class="nav nav-pills nav-stacked">
            <li><a style="cursor:pointer;" onclick="anamnese()"><i class="fa fa-newspaper"></i> Anamnese</a></li>
            <li><a style="cursor:pointer;" onclick="evolucaos()"><i class="fa fa-newspaper"></i> Evoluções</a></li>
            <li><a style="cursor:pointer;" onclick="diagnostico()"><i class="fa fa-heartbeat"></i> Diagnóstico</a></li>
            <li><a style="cursor:pointer;" onclick="exames()"><i class="fa fa-vial"></i> Exames</a></li>
            <li><a style="cursor:pointer;" onclick="atestado()"><i class="fa fa-file-signature"></i> Atestados</a></li>
           {{--  <li><a style="cursor:pointer;" onclick="receituario()"><i class="fa fa-file-invoice"></i> Receituário</a></li> --}}
          </ul>
        </div> <!-- /.box-body -->   
      </div> <!-- /. box -->
    </div> <!-- /.col -->
    <div class="col-md-8" >
        <div class="box box-primary" id="anamneseTab">
          <div class="box-header with-border" align="center">
            <h3 id="titulo" class="box-title"></h3>
          </div>
          <div div class="box-body" align="center">
  <!-- Inicio do Meio -->

<!-- DIAGNOSTICO -->
<div align="center" id="diagnostico">
  <div class="box-body pad">
    <div id="formularioCid">
        <h5 class="profile-username text-left" style="font-size: 14px">
          <select name="cid" id="cid" class="form-control">
             @foreach($cids as $cid)
                <option value="<?=$cid->id?>">Código: <?=$cid->codigo?> Descrição: <?=$cid->descricao?></option>
             @endforeach
          </select>  
        </h5>  
          <button id="cidAdicionar" class="btn btn-primary salvar">Adicionar</button>
    
    <br><hr></div> <h4 align="center"> <b>Histórico </b></h4>
      <div id="table" style="overflow:auto; max-height: 500px">
          <table class="table table-striped" id="tabelaCID">
            <thead> 
              <tr>
              <!-- Lista de Diagnosticos CID -->
              </tr>
          </thead>
          </table>
      </div><!--id="table" -->
  </div>
</div>
<!-- FIM DIAGNOSTICO -->    
 <!-- EXAMES -->
 
 <div  align="left" id="exames">
  <form id="formExames" method="POST">
    @csrf

  <div class="box-body pad">
    <div id="divExames">
    <div class="row">
      <div class="col-md-4" >
      <label> Tipo *</label>
          <select name="tipoExames" id="tipoExames" class="form-control" required>
            <option selected value='0'> Selectionar </option>
            @foreach($tipoexames as $tipoexame)  
            <option value="<?=$tipoexame->id?>"><?=$tipoexame->nome?></option>
            @endforeach
          </select>  
    </div> 
    <div class="col-md-8" >
      <label> Selecionar Exame *</label>
      <select name="Exames" id="Exames" class="form-control" required>
      </select>  
    </div>
    </div>
  <br>
  <div class="row">
      <div class="col-md-4">
        <label> Data para Realizar Exame *</label>
        <input type="date" name="dataExame" class="form-control" id="dataExame" required/> 
      </div>
    
      <div class="col-md-8">
        <label> Observação *</label>
        <textarea id="observacaoExame" name="observacaoExame" class="form-control" rows="04" cols="50" required></textarea>
      </div>
  </div>
  <br>
    <div class="row" align="center">
      <button id="SalvarExame" type="submit" class="btn btn-warning"><i class="fa fa-save" aria-hidden="true"></i> Salvar Pedido</button>
    </div>
  </form>

  <hr>
</div>
  <h4 align="center"> <b>Histórico </b></h4>
   <div class="row" align="left">
    
    <div id="table" style="overflow:auto">
      <table class="table table-striped" id="tabelaExames">
        <thead> 
          <tr>
          <!-- Lista de Exames -->
          </tr>
       </thead>
     </table>
  </div><!--id="table" -->
   </div>
</div>
</div>
<!-- FIM EXAMES -->
 <!-- RECEITUARIO -->
 <div  align="center" id="receituario">
  <div class="box-body pad">
    Em Construção!
        
  </div>
</div>
<!-- FIM RECEITUARIO -->
<!-- ATESTADO -->
<div  align="left" id="atestado">
  <div class="box-body pad">
    <select name="opcoesAtestado" id="opcoesAtestado" class="form-control" required>
      <option value="1">Atestado</option>
      <option value="2">Comparecimento</option>
     </select>
     <br>
<!--  INICIO ATESTADOPACIENTE -->
  <div id="atestadoPaciente">
    <form id="formAtestado" action="atestadopdf" target="_blank" class="form-inline" method="POST">
      @csrf
          <input type="hidden" id="atendimentoID" name="atendimentoID"/>
          <input type="hidden" value='1' name="atestado"/>
            <p>Atesto para devidos fins, que o(a)
            <p id="pacienteAtestado"></p>
            <p>esteve sob meus cuidados profissionais no período das <input type="time" name="atestadoHorainicio" class="form-control" id="atestadoHorainicio" required/> às <input type="time" name="atestadoHorafim" class="form-control" id="atestadoHorafim" required /> horas </p>
            <p>do dia <input type="date" name="atestadoData" class="form-control" id="atestadoData" required/> necessitando o(a) mesmo (a) de <input type="number" name="atestadoDias" class="form-control" id="atestadoDias" required /> dias</p>
            <p>de convalescença CID 
              <select name="CIDAtestado" id="CIDAtestado" class="form-control" required>
                @foreach($cids as $cid)  
                <option value="<?=$cid->id?>"><?=$cid->codigo?></option>
                @endforeach
               </select>
            </p>
        <br> 
            <button id="imprimitAtestado" type="submit" class="btn btn-warning"><i class="fa fa-print" aria-hidden="true"></i> Imprimir Atestado</button>
    </form> 
  </div> <!-- FIM ATESTADOPACIENTE -->

  <!--  INICIO COMPARECIMENTODOPACIENTE -->
  <div id="comparecimentoPaciente">
    <form id="formComparecimento" action="atestadopdf" target="_blank" class="form-inline" method="POST">
      @csrf
            <input type="hidden" id="atendimentoCompID" name="atendimentoCompID"/>
            <input type="hidden" value='2' name="atestado"/>
            <p>Atesto para devidos fins, que o(a) Sr(a) </p>
            <p id="pacienteAtestado"></p>
            <p>esteve em consulta médica no dia <input type="date" name="comparecimentoData" class="form-control" id="comparecimentoData" required/></p>
            <p>no período de  <input type="time" name="comparecimentoHorainicio" class="form-control" id="comparecimentoHorainicio" required/> às <input type="time" name="comparecimentoHorafim" class="form-control" id="comparecimentoHorafim" required /> horas </p> 
          <br>     
            <button id="imprimirComparecimento" type="submit" class="btn btn-warning"><i class="fa fa-print" aria-hidden="true"></i> Imprimir Comparecimento</button>
    </form> 
  </div> <!-- FIM COMPARECIMENTODOPACIENTE -->
  </div>
</div>
<!-- FIM ATESTADO -->

 <!-- ANAMNESE -->
<form method="post" action="/anamnese/salvar" id="formanamnese">
<div  align="center" id="anamnese">
  <div class="box-body pad">
         @csrf
         <textarea id="anamneseForm" name="anamneseForm" rows="8" cols="100"></textarea>
         <button type="submit" id="anamneseSalvar" class="btn btn-primary salvar">Salvar</button>
         <button type="button" id="anamneseLimpar" class="btn btn-secundary limpar">Limpar</button>
    <br><hr> <h4 align="center"> <b>Histórico </b></h4>
        <textarea disabled id="anamneseHist" name="anamneseHist" rows="12" cols="100"></textarea>
  </div>
</div>
</form>
<!-- FIM ANAMNESE -->
<!-- EVOLUCOES -->
<form method="post" action="/evolucaos/salvar" id="formevolucoes">
 <div  align="center" id="evolucoes">
  <div class="box-body pad">
          @csrf
          <textarea id="evolucoesForm" name="evolucoesForm" rows="8" cols="100"></textarea>
          <button id="evolucaosSalvar" class="btn btn-primary salvar">Salvar</button>
          <button id="evolucaosLimpar" class="btn btn-secundary limpar">Limpar</button>
    <br><hr> <h4 align="center"> <b>Histórico </b></h4>
          <textarea disabled id="evolucaosHist" name="evolucaosHist" rows="12" cols="100"></textarea>
  </div>
</div>
</form>
<!-- FIM EVOLUCOES -->

<!-- Fim do Meio -->
          </div>
        </div>
    </div>
</div> <!-- /.row Principal -->

       <!-- FIM MEIO -->
       <script src="{{ asset('js/select2.full.min.js') }}"></script>
       <script src="{{ asset('js/functions/prontuario/prontuario.js') }}"></script>
       <script src="{{ asset('js/functions/prontuario/iniciar.js') }}"></script>
       <script src="{{ asset('js/functions/prontuario/finalizar.js') }}"></script>
       <script src="{{ asset('js/functions/prontuario/anamnese.js') }}"></script>
       <script src="{{ asset('js/functions/prontuario/evolucaos.js') }}"></script>
       <script src="{{ asset('js/functions/prontuario/diagnostico.js') }}"></script>
       <script src="{{ asset('js/functions/prontuario/atestado.js') }}"></script>
       <script src="{{ asset('js/functions/prontuario/exames.js') }}"></script>
       <script src="{{ asset('js/functions/prontuario/cronometro.js') }}"></script>
       
       <script>$('#paciente').select2();</script>
       <script>$('#cid').select2();</script>
       <script>$('#CIDAtestado').select2();</script>
       <script>$('#motivoAtestado').select2();</script>

@stop
