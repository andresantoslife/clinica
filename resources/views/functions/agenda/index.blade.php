@extends('adminlte::page')

@section('title', 'AAPVR - Clínica')

@section('content_header')
@stop
@section('content')
<link rel="stylesheet" type="text/css" href="{{ asset('css/functions/agenda/zabuto_calendar.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/functions/agenda/agenda.css') }}">

{{-- Inicio Modal Atender Paciente --}}
<!-- Large modal -->
<form action="" method="POST" id="atendimentoPaciente" role="form">
<div class="modal fade" id="modal-atender" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4><b>Paciente</b><div id="nomePaciente"></div> <br><b>Horário</b><div id="horaPaciente"></div></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        {{-- Meio Modal--}}
      <div class="row">
      <div class="col-md-12">
        <div class="form-group">
          <input type="hidden" id="paciente_id" name="paciente_id">
          <input type="hidden" id="agendasAtender_id" name="agendasAtender_id">
          <input type="hidden" value='9' id="indicacaoacidente_atender" name="indicacaoacidente_atender">
          <input type="hidden" value='1' id="carateratendimento_atender" name="carateratendimento_atender">
            
          <meta name="csrf-token" content="{{ csrf_token() }}" />
         
          <div class="form-group row">
            <div class="col-xs-4"> 
            <label>Convênio *</label>
                <select name="convenio_atender" id="convenio_atender" class="form-control" required>
                      
                </select> 
            </div>

            <div class="col-xs-4"> 
              <label>Carteirinha *</label>
              <input type="number" id="carteirinha_atender" name="carteirinha_atender" class="form-control" required>     
            </div>

            <div class="col-xs-3"> 
              <label>Senha</label>
              <input type="text" id="senhacarteirinha_atender" name="senhacarteirinha_atender" class="form-control">     
            </div>
           </div>  {{--<div class="form-group row"> --}}

        <div class="form-group row">
          <div class="col-xs-4"> 
            <label>Validade *</label>
            <input type="date" id="validadecarteirinha_atender" name="validadecarteirinha_atender" class="form-control" required>     
          </div>
          <div class="col-xs-4"> 
            <label>Número da Guia</label>
            <input type="number" id="nrguia_atender" name="nrguia_atender" class="form-control">     
          </div>
          <div class="col-xs-3"> 
            <label>Recém-nascido *</label>
            <select name="rn_atender" id="rn_atender" class="form-control">
              <option value='nao' selected> Não </option>
              <option value='sim'> Sim </option>
            </select> 
          </div>

        </div>  {{--<div class="form-group row"> --}}
          <div class="form-group row">
            <div class="col-sm-12"> 
              <label>Tipo de Atendimento *</label><br>
              <select whitname="tipoatendimento_atender[]" id="tipoatendimento_atender" class="form-control" required>
              
              </select> 
            </div></div>
          <div class="form-group row">
            <div class="col-sm-12"> 
              <label>Quais os Procedimentos? *</label><br>
              <select name="procedimentos_atender[]" id="procedimentos_atender" class="form-control" multiple="multiple" required>
                
              </select> 
            </div>

        </div>  {{--<div class="form-group row"> --}}  
       </div>{{--<div class="form-group"> --}}  
      </div>
      </div>
        {{-- Meio Modal--}}
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
        <button type="submit" id="buttonModalAtender" class="btn btn-primary">Gerar Atendimento</button>
      </form>  
      </div>
    </div>
  </div>
</div>
{{-- Fim Modal --}}


{{-- Inicio Modal Agendar--}}
<!-- Large modal -->
<div class="modal fade" id="modal-agendar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4><b>Agendamento</b></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        {{-- Meio Modal--}}
      <div class="row">
      <div class="col-md-12">
        <div class="form-group">
          <input type="hidden" id="idAgendaModal" name="idAgendaModal">
          <label >Selecionar um Paciente</label> <br>
          <select name="selectPaciente" class="form-control" id="selectPaciente">
          </select>
        </div>
        <div class="form-group">
          <label >Tipo de Consulta</label>
          <select name="selectTipoConsulta" class="form-control" id="selectTipoConsulta">
            @foreach($tipoagendas as $tipoagenda)
            <option value="<?= $tipoagenda->id ?>"><?=$tipoagenda->nome ?></option>
            @endforeach     
        </select>
        </div>
      </div>
      </div>
        {{-- Meio Modal--}}
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
        <button type="button" id="buttonModalAgendar" class="btn btn-primary">Salvar</button>
      </div>
    </div>
  </div>
</div>
{{-- Fim Modal --}}


{{-- Inicio Modal Agendar--}}
<!-- Large modal -->
<div class="modal fade" id="modal-encaixe" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 id="labelEncaixe"></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        {{-- Meio Modal--}}
      <form action='/encaixe' method="POST" id="formEncaixe">
      <div class="row">
        <div class="col-md-1">
          <div class="form-group">
            <label>Horário</label>
            <input type="time"  id="encaixeHorario" name="encaixeHorario" required>
          </div>
        </div>
      </div>
      <div class="row">
      <div class="col-md-12">
        <div class="form-group">
          <label >Selecionar um Paciente</label> <br>
          <select name="encaixePaciente" class="form-control" id="encaixePaciente" required>
          </select>
        </div>
        <div class="form-group">
          <label >Tipo de Consulta</label>
          <select name="encaixeTipoConsulta" class="form-control" id="encaixeTipoConsulta" required>
            @foreach($tipoagendas as $tipoagenda)
            <option value="<?= $tipoagenda->id ?>"><?=$tipoagenda->nome ?></option>
            @endforeach     
        </select>
        </div>
      </div>
      </div>
        {{-- Meio Modal--}}
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
        <button type="submit" id="buttonModalEncaixe" class="btn btn-primary">Salvar</button>
    </form>
      </div>
    </div>
  </div>
</div>
{{-- Fim Modal Encaixe --}}




{{-- Inicio Modal Ausentar Paciente --}}
<!-- Large modal -->
<div class="modal fade" id="modal-ausentar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4><b>Ausentar Paciente</b></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        {{-- Meio Modal--}}
      <div class="row">
      <div class="col-md-12">
        <div class="form-group">
          <input type="hidden" id="agendas_id" name="agendas_id">
          <meta name="csrf-token" content="{{ csrf_token() }}" />
          <div class="form-group">
            <label>* Observação</label>
            <textarea id="observacao_modal" class="form-control" rows="3" required></textarea>
          </div>
        </div>
      </div>
      </div>
        {{-- Meio Modal--}}
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
        <button type="button" id="buttonModalAusentar" class="btn btn-primary">Salvar</button>
      </div>
    </div>
  </div>
</div>
{{-- Fim Modal --}}

{{-- Inicio da Agenda --}}
<div class="row">
  <!-- left column -->
<div class="col-md-4">
<div class="box box-primary">
  <!-- /.box-header -->
  <!-- form start -->
  <form role="form">
    <div class="box-body">
      <div class="form-group">
        <label >Unidade</label>
        <select name="unidade" class="form-control" id="unidade">
        <option disabled hidden selected> Selectionar </option>
           @foreach($unidades as $unidade)
                <option value="{{ $unidade->id }}">{{ $unidade->nome }}</option>
           @endforeach    
      </select>
     </div>
      <div class="form-group">
        <label >Especialidade</label>
        <select name="especialidade" class="form-control" id="especialidade">
          <option value="" selected disabled>Selecione a Especialidade</option>
      </select>
      </div>
      <div class="form-group">
        <label >Prestador </label>
        <select name="prestador" class="form-control" id="prestador">
          <option value="" selected disabled>Selecione o Prestador</option>
      </select>
      </div>
      <div id="calendar-container">
        <div id="calendario"></div>  
        <style>

          .green {
              background-color: green;
          }
          .red {
              background-color: red;
          }   
      </style>  
      </div>
    </div>
      <!-- /.box-body -->
    </form>
   </div>
  </div>
 <!-- /.FIM DO CALENDARIO -->
 <!-- /.INICIO TABELA AGENDA -->
 <div class="col-md-8" >
 
<!-- /MEIO -->
<div class="box box-primary" >
  <div class="box-header with-border">
    <div align="center">
    <h3 class="box-title">Horários Disponíveis</h3><br>
    <img src="{{ asset('img/spinner.gif') }}" id="spinner"></img>
    </div>
  </div>
  <!-- /.box-header -->
  
  <div class="box-body" id="horarios">
    <table class="table table-bordered text-center" id="tabelabuscarDados">
      <tr>
      </tr>
      <thead> 
        <tr>
        <!-- Lista da agenda JQuery -->
        </tr>
    </thead>
    </table>
  </div>
  <!-- /.box-body -->
  <div class="box-footer clearfix">
     <!-- /.Botões de Ação -->
     <button type="button" id="novoEncaixe" class="btn btn-primary btn-sm">Encaixe</button>
    {{--  <button type="button" id="novoCompromisso" class="btn btn-primary btn-sm">Compromisso</button> --}}
     <!-- /.Fim Botões de Ação -->
  </div>
</div>
<!-- /.box -->


<!-- /MEIO -->
   </div>
 </div>
 <!-- /.FIM DA TABELA AGENDA -->

  </div>
</div>
<!-- /.box -->
{{-- Fim da Agenda --}}
<script src="{{ asset('js/jquery.inputmask.js') }}"></script>
<script src="{{ asset('js/jquery.maskedinput.min.js') }}"></script>
<script src="{{ asset('js/functions/agenda/zabuto_calendar.min.js') }}"></script>
<script src="{{ asset('js/jquery.maskedinput.min.js') }}"></script>
<script src="{{ asset('js/functions/agenda/agendaZabuto.js') }}"></script>
<script src="{{ asset('js/functions/agenda/agenda.js') }}"></script>
<script src="{{ asset('js/functions/agenda/menu.js') }}"></script>
<script src="{{ asset('js/functions/agenda/atender.js') }}"></script>
<link rel="stylesheet" href="{{ asset('css/functions/agenda/jquery.contextMenu.min.css') }}">
<script src="{{ asset('js/functions/agenda/jquery.contextMenu.min.js') }}"></script>
<script src="{{ asset('js/functions/agenda/jquery.ui.position.js') }}"></script>
<script src="{{ asset('js/functions/agenda/encaixe.js') }}"></script>
<script src="{{ asset('js/select2.full.min.js') }}"></script>
<script>$('#tipoatendimento_atender').select2({ width: '50%'});</script>
<script>$('#procedimentos_atender').select2({ width: '60%'});</script>
<script>$('#selectPaciente').select2({ width: '100%'});</script>

@stop

