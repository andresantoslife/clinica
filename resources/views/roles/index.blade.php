@extends('adminlte::page')

@section('title', 'AAPVR - Clínica')

@section('content_header')
    <h1>Grupos</h1>
    <ol class="breadcrumb"> 
        <li><a href="/">Dashboard</a></li>
        <li><a href="">Grupos</a></li>
    </ol>
@stop

@section('content')
<div class="box">
 <div class="box-header"> 
{{-- MEIO --}}
<div class="row">
  <div class="col-lg-12 margin-tb">
      <div class="pull-left">
          <h2>Administrar Grupos</h2>
      </div>
      <div class="pull-right">
      @can('role-create')
          <a class="btn btn-success" href="{{ route('roles.create') }}"> Cadastrar</a>
          @endcan
      </div>
  </div>
</div>


@if ($message = Session::get('success'))
  <div class="alert alert-success">
      <p>{{ $message }}</p>
  </div>
@endif


<table class="table table-bordered">
<tr>
   <th>No</th>
   <th>Nome</th>
   <th width="280px">Ações</th>
</tr>
  @foreach ($roles as $key => $role)
  <tr>
      <td>{{ ++$i }}</td>
      <td>{{ $role->name }}</td>
      <td>
          <a class="btn btn-info" href="{{ route('roles.show',$role->id) }}">Visualizar</a>
          @can('role-edit')
              <a class="btn btn-primary" href="{{ route('roles.edit',$role->id) }}">Editar</a>
          @endcan
          @can('role-delete')
              {!! Form::open(['method' => 'DELETE','route' => ['roles.destroy', $role->id],'style'=>'display:inline']) !!}
                  {!! Form::submit('Deletar', ['class' => 'btn btn-danger']) !!}
              {!! Form::close() !!}
          @endcan
      </td>
  </tr>
  @endforeach
</table>


{!! $roles->render() !!}
{{-- FIM --}}
 </div>
</div>
@stop