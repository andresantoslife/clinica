<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>Categorias</title>
        <!--Custon CSS (está em /public/assets/site/css/certificate.css)-->
    </head>
    <body>

<h1>Categorias</h1>
<table>
<table class="table table-striped" >
      <tr>
         <th align="left">Nome</th>
      </tr>
      <?php foreach($categorias as $categoria) { ?>
        <tr>
            <td><?=$categoria['nome']?></td>
        </tr>
      <?php } ?>   
</table>
</html>
