@extends('adminlte::page')

@section('title', 'AAPVR - Clínica')

@section('content_header')
    <h1>Formulário de Cadastro :: Carater de Atendimento</h1>
    <ol class="breadcrumb"> 
        <li><a href="/">Dashboard</a></li>
        <li><a href="">Carater de Atendimento</a></li>
    </ol>
@stop

@section('content')
  
@foreach($errors->all() as $error)
<div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-ban"></i> Atenção</h4>
                {{ $error }}
              </div>
@endforeach


<div class="box">
 <div class="box-header">
 <a href="{{ route('carateratendimento.index') }}" class="btn btn-link"><span class="glyphicon glyphicon-chevron-left"></span> Voltar</a> 
 </div>

<div class="box-body">
<form action="/carateratendimento"  method="POST">
     @csrf
<div class="form-group row">
  <div class="col-xs-5">  
      <label>* Nome</label>
      <input type="text" class="form-control" id="nome" name="nome" required>
  </div>
  </div>

  <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-save"></span> Salvar</button>
</form>
</div>
@stop
