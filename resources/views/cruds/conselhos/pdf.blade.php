<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>Conselhos</title>
        <!--Custon CSS (está em /public/assets/site/css/certificate.css)-->
    </head>
    <body>

<h1>Conselhos</h1>
<table>
<table class="table table-striped" >
      <tr>
         <th align="left" width="80px" >Número</th>
         <th align="left" width="80px" >Sigla</th>
         <th align="left">Nome</th>
      </tr>
      <?php foreach($conselhos as $conselho) { ?>
        <tr>
            <td><?=$conselho['numero']?></td>
            <td><?=$conselho['sigla']?></td>
            <td><?=$conselho['nome']?></td>
        </tr>
      <?php } ?>   
</table>
</html>
