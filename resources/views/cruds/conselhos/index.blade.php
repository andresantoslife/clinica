@extends('adminlte::page')

@section('title', 'AAPVR - Clínica')

@section('content_header')
    <h1>Lista :: Conselhos</h1>
    <ol class="breadcrumb"> 
        <li><a href="/">Dashboard</a></li>
        <li><a href="">Conselhos</a></li>
    </ol>
@stop

@section('content')
<div class="box">
 <div class="box-header">
 
    <form action="{{ route('conselhos.pesquisar') }}"  method="POST" class="form-inline">
     @csrf
     <input type="number" name="numero" class="form-control" placeholder="Número">
     <input type="text" name="sigla" class="form-control" placeholder="Sigla">
     <input type="text" name="nome" class="form-control" placeholder="Nome">
     <button type="submit" class="btn btn-secondary"><span class="glyphicon glyphicon-search"></span> Pesquisar</button>
     <a href="{{ route('conselhos.create') }}" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span> Novo Registro</a>
     <a href="{{ route('conselhos.pdf') }}" class="btn btn-warning"><span class="glyphicon glyphicon-print"></span> Imprimir</a>
    </form>
 </div>

<div class="box-body">
@if(session('mensagem'))
<!-- Modal -->
<div class="modal modal-success fade" id="modal-success" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-body">
          <p align="center"> {{session('mensagem')}}</p>
      </div>
    </div>
  </div>
</div>
@endif
@if(session('warning'))
<!-- Modal -->
<div class="modal modal-warning fade" id="modal-success" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-body">
          <p align="center"><b>Atenção!</b> <br> {{session('warning')}}</p>
      </div>
    </div>
  </div>
</div>
@endif
<!-- Fim do Modal -->

    <table class="table table-striped">
      <tr>
         <th width="100px">Numero</th>
         <th width="100px">Sigla</th>
         <th>Nome</th>         
         <th width="100px">Ações</th>
      </tr>
    @foreach($conselhos as $conselho)
        <tr>
            <td><?=$conselho['numero']?></td>
            <td><?=$conselho['sigla']?></td>
            <td><?=$conselho['nome']?></td>
            
            <td>
                <a href="/conselhos/<?=$conselho['id']?>/edit">
                   <span class="glyphicon glyphicon-pencil"></span>
                </a>
                <a data-toggle="modal" data-target="#delete-modal<?=$conselho['id']?>" class="glyphicon glyphicon-trash">
            </td>
        </tr>
    <!-- Modal de Delete-->
<div class="modal fade" id="delete-modal<?=$conselho['id']?>" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Fechar"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modalLabel">Excluir Item</h4>
      </div>
      <div class="modal-body">
        Deseja realmente excluir este item?
      </div>
      <div class="modal-footer">
        
      <form action="/conselhos/<?=$conselho['id']?>"  method="POST">
        @csrf
        @method('DELETE')
        <button id="confirm" class="btn btn-primary" onclick="document.NameofUrForm.submit()">Sim</button>
        <button id="cancel" class="btn btn-default" data-dismiss="modal">N&atilde;o</button>
        </form>
      </div>
    </div>
  </div>
</div> <!-- /.modal -->
    @endforeach   
    </table>
    {!! $conselhos->links()!!}
    </div>
</div>
<script src="{{ asset('js/mensagem.js') }}"></script>
@stop