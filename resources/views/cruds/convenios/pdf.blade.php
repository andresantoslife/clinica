<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>Convênios</title>
        <!--Custon CSS (está em /public/assets/site/css/certificate.css)-->
    </head>
    <body>

<h1>Convênios</h1>
<table>
<table class="table table-striped" >
      <tr>
          <th align="left" width="100px" >Status</th>
         <th align="left" width="140px" >Registro ANS</th>
         <th align="left">Nome</th>
      </tr>
      <?php foreach($convenios as $convenio) { ?>
        <tr>
        <td><?=$convenio['status']?></td>
            <td><?=$convenio['regans']?></td>
            <td><?=$convenio['nome']?></td>
        </tr>
      <?php } ?>   
</table>
</html>
