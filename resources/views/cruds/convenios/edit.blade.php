@extends('adminlte::page')

@section('title', 'AAPVR - Clínica')

@section('content_header')
    <h1>Formulário de Edição :: Convênios</h1>
    <ol class="breadcrumb"> 
        <li><a href="/">Dashboard</a></li>
        <li><a href="">Convênios</a></li>
    </ol>
@stop

@section('content')
<?php 
//Somente um registro
$convenios = $convenios[0];
$tabelafaturamento_selects = $tabelafaturamento_selects[0];
?>

<div class="box">
 <div class="box-header">
 <a href="{{ route('convenios.index') }}" class="btn btn-link"><span class="glyphicon glyphicon-chevron-left"></span> Voltar</a> 
 </div>

<div class="box-body">
@if(session('mensagem'))
    <div class="alert alert-success">
        <p>{{session('mensagem')}}</p>
    </div>
@endif

<form action="/convenios/<?=$convenios->id?>"  method="POST">
     @csrf
     @method('PUT')
<div class="form-group row">
<div class="col-xs-2">    
          <label>Status</label>
          <select name="status" id="status" class="form-control">
                <?php if ($convenios->status == 'ativo') {?> 
                    <option value="<?=$convenios->status?>">Ativo</option> 
                    <option value="inativo">Inativo</option> 
                <?php } else { ?>  
                    <option value="<?=$convenios->status?>">Inativo</option> 
                    <option value="ativo">Ativo</option> 
                <?php }?> 
         </select> 
      </div> 
  <div class="col-xs-5">  
      <label>* Nome</label>
      <input type="text" class="form-control" id="nome" name="nome" value="<?=$convenios->nome; ?>" required>
  </div>
  </div>
<div class="form-group row">
   <div class="col-xs-3">    
      <label>* Registro Ans</label>
      <input type="number" class="form-control" id="regans" name="regans" value="<?=$convenios->regans; ?>" required>
  </div>
</div>
<div class="form-group row">
    <div class="col-xs-3">    
       <label>* Tabela Faturamento</label>
       <select class="form-control" id="tabela_faturamentos_id" name="tabela_faturamentos_id">
             <option selected value="<?=$tabelafaturamento_selects->id ?>"><?=$tabelafaturamento_selects->nome ?></option>
             @foreach($tabelafaturamentos as $tabelafaturamento)
             <option  value="<?=$tabelafaturamento->id ?>"><?=$tabelafaturamento->nome ?></option>
             @endforeach
       </select>
   </div>
</div>

  <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-save"></span> Salvar</button>
</form>
</div>
<script src="{{ asset('js/mensagem.js') }}"></script>
@stop
