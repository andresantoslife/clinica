<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>Unidades</title>
        <!--Custon CSS (está em /public/assets/site/css/certificate.css)-->
    </head>
    <body>

<h1>Unidades</h1>
<table>
<table class="table table-striped" >
      <tr>
         <th align="left" width="80px" >Registro ANS</th>
         <th align="left">Nome</th>
      </tr>
      <?php foreach($unidades as $unidade) { ?>
        <tr>
            <td><?=$unidade['regans']?></td>
            <td><?=$unidade['nome']?></td>
        </tr>
      <?php } ?>   
</table>
</html>
