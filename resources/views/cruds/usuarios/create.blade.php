@extends('adminlte::page')

@section('title', 'AAPVR - Clínica')

@section('content_header')
    <h1>Formulário de Cadastro :: Usuários</h1>
    <ol class="breadcrumb"> 
        <li><a href="/">Dashboard</a></li>
        <li><a href="">Usuários</a></li>
    </ol>
@stop

@section('content')
  
@foreach($errors->all() as $error)
<div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-ban"></i> Atenção</h4>
                {{ $error }}
              </div>
@endforeach


<div class="box">
 <div class="box-header">
 <a href="{{ route('usuarios.index') }}" class="btn btn-link"><span class="glyphicon glyphicon-chevron-left"></span> Voltar</a> 
 </div>

<div class="box-body">
<form action="/usuarios" method="POST" autocomplete="off">
     @csrf
<div class="form-group row">
  <div class="col-xs-5">  
      <label>* E-mail</label>
      <input type="email" class="form-control" id="email" name="email" required>
  </div>
  </div>
<div class="form-group row">
   <div class="col-xs-3">    
      <label>* Senha</label>
      <input type="password" class="form-control" id="password" name="password" required>
  </div>
</div>
<div class="form-group row">
  <div class="col-sm-6">
  <label> Grupos </label>
      <div class="form-group">
         <select class="form-control" id="grupos_id" name="grupos_id">
             @foreach($grupos as $grupo)
                  <option value="<?=$grupo['id']?>"><?=$grupo['nome']?></option>
             @endforeach
         </select>
      </div>                  
  </div>  
</div>
  <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-save"></span> Salvar</button>
</form>
</div>
@stop
