@extends('adminlte::page')

@section('title', 'AAPVR - Clínica')

@section('content_header')
    <h1>Formulário de Edição :: Usuários</h1>
    <ol class="breadcrumb"> 
        <li><a href="/">Dashboard</a></li>
        <li><a href="">Usuários</a></li>
    </ol>
@stop

@section('content')
<?php 
//Somente um registro
$usuarios = $usuarios[0];
?>

<div class="box">
 <div class="box-header">
 <a href="{{ route('usuarios.index') }}" class="btn btn-link"><span class="glyphicon glyphicon-chevron-left"></span> Voltar</a> 
 </div>

<div class="box-body">
@if(session('mensagem'))
    <div class="alert alert-success">
        <p>{{session('mensagem')}}</p>
    </div>
@endif

<form action="/usuarios/<?=$usuarios->id?>"  method="POST" autocomplete="off"> 
     @csrf
     @method('PUT')
<div class="form-group row">
  <div class="col-xs-5">  
      <label>E-mail</label>
      <input type="text" autocomplete="off" class="form-control" name="email" value="<?=$usuarios->email; ?>" required>
  </div>
  </div>
<div class="form-group row">
   <div class="col-xs-3">    
      <label>Trocar Senha</label>
      <input type="password" autocomplete="off" class="form-control" id="password" name="password">
  </div>
</div>
<div class="form-group row">
    <div class="col-sm-6">
    <label>Grupos </label>
        <div class="form-group">
            <select name="grupos_id" id="grupos_id" class="form-control">
                <option value="<?= $grupos_atual[0]->id ?>"> <?=$grupos_atual[0]->nome?> </option>   
                    @foreach($grupos as $grupo)
                        <option value="{{ $grupo->id }}">{{ $grupo->nome }}</option>
                    @endforeach       
            </select>
        </div>                  
    </div>  
  </div>
  <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-save"></span> Salvar</button>
</form>
</div>
<script src="{{ asset('js/mensagem.js') }}"></script>
@stop
