@extends('adminlte::page')

@section('title', 'AAPVR - Clínica')

@section('content_header')
    <h1>Formulário de Cadastro :: Prestadores</h1>
    <ol class="breadcrumb"> 
        <li><a href="/">Dashboard</a></li>
        <li><a href="">Prestadores</a></li>
    </ol>
@stop

@section('content')

@foreach($errors->all() as $error)
<div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-ban"></i> Atenção</h4>
                {{ $error }}
              </div>
@endforeach

<div class="box">
 <div class="box-header">
 <a href="{{ route('prestadores.index') }}" class="btn btn-link"><span class="glyphicon glyphicon-chevron-left"></span> Voltar</a> 
 </div>

<div class="box-body">
<form action="/prestadores"  method="POST">
     @csrf
<!-- Dados Pessoais -->
<div class="box box-primary">
   <div class="box-header with-border">
      <h3 class="box-title">Dados Pessoais</h3>
    </div>
  <div class="form-group row">
      <div class="col-xs-2">    
          <label>Status</label>
          <select name="status" id="status" class="form-control">
              <option value="ativo">Ativo</option> 
              <option value="inativo">Inativo</option> 
         </select> 
      </div> 
      <div class="col-xs-3">    
          <label>* Login</label>
          <select name="users_id" id="users_id" class="form-control">
                        @foreach($users as $user)
                            <option value="{{ $user->id }}">{{ $user->email }}</option>
                        @endforeach       
         </select>
      
      </div> 
      <div class="col-xs-5">    
          <label>* Nome</label>
          <input type="text" class="form-control" id="nome" name="nome" required>
      </div>
      <div class="col-xs-2">    
          <label>* CPF</label>
          <input type="number" class="form-control" id="cpf" name="cpf" required>
      </div>
  </div>
  <div class="form-group row">
      <div class="col-xs-2">  
          <label>* RG</label>
          <input type="number" class="form-control" id="rg" name="rg" required>
      </div>
    <div class="col-xs-2">  
        <label>* Nascimento</label>
        <input type="date" class="form-control" id="dtnascimento" name="dtnascimento" required>
    </div>
    <div class="col-xs-2">  
        <label>Telefone</label>
        <input type="text" class="form-control" id="telefone" name="telefone">
    </div>
    <div class="col-xs-2">  
        <label>Celular</label>
        <input type="text" class="form-control" id="celular" name="celular">
    </div>
    <div class="col-xs-2">  
        <label>Conselho</label>
                 <select name="conselhos_id" id="conselhos_id" class="form-control">
                        @foreach($conselhos as $conselho)
                            <option value="{{ $conselho->id }}">{{ $conselho->sigla }}</option>
                        @endforeach       
                    </select>
    </div>
    <div class="col-xs-2">  
        <label>Número do Conselho</label>
        <input type="number" class="form-control" id="nrconselho" name="nrconselho" required>
    </div>
  </div>
</div>
<!-- Fim Dados Pessoais -->
<!-- Endereço -->
<div class="box box-secondary">
   <div class="box-header with-border">
      <h3 class="box-title">Endereço</h3>
    </div>
  <div class="form-group row">
      <div class="col-xs-2">  
          <label>CEP</label>
          <input type="text" class="form-control" id="cep" name="cep">
      </div>
      <div class="col-xs-5">  
          <label>Logradouro</label>
          <input type="text" class="form-control" id="logradouro" name="logradouro">
      </div>
      <div class="col-xs-1">  
          <label>Número</label>
          <input type="number" class="form-control" id="numero" name="numero">
      </div>
      <div class="col-xs-3">  
          <label>Bairro</label>
          <input type="text" class="form-control" id="bairro" name="bairro">
      </div>
      
  </div>
  <div class="form-group row">
      <div class="col-xs-3">  
          <label>Cidade</label>
          <input type="text" class="form-control" id="cidade" name="cidade">
      </div>
      <div class="col-xs-5">  
          <label>Complemento</label>
          <input type="text" class="form-control" id="complemento" name="complemento">
      </div>
      <div class="col-xs-1">  
          <label>UF</label>
          <input type="text" class="form-control" id="uf" name="uf">
      </div>
  </div>
</div>
<!-- Fim Endereço -->

  <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-save"></span> Salvar</button>

</form>
</div>
<script src="{{ asset('js/jquery.maskedinput.min.js') }}"></script>
<script src="{{ asset('js/prestadorMask.js') }}"></script>
<script src="{{ asset('js/mensagem.js') }}"></script>
<script src="{{ asset('js/buscaCep.js') }}"></script>
<script> 
$(document).ready(function(){
    $('#users_id').select2();
  }); 
</script>
@stop
