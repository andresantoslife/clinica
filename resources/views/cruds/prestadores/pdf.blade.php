<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>Prestadores</title>
        <!--Custon CSS (está em /public/assets/site/css/certificate.css)-->
    </head>
    <body>

<h1>Prestadores</h1>
<table>
<table class="table table-striped" >
      <tr>
         <th align="left" width="100px" >CPF</th>
         <th align="left" width="100px" >Nascimento</th>
         <th align="left">Nome</th>
      </tr>
      <?php foreach($prestadores as $prestador) { ?>
        <tr>
            <td><?=$prestador['cpf']?></td>
            <td><?=date('d/m/Y', strtotime($prestador['dtnascimento']))?></td>
            <td><?=$prestador['nome']?></td>
        </tr>
      <?php } ?>   
</table>
</html>
