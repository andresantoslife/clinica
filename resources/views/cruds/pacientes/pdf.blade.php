<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>Pacientes</title>
        <!--Custon CSS (está em /public/assets/site/css/certificate.css)-->
    </head>
    <body>

<h1>Pacientes</h1>
<table>
<table class="table table-striped" >
      <tr>
         <th align="left" width="100px" >ID</th>
         <th align="left">Nome</th>
      </tr>
      <?php foreach($pacientes as $paciente) { ?>
        <tr>
            <td><?=$paciente['id']?></td>
            <td><?=$paciente['nome']?></td>
        </tr>
      <?php } ?>   
</table>
</html>
