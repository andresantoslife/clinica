@extends('adminlte::page')

@section('title', 'AAPVR - Clínica')

@section('content_header')
    <h1>Formulário de Cadastro :: Pacientes</h1>
    <ol class="breadcrumb"> 
        <li><a href="/">Dashboard</a></li>
        <li><a href="">Pacientes</a></li>
    </ol>
@stop

@section('content')

@foreach($errors->all() as $error)
<div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-ban"></i> Atenção</h4>
                {{ $error }}
              </div>
@endforeach

<div class="box">
 <div class="box-header">
 <a href="{{ route('pacientes.index') }}" class="btn btn-link"><span class="glyphicon glyphicon-chevron-left"></span> Voltar</a> 
 </div>

<div class="box-body">
<form action="/pacientes"  method="POST">
     @csrf
<!-- Dados Pessoais -->
<div class="box box-primary">
   <div class="box-header with-border">
      <h3 class="box-title">Dados Pessoais</h3>
    </div>
  <div class="form-group row">
      <div class="col-xs-2">    
          <label>Status</label>
          <select name="status" id="status" class="form-control">
              <option value="ativo">Ativo</option> 
              <option value="inativo">Inativo</option> 
         </select> 
      </div> 
      <div class="col-xs-5">    
          <label>* Nome</label>
          <input type="text" class="form-control" id="nome" name="nome" required>
      </div>
      <div class="col-xs-2">    
          <label>* CPF</label>
          <input type="number" class="form-control" id="cpf" name="cpf" required>
      </div>
      <div class="col-xs-2">  
          <label>* RG</label>
          <input type="number" class="form-control" id="rg" name="rg" required>
      </div>
  </div>
  <div class="form-group row">
    <div class="col-xs-2">   
      <label>Estado Civil</label>
          <select name="estadocivil" id="estadocivil" class="form-control">
              <option value="solteiro">Solteiro</option> 
              <option value="casado">Casado</option> 
              <option value="divorciado">Divorciado</option> 
              <option value="viuvo">Viúvo</option> 
              <option value="outros">Outros</option> 
         </select>  
    </div>
    <div class="col-xs-2">  
        <label>Nascimento</label>
        <input type="date" class="form-control" id="dtnascimento" name="dtnascimento" required>
    </div>
    <div class="col-xs-2"> 
          <label>Sexo</label>
          <select name="sexo" id="sexo" class="form-control">
              <option value="masculino">Masculino</option> 
              <option value="feminino">Feminino</option> 
         </select> 
    </div>
    <div class="col-xs-2">  
        <label>Telefone</label>
        <input type="text" class="form-control" id="telefone" name="telefone">
    </div>
    <div class="col-xs-2">  
        <label>Celular</label>
        <input type="text" class="form-control" id="celular" name="celular">
    </div>
  </div>
  <div class="form-group row">
      <div class="col-xs-4"> 
      <label>Nome da Mãe</label>
          <input type="text" class="form-control" id="mae" name="mae" required>
      </div>
    <div class="col-xs-3"> 
    <label>Categoria</label>
    <select name="categorias_id" id="categorias_id" class="form-control" required>
          @foreach($categorias as $categoria)
           <option value="<?=$categoria['id']?>"><?=$categoria['nome']?></option>
          @endforeach
   </select>
    </div>
</div>
</div>
<!-- Fim Dados Pessoais -->
<!-- Endereço -->
<div class="box box-secondary">
   <div class="box-header with-border">
      <h3 class="box-title">Endereço</h3>
    </div>
  <div class="form-group row">
      <div class="col-xs-2">  
          <label>CEP</label>
          <input type="text" class="form-control" id="cep" name="cep">
      </div>
      <div class="col-xs-5">  
          <label>Logradouro</label>
          <input type="text" class="form-control" id="logradouro" name="logradouro">
      </div>
      <div class="col-xs-1">  
          <label>Número</label>
          <input type="number" class="form-control" id="numero" name="numero">
      </div>
      <div class="col-xs-3">  
          <label>Bairro</label>
          <input type="text" class="form-control" id="bairro" name="bairro">
      </div>
      
  </div>
  <div class="form-group row">
      <div class="col-xs-3">  
          <label>Cidade</label>
          <input type="text" class="form-control" id="cidade" name="cidade">
      </div>
      <div class="col-xs-5">  
          <label>Complemento</label>
          <input type="text" class="form-control" id="complemento" name="complemento">
      </div>
      <div class="col-xs-1">  
          <label>UF</label>
          <input type="text" class="form-control" id="uf" name="uf">
      </div>
  </div>
</div>
<!-- Fim Endereço -->
<div class="box box-secondary">
   <div class="box-header with-border">
      <h3 class="box-title"></h3>
    </div>
  <div class="form-group row">
      <div class="col-sm-6">
         <label> Convênios </label>
          <div class="form-group">
                <select class="form-control" id="convenioSelect" name="convenio[]" multiple="multiple">
                    @foreach($convenios as $convenio)
                        <option value="<?=$convenio['id']?>"><?=$convenio['nome']?></option>
                    @endforeach
                </select>
            </div>                  
        </div>  
    </div>
</div>
<button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-save"></span> Salvar</button>

</form>
</div>
<script src="{{ asset('js/jquery.maskedinput.min.js') }}"></script>
<script src="{{ asset('js/mensagem.js') }}"></script>
<script src="{{ asset('js/buscaCep.js') }}"></script>
<script src="{{ asset('js/select2.full.min.js') }}"></script>
<script src="{{ asset('js/cruds/pacientes/pacienteMask.js') }}"></script>
<script>$('#convenioSelect').select2();</script>

@stop
