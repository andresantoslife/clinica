@extends('adminlte::page')

@section('title', 'AAPVR - Clínica')

@section('content_header')
    <h1>Formulário de Edição :: Pacientes</h1>
    <ol class="breadcrumb"> 
        <li><a href="/">Dashboard</a></li>
        <li><a href="">Pacientes</a></li>
    </ol>
@stop

@section('content')
<?php 
//Somente um registro
$pacientes = $pacientes[0];
$categoria_select = $categoria_select[0];
?>

<div class="box">
 <div class="box-header">
 <a href="{{ route('pacientes.index') }}" class="btn btn-link"><span class="glyphicon glyphicon-chevron-left"></span> Voltar</a> 
 </div>

<div class="box-body">
@if(session('mensagem'))
    <div class="alert alert-success">
        <p>{{session('mensagem')}}</p>
    </div>
@endif

<form action="/pacientes/<?=$pacientes->id?>"  method="POST">
     @csrf
     @method('PUT')
<!-- Dados Pessoais -->
<div class="box box-primary">
   <div class="box-header with-border">
      <h3 class="box-title">Dados Pessoais</h3>
    </div>
  <div class="form-group row">
      <div class="col-xs-2">    
      <label>Status</label>
          <select name="status" id="status" class="form-control">
                <?php if ($pacientes->status == 'ativo') {?> 
                    <option value="<?=$pacientes->status?>">Ativo</option> 
                    <option value="inativo">Inativo</option> 
                <?php } else { ?>  
                    <option value="<?=$pacientes->status?>">Inativo</option> 
                    <option value="ativo">Ativo</option> 
                <?php }?> 
         </select> 
      </div> 
      <div class="col-xs-5">  
          <input type="hidden" id="pacienteSelect" value="<?=$pacientes->id; ?>">   
          <label>* Nome</label>
          <input type="text" class="form-control" id="nome" name="nome" value="<?=$pacientes->nome; ?>" required>
      </div>
      <div class="col-xs-2">    
          <label>CPF</label>
          <input type="number" class="form-control" id="cpf" name="cpf" value="<?=$pacientes->cpf; ?>">
      </div>
      <div class="col-xs-2">  
          <label>RG</label>
          <input type="number" class="form-control" id="rg" name="rg" value="<?=$pacientes->rg; ?>" >
      </div>
  </div>
  <div class="form-group row">
    <div class="col-xs-2">   
      <label>Estado Civil</label>
         <select name="estadocivil" id="estadocivil" class="form-control">
                <?php if ($pacientes->estadocivil == 'solteiro') {?> 
                    <option value="solteiro" selected>Solteiro</option> 
                    <option value="casado">Casado</option> 
                    <option value="divorciado">Divorciado</option> 
                    <option value="viuvo">Viúvo</option> 
                    <option value="outros">Outros</option> 
                <?php } else ?> 
                <?php if ($pacientes->estadocivil == 'casado') {?>  
                    <option value="solteiro">Solteiro</option> 
                    <option value="casado" selected>Casado</option> 
                    <option value="divorciado">Divorciado</option> 
                    <option value="viuvo">Viúvo</option> 
                    <option value="outros">Outros</option> 
                <?php } else ?> 
                <?php if ($pacientes->estadocivil == 'divorciado') {?>  
                    <option value="solteiro" >Solteiro</option> 
                    <option value="casado">Casado</option> 
                    <option value="divorciado" selected>Divorciado</option> 
                    <option value="viuvo">Viúvo</option> 
                    <option value="outros">Outros</option> 
                <?php } else ?> 
                <?php if ($pacientes->estadocivil == 'viuvo') {?>  
                    <option value="solteiro" >Solteiro</option> 
                    <option value="casado">Casado</option> 
                    <option value="divorciado">Divorciado</option> 
                    <option value="viuvo" selected>Viúvo</option> 
                    <option value="outros">Outros</option> 
                <?php } else ?> 
                <?php if ($pacientes->estadocivil == 'outros') {?>  
                    <option value="solteiro">Solteiro</option> 
                    <option value="casado">Casado</option> 
                    <option value="divorciado">Divorciado</option> 
                    <option value="viuvo">Viúvo</option> 
                    <option value="outros" selected>Outros</option> 
                <?php }?> 
         </select> 
    </div>
    <div class="col-xs-2">  
        <label>Nascimento</label>
        <input type="date" class="form-control" id="dtnascimento" name="dtnascimento" value="<?=$pacientes->dtnascimento; ?>">
    </div>
    <div class="col-xs-2"> 
          <label>Sexo</label>
          <select name="sexo" id="sexo" class="form-control">
                <?php if ($pacientes->sexo == 'masculino') {?> 
                    <option value="masculino">Masculino</option> 
                    <option value="feminino">Feminino</option> 
                <?php } else { ?>  
                    <option value="feminino">Feminino</option> 
                    <option value="masculino">Masculino</option> 
                <?php }?> 
         </select> 
    </div>
    <div class="col-xs-2">  
        <label>Telefone</label>
        <input type="text" class="form-control" id="telefone" name="telefone" value="<?=$pacientes->telefone; ?>">
    </div>
    <div class="col-xs-2">  
        <label>Celular</label>
        <input type="text" class="form-control" id="celular" name="celular" value="<?=$pacientes->celular; ?>">
    </div>
  </div>
  <div class="form-group row">
      <div class="col-xs-4"> 
      <label>Nome da Mãe</label>
          <input type="text" class="form-control" id="mae" name="mae" value="<?=$pacientes->mae; ?>">
      </div>
      <div class="col-xs-3"> 
        <label>Categoria</label>
        <select name="categorias_id" id="categorias_id" class="form-control">
            <option selected value="<?=$categoria_select->idCat ?>"><?=$categoria_select->nomeCat ?></option>
              @foreach($categorias as $categoria)
               <option value="<?=$categoria->id?>"><?=$categoria->nome?></option>
              @endforeach
       </select>
       </div>
  </div>
</div>
<!-- Fim Dados Pessoais -->
<!-- Endereço -->
<div class="box box-secondary">
   <div class="box-header with-border">
      <h3 class="box-title">Endereço</h3>
    </div>
  <div class="form-group row">
      <div class="col-xs-2">  
          <label>CEP</label>
          <input type="text" class="form-control" id="cep" name="cep" value="<?=$pacientes->cep; ?>">
      </div>
      <div class="col-xs-5">  
          <label>Logradouro</label>
          <input type="text" class="form-control" id="logradouro" name="logradouro" value="<?=$pacientes->logradouro; ?>">
      </div>
      <div class="col-xs-1">  
          <label>Número</label>
          <input type="number" class="form-control" id="numero" name="numero" value="<?=$pacientes->numero; ?>">
      </div>
      <div class="col-xs-3">  
          <label>Bairro</label>
          <input type="text" class="form-control" id="bairro" name="bairro" value="<?=$pacientes->bairro; ?>">
      </div>
      
  </div>
  <div class="form-group row">
      <div class="col-xs-3">  
          <label>Cidade</label>
          <input type="text" class="form-control" id="cidade" name="cidade" value="<?=$pacientes->cidade; ?>">
      </div>
      <div class="col-xs-5">  
          <label>Complemento</label>
          <input type="text" class="form-control" id="complemento" name="complemento" value="<?=$pacientes->complemento; ?>">
      </div>
      <div class="col-xs-1">  
          <label>UF</label>
          <input type="text" class="form-control" id="uf" name="uf" value="<?=$pacientes->uf; ?>">
      </div>
  </div>
</div>

<!-- Fim Endereço -->
<div class="box box-secondary">
   <div class="box-header with-border">
   <br>
      <h3 class="box-title">Alterações em Tempo de Execução</h3>
    </div>
  <div class="form-group row">
      <div class="col-sm-4">
      <label> Convênios </label>
          <div class="form-group">
             <select class="form-control" id="convenios" name="convenio">
                <thead>  
                    
                </thead>
             </select>
          </div>                  
      </div>
      <div class="col-sm-3" align="center">
      <label> Ação</label>
          <div class="form-group">
          <button id="acoes" class="btn btn-default"><span class="glyphicon glyphicon-chevron-right"></span></button>
          </div>                  
      </div>

      <div class="col-sm-4" align="left">
      <label> Convênios do Paciente </label>
      
          <div class="form-group">
          <img src="{{ asset('img/spinner.gif') }}" id="spinner"></img>
          <div id="table">
                <table class="table table-bordered" id="tabelaConvenio">
                <tbody>
                    
                    </tbody>
                    <thead> 
                        <tr>
                        <!-- Lista de Convenios JQuery -->
                        </tr>
                    </thead>
                    </table>
                </div><!--id="table" -->
          </div>                  
      </div>
     
  </div>
</div>
<button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-save"></span> Salvar</button>

</form>
</div>
<script src="{{ asset('js/jquery.maskedinput.min.js') }}"></script>
<script src="{{ asset('js/mensagem.js') }}"></script>
<script src="{{ asset('js/buscaCep.js') }}"></script>
<script src="{{ asset('js/select2.full.min.js') }}"></script>
<script src="{{ asset('js/cruds/pacientes/pacienteMask.js') }}"></script>
<script src="{{ asset('js/cruds/pacientes/pacienteConvenio.js') }}"></script>
@stop
