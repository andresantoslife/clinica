<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>Especialidades</title>
        <!--Custon CSS (está em /public/assets/site/css/certificate.css)-->
    </head>
    <body>

<h1>Especialidades</h1>
<table>
<table class="table table-striped" >
      <tr>
         <th align="left" width="80px" >Código</th>
         <th align="left">Nome</th>
      </tr>
      <?php foreach($especialidades as $especialidade) { ?>
        <tr>
            <td><?=$especialidade['codigo']?></td>
            <td><?=$especialidade['nome']?></td>
        </tr>
      <?php } ?>   
</table>
</html>
