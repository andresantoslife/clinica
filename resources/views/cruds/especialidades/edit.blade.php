@extends('adminlte::page')

@section('title', 'AAPVR - Clínica')

@section('content_header')
    <h1>Formulário de Edição :: Especialidades</h1>
    <ol class="breadcrumb"> 
        <li><a href="/">Dashboard</a></li>
        <li><a href="">Especialidades</a></li>
    </ol>
@stop

@section('content')
<?php 
//Somente um registro
$especialidades = $especialidades[0];
?>

<div class="box">
 <div class="box-header">
 <a href="{{ route('especialidades.index') }}" class="btn btn-link"><span class="glyphicon glyphicon-chevron-left"></span> Voltar</a> 
 </div>

<div class="box-body">
@if(session('mensagem'))
    <div class="alert alert-success">
        <p>{{session('mensagem')}}</p>
    </div>
@endif

<form action="/especialidades/<?=$especialidades->id?>"  method="POST">
     @csrf
     @method('PUT')
<div class="form-group row">
  <div class="col-xs-3">  
      <label>* Código</label>
      <input type="number" class="form-control" id="codigo" name="codigo" value="<?=$especialidades->codigo; ?>" required>
  </div>
  </div>
<div class="form-group row">
   <div class="col-xs-5">    
      <label>* Nome</label>
      <input type="text" class="form-control" id="nome" name="nome" value="<?=$especialidades->nome; ?>" required>
  </div>
</div>
  <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-save"></span> Salvar</button>
</form>
</div>
<script src="{{ asset('js/mensagem.js') }}"></script>
@stop
