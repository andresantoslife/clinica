@extends('adminlte::page')

@section('title', 'AAPVR - Clínica')

@section('content_header')
    <h1>Formulário de Edição :: Indicação de Acidente</h1>
    <ol class="breadcrumb"> 
        <li><a href="/">Dashboard</a></li>
        <li><a href="">Indicação de Acidente</a></li>
    </ol>
@stop

@section('content')
<?php 
//Somente um registro
$indicacaoacidentes = $indicacaoacidentes[0];
?>

<div class="box">
 <div class="box-header">
 <a href="{{ route('indicacaoacidente.index') }}" class="btn btn-link"><span class="glyphicon glyphicon-chevron-left"></span> Voltar</a> 
 </div>

<div class="box-body">
@if(session('mensagem'))
    <div class="alert alert-success">
        <p>{{session('mensagem')}}</p>
    </div>
@endif

<form action="/indicacaoacidente/<?=$indicacaoacidentes->id?>"  method="POST">
     @csrf
     @method('PUT')
<div class="form-group row">
  <div class="col-xs-5">  
      <label>* Nome</label>
      <input type="text" class="form-control" id="nome" name="nome" value="<?=$indicacaoacidentes->nome; ?>" required>
  </div>
  </div>
<button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-save"></span> Salvar</button>
</form>
</div>
<script src="{{ asset('js/mensagem.js') }}"></script>
@stop
