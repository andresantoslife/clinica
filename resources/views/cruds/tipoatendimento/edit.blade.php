@extends('adminlte::page')

@section('title', 'AAPVR - Clínica')

@section('content_header')
    <h1>Formulário de Edição :: Tipo de Atendimento</h1>
    <ol class="breadcrumb"> 
        <li><a href="/">Dashboard</a></li>
        <li><a href="">Tipo de Atendimento</a></li>
    </ol>
@stop

@section('content')
<?php 
//Somente um registro
$tipoatendimentos = $tipoatendimentos[0];
?>

<div class="box">
 <div class="box-header">
 <a href="{{ route('tipoatendimento.index') }}" class="btn btn-link"><span class="glyphicon glyphicon-chevron-left"></span> Voltar</a> 
 </div>

<div class="box-body">
@if(session('mensagem'))
    <div class="alert alert-success">
        <p>{{session('mensagem')}}</p>
    </div>
@endif

<form action="/tipoatendimento/<?=$tipoatendimentos->id?>"  method="POST">
     @csrf
     @method('PUT')
<div class="form-group row">
  <div class="col-xs-5">  
      <label>* Nome</label>
      <input type="text" class="form-control" id="nome" name="nome" value="<?=$tipoatendimentos->nome; ?>" required>
  </div>
  </div>
<button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-save"></span> Salvar</button>
</form>
</div>
<script src="{{ asset('js/mensagem.js') }}"></script>
@stop
