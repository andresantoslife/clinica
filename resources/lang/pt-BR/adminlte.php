<?php

return [
    'register_message' => 'Registrar novo usuário',
    'register_a_new_membership' => 'Registrar novo usuário',
    'i_already_have_a_membership' => 'Já tenho usuário!',
];
