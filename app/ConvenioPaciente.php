<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConvenioPaciente extends Model
{
    protected $fillable = [
        'pacientes_id',
        'convenios_id'
    ];
}
