<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoBloqueio extends Model
{
    protected $fillable = [
        'nome'
    ];
}
