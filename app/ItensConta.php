<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItensConta extends Model
{
    protected $fillable = [
        'valor_procedimentos_id',
        'conta_pacientes_id'
    ];
}
