<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Agenda extends Model
{
    protected $fillable = [
        'dtagenda',
        'observacao',
        'pacientes_id',
        'expedientes_id',
        'situacao_agendas_id',
        'tipo_agendas_id',
        'bloqueio_agendas_id',
    ];
}
