<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prestador extends Model
{
    protected $fillable = [
        'nome',
        'cpf',
        'rg',
        'dtnascimento',
        'cep',
        'logradouro',
        'numero',
        'bairro',
        'cidade',
        'complemento',
        'uf',
        'telefone',
        'celular',
        'conselhos_id',
        'nrconselho',
        'users_id',
        'status'
    ];
    
    public function conselho() {
         return $this->hasOne(Conselho::class, 'id', 'conselhos_id');
    }
}
