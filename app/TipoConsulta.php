<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoConsulta extends Model
{
    protected $fillable = [
        'nome'
    ];
}
