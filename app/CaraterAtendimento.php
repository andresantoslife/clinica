<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CaraterAtendimento extends Model
{
    protected $fillable = [
        'nome'
    ];
}
