<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContaPaciente extends Model
{
    protected $fillable = [
        'pacientes_id',
        'dtfechamento',
        'status',
        'atendimentos_id'
    ];
}
