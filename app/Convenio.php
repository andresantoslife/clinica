<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Convenio extends Model
{
    protected $fillable = [
        'nome',
        'regans',
        'status',
        'tabela_faturamentos_id'
    ];
}
