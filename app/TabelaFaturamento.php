<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TabelaFaturamento extends Model
{
    protected $fillable = ['nome'];
}
