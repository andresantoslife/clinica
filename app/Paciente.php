<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Paciente extends Model
{
    protected $fillable = [
        'nome',
        'cpf',
        'rg',
        'dtnascimento',
        'mae',
        'sexo',
        'estadocivil',
        'cep',
        'logradouro',
        'numero',
        'bairro',
        'cidade',
        'complemento',
        'uf',
        'telefone',
        'celular',
        'status',
        'categorias_id'
    ];
    
    public function categorias()
       {
           return $this->belongsToMany(PacienteCategoria::class, 'paciente_categorias', 'pacientes_id', 'categorias_id');
       }
    
}
