<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SituacaoAgenda extends Model
{
    protected $fillable = [
        'nome'
    ];
}
