<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Expediente extends Model
{
    protected $fillable = [
        'dtinicio',
        'dtfinal',
        'hinicio',
        'hfinal',
        'intervalo',
        'semana',
        'alocacaos_id'
    ];
}
