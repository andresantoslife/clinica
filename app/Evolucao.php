<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Evolucao extends Model
{
    protected $fillable = [
        'descricao',
        'atendimentos_id'
    ];
}
