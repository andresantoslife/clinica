<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Alocacao extends Model
{
    protected $fillable = [
        'unidades_id',
        'prestadors_id',
        'especialidades_id'
    ];
}
