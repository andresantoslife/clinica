<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Anamnese extends Model
{
    protected $fillable = [
        'descricao',
        'atendimentos_id'
    ];
}
