<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoAgenda extends Model
{ 
    protected $fillable = [
        'nome'
    ];
}
