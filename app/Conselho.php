<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Conselho extends Model
{
    protected $fillable = [
        'numero',
        'sigla',
        'nome'
    ];
}

