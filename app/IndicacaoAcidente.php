<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IndicacaoAcidente extends Model
{
    protected $fillable = [
        'nome'
    ];
}
