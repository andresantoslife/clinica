<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PedidoExame extends Model
{
    protected $fillable = [
        'observacao',
        'dataExame',
        'exames_id',
        'prestadors_id',
        'atendimentos_id',
        'pacientes_id'
    ];
}
