<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoAtendimento extends Model
{
    protected $fillable = [
        'nome'
    ];
}
