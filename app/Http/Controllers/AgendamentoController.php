<?php

namespace App\Http\Controllers;
use App\Expediente;
use App\Alocacao;
use App\Unidade;
use App\Agenda;
use App\TipoAgenda;
use App\Paciente;
use App\FaltaPaciente;
use DB;
use Illuminate\Http\Request;

class AgendamentoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $unidades = Unidade::all();  
        $tipoagendas = TipoAgenda::all();
        $pacientes = Paciente::all();
        return view('functions/agenda/index',compact('unidades','tipoagendas','pacientes',$unidades,$tipoagendas,$pacientes));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function listaragendaespecialidade($unidade) {
        $especialidade = DB::select("select especialidades.id, especialidades.nome from especialidades join alocacaos on alocacaos.especialidades_id = especialidades.id where alocacaos.unidades_id = ?",[$unidade]);
        return response()->json($especialidade);
    }

    public function listaragendaprestador($unidade, $especialidade) {
        $prestador = DB::select("select prestadors.id, prestadors.nome from prestadors join alocacaos on alocacaos.prestadors_id = prestadors.id where alocacaos.unidades_id = ? and alocacaos.especialidades_id = ?",[$unidade, $especialidade]);
        return response()->json($prestador);
    }

    public function listarcalendario($unidade, $prestador, $especialidade){
        //URI 
        //listarcalendario/{unidade}/prestador/{prestador}/especialidade/{especialidade}
        // CASO O PACIENTE NAO ESTE NO CAMPO HLIVRE, E SE ESTIVER VAI COMO OCUPADO 
        $calendarios = DB::select("
        select date_format(agendas.dtagenda, '%Y-%m-%d') as date,
        case when agendas.situacao_agendas_id = '4' then 'green' else 'red' end AS classname,
        SUM(CASE WHEN agendas.situacao_agendas_id <> '4' THEN 1 END) AS red,
        SUM(CASE WHEN agendas.situacao_agendas_id = '4' THEN 1 END) AS green
        from agendas 
        join expedientes on expedientes.id = agendas.expedientes_id 
        join alocacaos on alocacaos.id = expedientes.alocacaos_id
        where 
        alocacaos.unidades_id = ? 
        and alocacaos.prestadors_id = ?
        and alocacaos.especialidades_id = ? group by date",[$unidade, $prestador, $especialidade]);    
        
        foreach($calendarios as $calendario) {
            $total = $calendario->red + $calendario->green;
            $red = $calendario->red;
            $green = $calendario->green;
            $red == null ? $red ='0' : $red;
             
            if ($red != $total) {
              $calendarioJson[] = [
                 'date' =>  $calendario->date,
                 'classname' => 'green',
                 'title' => $calendario->green . ' Horários Livres',
                 'green' => $red,
                 'total' => $total
              ]; } else {
                $calendarioJson[] = [
                    'date' =>  $calendario->date,
                    'classname' => 'red',
                    'title' => 'Agenda Cheia!',
                    'red' => $red,
                    'total' => $total
                   
                 ];
            } 
        }
        if (empty($calendarioJson)) { $calendarioJson = null; } else {$calendarioJson;}
        return response()->json($calendarioJson);
    }

    public function buscardadosagenda($unidade, $especialidade , $prestador, $data) {
        //URI buscardadosagenda/{unidade}/especialidade/{especialidade}/prestador/{prestador}/data/{data}
        $dadosdaAgenda = DB::select("select DATE_FORMAT(agendas.dtagenda, '%H:%i') as horario, 
        (SELECT pacientes.nome FROM pacientes WHERE pacientes.id = agendas.pacientes_id LIMIT 0, 1) AS paciente,
        (SELECT tipo_agendas.nome FROM tipo_agendas WHERE tipo_agendas.id = agendas.tipo_agendas_id LIMIT 0, 1) AS tipo,
        (SELECT situacao_agendas.nome FROM situacao_agendas WHERE situacao_agendas.id = agendas.situacao_agendas_id LIMIT 0, 1) AS situacao,
        agendas.id as id
        from agendas 
        join expedientes on expedientes.id = agendas.expedientes_id 
        join alocacaos on alocacaos.id = expedientes.alocacaos_id
        where 
            alocacaos.unidades_id = ?
            and alocacaos.prestadors_id = ?
            and alocacaos.especialidades_id = ?
            and DATE(agendas.dtagenda) = ?",[$unidade, $prestador, $especialidade, $data]);    
        return response()->json($dadosdaAgenda);
  
    }

    public function limpar($id) {
        DB::update("UPDATE agendas SET observacao = '', pacientes_id = null, situacao_agendas_id = '4', tipo_agendas_id = null, bloqueio_agendas_id = null where id = ?", [$id]);
    }

    public function confirmar($id) {
        $pacientes = DB::SELECT("select pacientes_id as id from agendas where id = ?", [$id]);
        foreach($pacientes as $paciente) {
            if ($paciente->id == null) {
            } else {
                DB::update("UPDATE agendas SET situacao_agendas_id = '7' where id = ?", [$id]);
            }
        }
    }

    public function bloquear($id) {
        DB::update("UPDATE agendas SET observacao = 'Horário Bloqueado', pacientes_id = null, situacao_agendas_id = '6', tipo_agendas_id = null, bloqueio_agendas_id = null where id = ?", [$id]);
    }

    public function agendar($id, $paciente, $tipo) {
        DB::update("UPDATE agendas SET pacientes_id = ?, situacao_agendas_id = '1', tipo_agendas_id = ? where id = ?", [$paciente, $tipo, $id]);
    }

    public function encaixe(Request $request){
        $unidade = $request->unidade;
        $especialidade = $request->especialidade;
        $prestador = $request->prestador;
        $dataAgenda = $request->dataAgenda;
        $encaixeHorario = $request->encaixeHorario;
        $encaixePaciente = $request->encaixePaciente;
        $encaixeTipoConsulta = $request->encaixeTipoConsulta;
        //SELECT DATABASE
        $dbalocacao = DB::select("SELECT id FROM alocacaos WHERE unidades_id = ? and prestadors_id = ? and especialidades_id = ?",[$unidade,$prestador,$especialidade,]);
        $idAlocacao = $dbalocacao[0]->id;  
        $dbexpediente = DB::select("SELECT id from expedientes where alocacaos_id = ? order by id desc",[$idAlocacao]);
        $idExpediente = $dbexpediente[0]->id;  //analisar pega ultimo id
        $dataEncaixe = $dataAgenda . " " . $encaixeHorario . ":00";

        $encaixe =  [
              'dtagenda' => $dataEncaixe,
              'observacao' => 'Encaixe de Paciente',
              'pacientes_id' => $encaixePaciente,
              'expedientes_id' => $idExpediente,
              'situacao_agendas_id' => '1',
              'tipo_agendas_id' => $encaixeTipoConsulta 
        ];
        $agenda = Agenda::UpdateOrCreate($encaixe);
        return $agenda;
    }

    public function ausentaragenda(Request $request){
        $idAgenda = $request->agendas_id;
        $pacientes = DB::SELECT("select pacientes_id as id from agendas where id = ?", [$idAgenda]);
         foreach($pacientes as $paciente) {
            if ($paciente->id == null) {   
            } else {
                $paciente_id =  $paciente->id;
                $observacao = $request->observacao_modal;
                DB::update("UPDATE agendas SET pacientes_id = null, situacao_agendas_id = '4', tipo_agendas_id = null where id = ?", [$idAgenda]);
                DB::insert("insert into falta_pacientes (pacientes_id, agendas_id, observacao) values (?, ?, ?)",[$paciente_id, $idAgenda, $observacao]); 
            }
        }  

    }
}
