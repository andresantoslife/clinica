<?php

namespace App\Http\Controllers;
use App\Expediente;
use App\Alocacao;
use App\Unidade;
use DB;
use Illuminate\Http\Request;
use App\Repositories\ExpedienteRepository;
use Illuminate\Database\QueryException;

class ExpedienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
     $unidades = Unidade::all();       
     return view('functions/expediente/index',compact('unidades',$unidades));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

     public function listarprestador($unidades){
        $prestadores = DB::select("SELECT prestadors.id,prestadors.nome FROM prestadors join alocacaos on alocacaos.prestadors_id = prestadors.id WHERE unidades_id = ? GROUP BY prestadors.id",[$unidades]);
        return response()->json($prestadores);
    }

    public function expedientepost(Request $request, ExpedienteRepository $repository) {
       $repository->salvar($request);
    }


    public function expedientedel(Request $request, ExpedienteRepository $repository) {
        try {
          $repository->deletar($request->expediente);
        } catch (QueryException $e) {      
            return response()->json(['warning' => 'Este registro possuí vínculos!'],500);
        }    
     }

    public function listarexpediente($unidade, $prestador, $especialidade, ExpedienteRepository $repository){
    //URI listarexpediente/{unidade}/prestador/{prestador}/especialidade/{especialidade}    
    $alocacaos = DB::table('alocacaos')->where('unidades_id', '=', $unidade)->where('prestadors_id', '=', $prestador)->where('especialidades_id', '=', $especialidade)->get(); 
    $array = $alocacaos->toArray();
    $alocacao = $array[0]->id;
    $expediente = DB::select("select 
    expedientes.id as expediente,
    prestadors.nome as prestador,
    unidades.nome as unidade,
    alocacaos.id as alocacao,
    especialidades.nome as especialidade,
    case expedientes.semana
         when 1 then 'Segunda'
         when 2 then 'Terça'
         when 3 then 'Quarta' 
         when 4 then 'Quinta'
         when 5 then 'Sexta' 
         when 6 then 'Sábado' 
         when 0 then 'Domingo' 
         end as semana, 
         DATE_FORMAT(expedientes.dtinicio, '%d/%m/%Y' ) AS dtinicio,  
         DATE_FORMAT(expedientes.dtfinal, '%d/%m/%Y' ) AS dtfinal,
         expedientes.hinicio, 
         expedientes.hfinal, 
         expedientes.intervalo
         from expedientes 
         join alocacaos on alocacaos.id = expedientes.alocacaos_id 
         join especialidades on especialidades.id = alocacaos.especialidades_id
         join prestadors on prestadors.id = alocacaos.prestadors_id
         join unidades on unidades.id = alocacaos.unidades_id
         where alocacaos.id = ?",[$alocacao]);
    return response()->json($expediente);
    }

    
}
