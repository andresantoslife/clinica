<?php

namespace App\Http\Controllers;
use App\TipoAtendimento;
use Illuminate\Http\Request;
use App\Http\Requests\TipoAtendimentoRequest;
use App\Repositories\TipoAtendimentoRepository;
use Illuminate\Database\QueryException;
class TipoAtendimentoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function pesquisar(Request $request, TipoAtendimentoRepository $repository)
    {
        $totalPage = 10;
        $tipoatendimentos = $repository->pesquisar($request->nome, $totalPage);
        return view('cruds/tipoatendimento/index')->with('tipoatendimentos', $tipoatendimentos);
    }

    public function index(TipoAtendimentoRepository $repository)
    {
        $tipoatendimentos = $repository->paginacao(10);
        return view('cruds/tipoatendimento/index')->with('tipoatendimentos', $tipoatendimentos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('cruds/tipoatendimento/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TipoAtendimentoRequest $request, TipoAtendimentoRepository $repository)
    {
        $repository->salvar($request);
        return redirect()
            ->action('TipoAtendimentoController@index')
            ->with('mensagem', 'Registro criado');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, TipoAtendimentoRepository $repository)
    {
        $tipoatendimentos = $repository->buscarid($id);
        if(!empty($tipoatendimentos)) {
            return view('cruds/tipoatendimento/edit')->with('tipoatendimentos',$tipoatendimentos);
        } else {
            return redirect()->action('TipoAtendimentoController@index'); 
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request, TipoAtendimentoRepository $repository)
    {
        $repository->buscarid($id);
        $repository->update($id, $request);
        return redirect()->action('TipoAtendimentoController@index')->with('mensagem', 'O registro foi atualizado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, TipoAtendimentoRepository $repository)
    {
        $repository->buscarid($id);
        try {
        $repository->deletar($id);
        return redirect()->action('TipoAtendimentoController@index')->with('mensagem', 'O registro foi excluido');  
          } catch (QueryException $e) {
            return redirect()->action('TipoAtendimentoController@index')->with('warning', 'O registro pois possui relacionamentos!');
          }
    }

    public function pdf(TipoAtendimentoRepository $repository){
        $tipoatendimentos = TipoAtendimento::all();
        return \PDF::loadView('cruds.tipoatendimento.pdf', compact('tipoatendimentos'))->stream('cadastro_atendimento.pdf');
    }
}
