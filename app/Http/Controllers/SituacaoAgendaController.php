<?php

namespace App\Http\Controllers;
use App\SituacaoAgenda;
use Illuminate\Http\Request;
use App\Http\Requests\SituacaoAgendaRequest;
use App\Repositories\SituacaoAgendaRepository;
use Illuminate\Database\QueryException;
class SituacaoAgendaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function pesquisar(Request $request, SituacaoAgendaRepository $repository)
    {
        $totalPage = 10;
        $situacaoagendas = $repository->pesquisar($request->nome, $totalPage);
        return view('cruds/situacaoagenda/index')->with('situacaoagendas', $situacaoagendas);
    }



    public function index(SituacaoAgendaRepository $repository)
    {
        $situacaoagendas = $repository->paginacao(10);
        return view('cruds/situacaoagenda/index')->with('situacaoagendas', $situacaoagendas);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('cruds/situacaoagenda/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SituacaoAgendaRequest $request, SituacaoAgendaRepository $repository)
    {
        $repository->salvar($request);
        return redirect()
            ->action('SituacaoAgendaController@index')
            ->with('mensagem', 'Registro criado');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, SituacaoAgendaRepository $repository)
    {
        $situacaoagendas = $repository->buscarid($id);
        if(!empty($situacaoagendas)) {
            return view('cruds/situacaoagenda/edit')->with('situacaoagendas',$situacaoagendas);
        } else {
            return redirect()->action('SituacaoAgendaController@index'); 
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request, SituacaoAgendaRepository $repository)
    {
        $repository->buscarid($id);
        $repository->update($id, $request);
        return redirect()->action('SituacaoAgendaController@index')->with('mensagem', 'O registro foi atualizado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, SituacaoAgendaRepository $repository)
    {
        $repository->buscarid($id);
        try {
        $repository->deletar($id);
        return redirect()->action('SituacaoAgendaController@index')->with('mensagem', 'O registro foi excluido');  
          } catch (QueryException $e) {
            return redirect()->action('SituacaoAgendaController@index')->with('warning', 'O registro pois possui relacionamentos!');
          }  
    }

    public function pdf(SituacaoAgendaRepository $repository){
        $situacaoagendas = SituacaoAgenda::all();
        return \PDF::loadView('cruds.situacaoagenda.pdf', compact('situacaoagendas'))->stream('cadastro_situacaoagenda.pdf');
    }
}
