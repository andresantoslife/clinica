<?php

namespace App\Http\Controllers;
use App\CaraterAtendimento;
use Illuminate\Http\Request;
use App\Http\Requests\CaraterAtendimentoRequest;
use App\Repositories\CaraterAtendimentoRepository;
use Illuminate\Database\QueryException;

class CaraterAtendimentoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function pesquisar(Request $request, CaraterAtendimentoRepository $repository)
    {
        $totalPage = 10;
        $carateratendimentos = $repository->pesquisar($request->nome, $totalPage);
        return view('cruds/carateratendimento/index')->with('carateratendimentos', $carateratendimentos);
    }

    public function index(CaraterAtendimentoRepository $repository)
    {
        $carateratendimentos = $repository->paginacao(10);
        return view('cruds/carateratendimento/index')->with('carateratendimentos', $carateratendimentos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('cruds/carateratendimento/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CaraterAtendimentoRequest $request, CaraterAtendimentoRepository $repository)
    {
        $repository->salvar($request);
        return redirect()
            ->action('CaraterAtendimentoController@index')
            ->with('mensagem', 'Registro criado');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, CaraterAtendimentoRepository $repository)
    {
        $carateratendimentos = $repository->buscarid($id);
        if(!empty($carateratendimentos)) {
            return view('cruds/carateratendimento/edit')->with('carateratendimentos',$carateratendimentos);
        } else {
            return redirect()->action('CaraterAtendimentoController@index'); 
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request, CaraterAtendimentoRepository $repository)
    {
        $repository->buscarid($id);
        $repository->update($id, $request);
        return redirect()->action('CaraterAtendimentoController@index')->with('mensagem', 'O registro foi atualizado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, CaraterAtendimentoRepository $repository)
    {
        $repository->buscarid($id);
        try {
        $repository->deletar($id);
        return redirect()->action('CaraterAtendimentoController@index')->with('mensagem', 'O registro foi excluido');  
          } catch (QueryException $e) {
            return redirect()->action('CaraterAtendimentoController@index')->with('warning', 'O registro pois possui relacionamentos!');
          }    
    }

    public function pdf(CaraterAtendimentoRepository $repository){
        $carateratendimentos = CaraterAtendimento::all();
        return \PDF::loadView('cruds.carateratendimento.pdf', compact('carateratendimentos'))->stream('cadastro_carateratendimento.pdf');
    }
}
