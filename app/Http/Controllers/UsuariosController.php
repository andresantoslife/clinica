<?php

namespace App\Http\Controllers;
use App\Repositories\UsuariosRepository;
use Illuminate\Http\Request;
use App\Grupo;
use App\User;
use Illuminate\Database\QueryException;
use DB;

class UsuariosController extends Controller
{
    public function pesquisar(Request $request, UsuariosRepository $repository)
    {
        $usuarios = DB::select("select grupos.nome as grupos_nome, users.* from users 
        join grupos on grupos.id = users.grupos_id where users.email like '%$request->email%'");
        return view('cruds/usuarios/index')->with('usuarios', $usuarios);
    }

    public function index(UsuariosRepository $repository)
    {
        $usuarios = DB::select("select grupos.nome as grupos_nome, users.* from users 
        join grupos on grupos.id = users.grupos_id");
        //$usuarios = $repository->paginacao(10);
        return view('cruds/usuarios/index',compact('usuarios', $usuarios));
    }
    
    public function create()
    {
        $grupos = Grupo::all();
        return view('cruds/usuarios/create',compact('grupos', $grupos));
    }

    public function store(Request $request, UsuariosRepository $repository)
    {
        $repository->salvar($request);
        return redirect()
            ->action('UsuariosController@index')
            ->with('mensagem', 'Registro criado');
    }
    public function show($id)
    {
       
    }

    public function edit($id, UsuariosRepository $repository)
    {
        $usuarios = DB::select('select * from users where id = ?',[$id]);
        $grupos_atual = DB::select('select * from grupos where id =?' ,[$usuarios[0]->grupos_id]);
        //Usuarios  
        $grupos= DB::select('select * from grupos where id <> ?',[$grupos_atual[0]->id]);
        
        if(!empty($usuarios)) {
           return view('cruds/usuarios/edit',compact('usuarios','grupos_atual','grupos', $usuarios, $grupos_atual, $grupos));
        } else {
           return redirect()->action('UsuariosController@index'); 
        }
    }

    public function update($id, Request $request, UsuariosRepository $repository)
    {
        $repository->buscarid($id);
        $repository->update($id, $request);
        return redirect()->action('UsuariosController@index')->with('mensagem', 'O registro foi atualizado');
    }

    public function destroy($id, UsuariosRepository $repository)
    {
        $repository->buscarid($id);
        try {
        $repository->deletar($id);
        return redirect()->action('UsuariosController@index')->with('mensagem', 'O registro foi excluido');  
          } catch (QueryException $e) {
            return redirect()->action('UsuariosController@index')->with('warning', 'O registro pois possui relacionamentos!');
          } 
    }

}
