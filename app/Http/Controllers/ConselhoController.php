<?php

namespace App\Http\Controllers;
use App\Conselho;
use Illuminate\Http\Request;
use App\Http\Requests\ConselhoRequest;
use App\Repositories\ConselhoRepository;
use Illuminate\Database\QueryException;
class ConselhoController extends Controller
{
 
    public function pesquisar(Request $request, ConselhoRepository $repository)
    {
        $totalPage = 10;
        $conselhos = $repository->pesquisar($request->numero, $request->sigla,$request->nome, $totalPage);
        return view('cruds/conselhos/index')->with('conselhos', $conselhos);
    }

    public function index(ConselhoRepository $repository)
    {
        $conselhos = $repository->paginacao(10);
        return view('cruds/conselhos/index')->with('conselhos', $conselhos);
    }
    public function create()
    {
        return view('cruds/conselhos/create');
    }

    public function store(ConselhoRequest $request, ConselhoRepository $repository)
    {
        $repository->salvar($request);
        return redirect()
            ->action('ConselhoController@index')
            ->with('mensagem', 'Registro criado');
    }
    public function show($id)
    {
       
    }

    public function edit($id, ConselhoRepository $repository)
    {
        $conselhos = $repository->buscarid($id);
        if(!empty($conselhos)) {
            return view('cruds/conselhos/edit')->with('conselhos',$conselhos);
        } else {
            return redirect()->action('ConselhoController@index'); 
        }
    }

    public function update($id, Request $request, ConselhoRepository $repository)
    {
        $repository->buscarid($id);
        $repository->update($id, $request);
        return redirect()->action('ConselhoController@index')->with('mensagem', 'O registro foi atualizado');
    }

    public function destroy($id, ConselhoRepository $repository)
    {
        $repository->buscarid($id);
        try {
        $repository->deletar($id);
        return redirect()->action('ConselhoController@index')->with('mensagem', 'O registro foi excluido');  
          } catch (QueryException $e) {
            return redirect()->action('ConselhoController@index')->with('warning', 'O registro pois possui relacionamentos!');
          }     
    }

    public function pdf(ConselhoRepository $repository){
        $conselhos = Conselho::all();
        return \PDF::loadView('cruds.conselhos.pdf', compact('conselhos'))->stream('cadastro_conselhos.pdf');
    }
   
}
