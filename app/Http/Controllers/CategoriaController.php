<?php

namespace App\Http\Controllers;
use App\Categoria;
use Illuminate\Http\Request;
use App\Http\Requests\CategoriaRequest;
use App\Repositories\CategoriaRepository;
use Illuminate\Database\QueryException;
class CategoriaController extends Controller
{
    public function pesquisar(Request $request, CategoriaRepository $repository)
    {
        $totalPage = 10;
        $categorias = $repository->pesquisar($request->nome, $totalPage);
        return view('cruds/categorias/index')->with('categorias', $categorias);
    }


    public function index(CategoriaRepository $repository)
    {
        $categorias = $repository->paginacao(10);
        return view('cruds/categorias/index')->with('categorias', $categorias);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('cruds/categorias/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoriaRequest $request, CategoriaRepository $repository)
    {
        $repository->salvar($request);
        return redirect()
            ->action('CategoriaController@index')
            ->with('mensagem', 'Registro criado');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, CategoriaRepository $repository)
    {
        $categorias = $repository->buscarid($id);
        if(!empty($categorias)) {
            return view('cruds/categorias/edit')->with('categorias',$categorias);
        } else {
            return redirect()->action('CategoriaController@index'); 
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request, CategoriaRepository $repository)
    {
        $repository->buscarid($id);
        $repository->update($id, $request);
        return redirect()->action('CategoriaController@index')->with('mensagem', 'O registro foi atualizado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, CategoriaRepository $repository)
    {
        $repository->buscarid($id);
        try {
        $repository->deletar($id);
        return redirect()->action('CategoriaController@index')->with('mensagem', 'O registro foi excluido');  
        } catch (QueryException $e) {
        return redirect()->action('CategoriaController@index')->with('warning', 'O registro pois possui relacionamentos!');
        }       
    }

    public function pdf(CategoriaRepository $repository){
        $categorias = Categoria::all();
        return \PDF::loadView('cruds.categorias.pdf', compact('categorias'))->stream('cadastro_categorias.pdf');
    }
}
