<?php

namespace App\Http\Controllers;
use App\Unidade;
use Illuminate\Http\Request;
use App\Http\Requests\UnidadeRequest;
use App\Repositories\UnidadeRepository;
use Illuminate\Database\QueryException;
class UnidadeController extends Controller
{
 
    public function pesquisar(Request $request, UnidadeRepository $repository)
    {
        $totalPage = 10;
        $unidades = $repository->pesquisar($request->nome, $request->regans, $totalPage);
        return view('cruds/unidades/index')->with('unidades', $unidades);
    }

    public function index(UnidadeRepository $repository)
    {
        $unidades = $repository->paginacao(10);
        return view('cruds/unidades/index')->with('unidades', $unidades);
    }
    
    public function create()
    {
        return view('cruds/unidades/create');
    }

    public function store(UnidadeRequest $request, UnidadeRepository $repository)
    {
        $repository->salvar($request);
        return redirect()
            ->action('UnidadeController@index')
            ->with('mensagem', 'Registro criado');
    }
    public function show($id)
    {
       
    }

    public function edit($id, UnidadeRepository $repository)
    {
        $unidades = $repository->buscarid($id);
        if(!empty($unidades)) {
            return view('cruds/unidades/edit')->with('unidades',$unidades);
        } else {
            return redirect()->action('UnidadeController@index'); 
        }
    }

    public function update($id, Request $request, UnidadeRepository $repository)
    {
        $repository->buscarid($id);
        $repository->update($id, $request);
        return redirect()->action('UnidadeController@index')->with('mensagem', 'O registro foi atualizado');
    }

    public function destroy($id, UnidadeRepository $repository)
    {
        $repository->buscarid($id);
        try {
        $repository->deletar($id);
        return redirect()->action('UnidadeController@index')->with('mensagem', 'O registro foi excluido');  
          } catch (QueryException $e) {
            return redirect()->action('UnidadeController@index')->with('warning', 'O registro pois possui relacionamentos!');
          } 
    }

    public function pdf(UnidadeRepository $repository){
        $unidades = Unidade::all();
        return \PDF::loadView('cruds.unidades.pdf', compact('unidades'))->stream('cadastro_unidades.pdf');
    }
   
}
