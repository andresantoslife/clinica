<?php

namespace App\Http\Controllers;
use App\Especialidade;
use Illuminate\Http\Request;
use App\Http\Requests\EspecialidadeRequest;
use App\Repositories\EspecialidadeRepository;
use Illuminate\Database\QueryException;
class EspecialidadeController extends Controller
{
 
    public function pesquisar(Request $request, EspecialidadeRepository $repository)
    {
        $totalPage = 10;
        $especialidades = $repository->pesquisar($request->codigo, $request->nome, $totalPage);
        return view('cruds/especialidades/index')->with('especialidades', $especialidades);
    }

    public function index(EspecialidadeRepository $repository)
    {
        $especialidades = $repository->paginacao(10);
        return view('cruds/especialidades/index')->with('especialidades', $especialidades);
    }
    public function create()
    {
        return view('cruds/especialidades/create');
    }

    public function store(EspecialidadeRequest $request, EspecialidadeRepository $repository)
    {
        $repository->salvar($request);
        return redirect()
            ->action('EspecialidadeController@index')
            ->with('mensagem', 'Registro criado');
    }
    public function show($id)
    {
       
    }

    public function edit($id, EspecialidadeRepository $repository)
    {
        $especialidades = $repository->buscarid($id);
        if(!empty($especialidades)) {
            return view('cruds/especialidades/edit')->with('especialidades',$especialidades);
        } else {
            return redirect()->action('EspecialidadeController@index'); 
        }
    }

    public function update($id, Request $request, EspecialidadeRepository $repository)
    {
        $repository->buscarid($id);
        $repository->update($id, $request);
        return redirect()->action('EspecialidadeController@index')->with('mensagem', 'O registro foi atualizado');
    }

    public function destroy($id, EspecialidadeRepository $repository)
    {
        $repository->buscarid($id);
        try {
        $repository->deletar($id);
        return redirect()->action('EspecialidadeController@index')->with('mensagem', 'O registro foi excluido');  
          } catch (QueryException $e) {
            return redirect()->action('EspecialidadeController@index')->with('warning', 'O registro pois possui relacionamentos!');
          }  
    }

    public function pdf(EspecialidadeRepository $repository){
        $especialidades = Especialidade::all();
        return \PDF::loadView('cruds.especialidades.pdf', compact('especialidades'))->stream('cadastro_especialidades.pdf');
    }
   
}
