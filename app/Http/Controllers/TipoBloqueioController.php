<?php

namespace App\Http\Controllers;
use App\TipoBloqueio;
use Illuminate\Http\Request;
use App\Http\Requests\TipoBloqueioRequest;
use App\Repositories\TipoBloqueioRepository;
use Illuminate\Database\QueryException;
class TipoBloqueioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function pesquisar(Request $request, TipoBloqueioRepository $repository)
    {
        $totalPage = 10;
        $tipobloqueios = $repository->pesquisar($request->nome, $totalPage);
        return view('cruds/tipobloqueio/index')->with('tipobloqueios', $tipobloqueios);
    }

    public function index(TipoBloqueioRepository $repository)
    {
        $tipobloqueios = $repository->paginacao(10);
        return view('cruds/tipobloqueio/index')->with('tipobloqueios', $tipobloqueios);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('cruds/tipobloqueio/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TipoBloqueioRequest $request, TipoBloqueioRepository $repository)
    {
        $repository->salvar($request);
        return redirect()
            ->action('TipoBloqueioController@index')
            ->with('mensagem', 'Registro criado');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, TipoBloqueioRepository $repository)
    {
        $tipobloqueios = $repository->buscarid($id);
        if(!empty($tipobloqueios)) {
            return view('cruds/tipobloqueio/edit')->with('tipobloqueios',$tipobloqueios);
        } else {
            return redirect()->action('TipoBloqueioAgendaController@index'); 
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request, TipoBloqueioRepository $repository)
    {
        $repository->buscarid($id);
        $repository->update($id, $request);
        return redirect()->action('TipoBloqueioController@index')->with('mensagem', 'O registro foi atualizado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, TipoBloqueioRepository $repository)
    {
        $repository->buscarid($id);
        try {
        $repository->deletar($id);
        return redirect()->action('TipoBloqueioController@index')->with('mensagem', 'O registro foi excluido');  
          } catch (QueryException $e) {
            return redirect()->action('TipoBloqueioController@index')->with('warning', 'O registro pois possui relacionamentos!');
          }
    }

    public function pdf(TipoBloqueioRepository $repository){
        $tipobloqueios = TipoBloqueio::all();
        return \PDF::loadView('cruds.tipobloqueio.pdf', compact('tipobloqueios'))->stream('cadastro_tipobloqueio.pdf');
    }
}
