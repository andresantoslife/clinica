<?php

namespace App\Http\Controllers;
use App\TabelaFaturamento;
use App\Convenio;
use Illuminate\Http\Request;
use App\Http\Requests\ConvenioRequest;
use App\Repositories\ConvenioRepository;
use Illuminate\Database\QueryException;
use DB;
class ConvenioController extends Controller
{
 
    public function pesquisar(Request $request, ConvenioRepository $repository)
    {
        $totalPage = 10;
        $convenios = $repository->pesquisar($request->nome, $request->regans, $totalPage);
        return view('cruds/convenios/index')->with('convenios', $convenios);
    }

    public function index(ConvenioRepository $repository)
    {
        $convenios = $repository->paginacao(10);
        return view('cruds/convenios/index')->with('convenios', $convenios);
    }
    
    public function create()
    {
        $tabelaFaturamentos = TabelaFaturamento::all();
        return view('cruds/convenios/create',compact('tabelaFaturamentos',$tabelaFaturamentos));
    }

    public function store(ConvenioRequest $request, ConvenioRepository $repository)
    {
        $repository->salvar($request);
        return redirect()
            ->action('ConvenioController@index')
            ->with('mensagem', 'Registro criado');
    }
    public function show($id)
    {
       
    }

    public function edit($id, ConvenioRepository $repository)
    {
        $tabelafaturamento_selects = DB::select('SELECT 
        tabela_faturamentos.id, 
        tabela_faturamentos.nome 
        FROM convenios 
        join tabela_faturamentos 
        on tabela_faturamentos.id = convenios.tabela_faturamentos_id
        where convenios.id = ?',[$id]);

        $idtabelafaturamento = $tabelafaturamento_selects[0]->id;
        $tabelafaturamentos = DB::select("SELECT 
        id, nome 
        from tabela_faturamentos
        where tabela_faturamentos.id <> ?",[$idtabelafaturamento]);

        $convenios = $repository->buscarid($id);;

        if(!empty($convenios)) {
            return view('cruds/convenios/edit', compact('convenios', 'tabelafaturamento_selects','tabelafaturamentos', $convenios, $tabelafaturamento_selects, $tabelafaturamentos));
        } else {
            return redirect()->action('ConvenioController@index'); 
        }
    }

    public function update($id, Request $request, ConvenioRepository $repository)
    {
        $repository->buscarid($id);
        $repository->update($id, $request);
        return redirect()->action('ConvenioController@index')->with('mensagem', 'O registro foi atualizado');
    }

    public function destroy($id, ConvenioRepository $repository)
    {
        $repository->buscarid($id);
        try {
        $repository->deletar($id);
        return redirect()->action('ConvenioController@index')->with('mensagem', 'O registro foi excluido');  
          } catch (QueryException $e) {
            return redirect()->action('ConvenioController@index')->with('warning', 'O registro pois possui relacionamentos!');
          }    
    }

    public function pdf(ConvenioRepository $repository){
        $convenios = Convenio::all();
        return \PDF::loadView('cruds.convenios.pdf', compact('convenios'))->stream('cadastro_convenios.pdf');
    }
   
}
