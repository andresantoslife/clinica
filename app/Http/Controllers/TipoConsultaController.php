<?php

namespace App\Http\Controllers;
use App\TipoConsulta;
use Illuminate\Http\Request;
use App\Http\Requests\TipoConsultaRequest;
use App\Repositories\TipoConsultaRepository;
use Illuminate\Database\QueryException;
class TipoConsultaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function pesquisar(Request $request, TipoConsultaRepository $repository)
    {
        $totalPage = 10;
        $tipoconsultas = $repository->pesquisar($request->nome, $totalPage);
        return view('cruds/tipoconsulta/index')->with('tipoconsultas', $tipoconsultas);
    }

    public function index(TipoConsultaRepository $repository)
    {
        $tipoconsultas = $repository->paginacao(10);
        return view('cruds/tipoconsulta/index')->with('tipoconsultas', $tipoconsultas);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('cruds/tipoconsulta/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TipoConsultaRequest $request, TipoConsultaRepository $repository)
    {
        $repository->salvar($request);
        return redirect()
            ->action('TipoConsultaController@index')
            ->with('mensagem', 'Registro criado');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, TipoConsultaRepository $repository)
    {
        $tipoconsultas = $repository->buscarid($id);
        if(!empty($tipoconsultas)) {
            return view('cruds/tipoconsulta/edit')->with('tipoconsultas',$tipoconsultas);
        } else {
            return redirect()->action('TipoConsultaController@index'); 
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request, TipoConsultaRepository $repository)
    {
        $repository->buscarid($id);
        $repository->update($id, $request);
        return redirect()->action('TipoConsultaController@index')->with('mensagem', 'O registro foi atualizado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, TipoConsultaRepository $repository)
    {
        $repository->buscarid($id);
        try {
        $repository->deletar($id);
        return redirect()->action('TipoConsultaController@index')->with('mensagem', 'O registro foi excluido');  
          } catch (QueryException $e) {
            return redirect()->action('TipoConsultaController@index')->with('warning', 'O registro pois possui relacionamentos!');
          }
    }

    public function pdf(TipoConsultaRepository $repository){
        $tipoconsultas = TipoConsulta::all();
        return \PDF::loadView('cruds.tipoconsulta.pdf', compact('tipoconsultas'))->stream('cadastro_tipoconsulta.pdf');
    }
}
