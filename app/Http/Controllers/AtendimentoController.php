<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Atendimento;
use App\Agenda;
use App\TipoAtendimento;
use DB;
use Illuminate\Database\QueryException;
use App\Repositories\AtendimentoRepository;


class AtendimentoController extends Controller
{
    public function atenderpaciente(Request $request, AtendimentoRepository $repository){
        $atender = $repository->atender($request);
            return response()->json($atender); 
    }

    public function tipoatendimento(AtendimentoRepository $repository)
    {
       $tiposdeatendimentos = $repository->tipoatendimento();
        return response()->json($tiposdeatendimentos);
    }

    public function consultaagenda($id, AtendimentoRepository $repository) {
        $agenda = $repository->consultaagenda($id);
        return response()->json($agenda); 
    }

    public function convenio($id, AtendimentoRepository $repository) {
       $convenios = $repository->convenio($id);
        return response()->json($convenios);
    }

    public function procedimentos($agenda, $convenio, AtendimentoRepository $repository) {
        $procedimentos = $repository->procedimentos($agenda,$convenio);
        return response()->json($procedimentos);
     }
}
