<?php

namespace App\Http\Controllers;
use App\Paciente;
use App\Categoria;
use App\Convenio;
use Illuminate\Http\Request;
use App\Http\Requests\PacienteRequest;
use App\Repositories\PacienteRepository;
use DB;
use Illuminate\Database\QueryException;

class PacienteController extends Controller
{
 
    public function pesquisar(Request $request, PacienteRepository $repository)
    {
        $totalPage = 10;
        $pacientes = $repository->pesquisar($request->nome, $request->cpf, $totalPage);
        return view('cruds/pacientes/index')->with('pacientes', $pacientes);
    }

    public function index(PacienteRepository $repository)
    {
        $pacientes = $repository->paginacao(10);
        return view('cruds/pacientes/index')->with('pacientes', $pacientes);
    }
    public function create()
    {
        $categorias = Categoria::all();
        $convenios = Convenio::where('status', '=', 'ativo')->get();
        return view('cruds/pacientes/create',compact('categorias','convenios', $categorias, $convenios));
    }

    public function store(PacienteRequest $request, PacienteRepository $repository)
    {
        $repository->salvar($request);   
        return redirect()
              ->action('PacienteController@index')
              ->with('mensagem', 'Registro criado');
    }
    public function show($id)
    {
      
    }

    public function edit($id, PacienteRepository $repository)
    {   
        $categoria_select = DB::select('select 
        categorias.id as idCat, 
        categorias.nome as nomeCat
        from pacientes 
        join categorias 
        on categorias.id = pacientes.categorias_id 
        where pacientes.id = ?',[$id]);
        $idCat = $categoria_select[0]->idCat;
        $categorias = DB::select("select * from categorias where id <> '$idCat'");
        $pacientes = $repository->buscarid($id);
        return view('cruds/pacientes/edit', compact('pacientes', 'categorias','categoria_select', $pacientes, $categorias, $categoria_select));
    }

    public function update(Request $request, PacienteRepository $repository, $id)
    {
        $pacientes = $repository->atualizar($id, $request);
        return redirect()->action('PacienteController@index')->with('mensagem', 'O registro foi atualizado');
    }

    public function destroy($id, PacienteRepository $repository)
    {
        $repository->buscarid($id);
        try {
        $repository->deletar($id);
        return redirect()->action('PacienteController@index')->with('mensagem', 'O registro foi excluido');  
          } catch (QueryException $e) {
            return redirect()->action('PacienteController@index')->with('warning', 'O registro pois possui relacionamentos!');
          }        
        
    }

    public function pdf(PacienteRepository $repository){
        $pacientes = Paciente::all();
        return \PDF::loadView('cruds.pacientes.pdf', compact('pacientes'))->stream('cadastro_pacientes.pdf');
    }

    public function listarpacienteconvenios(PacienteRepository $repository, Request $request, $paciente){
        $consulta = $repository->select2Convenio($request, $paciente);
        return response()->json($consulta);
    }

    public function adicionarpacienteconvenios(PacienteRepository $repository, $paciente, $convenio){
        $adicionar = $repository->addselect2Convenio($paciente, $convenio);
        return response()->json($adicionar);
    }
   
    public function removerpacienteconvenios(PacienteRepository $repository, Request $request) {
        $repository->deletarRelacionamento($request);
    }

    public function checarpaciente($nome = null) {
       $usuarios = DB::select("select count(*) as contador from pacientes where nome= ?",[$nome]);
       return $usuarios[0]->contador;
    }
    public function novopaciente(Request $request) {
        $pacientes = [
            'nome' => $request->nomePaciente,
            'status' => 'ativo'
        ];
        Paciente::UpdateOrCreate($pacientes);
    }
    public function todospacientes() {
        $pacientes = DB::select("select 
        date_format(pacientes.dtnascimento, '%d/%m/%Y') as dtnasc, 
        categorias.nome as catNome,
        pacientes.*
        from pacientes
        left join categorias on categorias.id = pacientes.categorias_id      
        ");
        return $pacientes;
    }
}
