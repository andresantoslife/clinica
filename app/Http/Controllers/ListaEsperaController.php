<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class ListaEsperaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $usuarioLogado = auth()->user()->name;
        $dataAtual = date('Y-m-d');
        $dadosdaAgendas = DB::select("select 
        DATE_FORMAT(agendas.dtagenda, '%H:%i') as horario, 
        atendimentos.id as atendimento_id,
        pacientes.id as paciente_id,
        pacientes.nome as paciente,
        atendimentos.status AS status,
        atendimentos.tempoespera as tempodeespera,
        convenios.nome as convenio
        
        from agendas 
        join atendimentos on atendimentos.agendas_id = agendas.id
        join pacientes on pacientes.id = agendas.pacientes_id
        join convenio_pacientes on convenio_pacientes.id = atendimentos.convenio_pacientes_id
        join convenios on convenio_pacientes.convenios_id = convenios.id
         where agendas.dtagenda like '$dataAtual%' order by agendas.id asc");
        return view('functions/listaespera/index',compact('dadosdaAgendas',$dadosdaAgendas));
    }

    public function pesquisar(Request $request){
        $dataPesquisar = $request->dataPesquisar;
        $dataAtual = implode('-', array_reverse(explode('/', $dataPesquisar)));
        $usuarioLogado = auth()->user()->name;
        $dadosdaAgendas = DB::select("select 
        DATE_FORMAT(agendas.dtagenda, '%H:%i') as horario, 
        atendimentos.id as atendimento_id,
        pacientes.id as paciente_id,
        pacientes.nome as paciente,
        atendimentos.status AS status,
        DATE_FORMAT(agendas.dtagenda, '%Y-%m-%d') as data,
        convenios.nome as convenio
        from agendas 
        join atendimentos on atendimentos.agendas_id = agendas.id
        join pacientes on pacientes.id = agendas.pacientes_id
        join convenio_pacientes on convenio_pacientes.id = atendimentos.convenio_pacientes_id
        join convenios on convenio_pacientes.convenios_id = convenios.id
         where agendas.dtagenda like '$dataAtual%' order by agendas.id asc");
        return view('functions/listaespera/index',compact('dadosdaAgendas',$dadosdaAgendas));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
