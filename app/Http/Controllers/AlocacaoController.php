<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Prestador;
use App\Especialidade;
use App\Unidade;
use App\Alocacao;
use DB;
use App\Repositories\AlocacaoRepository;
use Illuminate\Database\QueryException;

class AlocacaoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $prestadores = Prestador::all();
      $unidades = Unidade::all();
      $especialidades = Especialidade::all();
      return view('functions/alocacao/index',compact('prestadores','unidades','especialidades', $prestadores, $unidades, $especialidades));
    }

    public function listarprestadores(Request $request, $prestador, $unidade) {
        $alocacao = DB::select("SELECT alocacaos.id,especialidades.nome FROM especialidades join alocacaos on alocacaos.especialidades_id = especialidades.id WHERE alocacaos.prestadors_id = ? and unidades_id = ? order by nome asc",[$prestador,$unidade]);
           return response()->json($alocacao);
        
    }

    public function listarespecialidade($unidade, $prestador) {
        $alocacao = DB::select("SELECT especialidades.id,especialidades.nome FROM especialidades join alocacaos on alocacaos.especialidades_id = especialidades.id WHERE alocacaos.prestadors_id = ? and unidades_id = ? order by nome asc",[$prestador,$unidade]);
        return response()->json($alocacao);

    }

    
    public function alocacaopost(Request $request, AlocacaoRepository $repository) {
        $repository->salvar($request);
        //return response()->json(['success'=>'Registro Adicionado.']);
    }
    
    public function alocacaodel(Request $request, AlocacaoRepository $repository) {
       try {
        $repository->deletar($request->alocacao);
       } catch (QueryException $e) {      
        return response()->json(['warning' => 'Este registro possuí vínculos!'],500);
        }  
     }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
          
    }

    public function store(Request $request, AlocacaoRepository $repository)
    {
        //return redirect()->action('AlocacaoController@create'); 
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
    
}
