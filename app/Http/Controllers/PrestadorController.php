<?php

namespace App\Http\Controllers;
use App\Prestador;
use App\Conselho;
use App\User;
use Illuminate\Http\Request;
use App\Http\Requests\PrestadorRequest;
use App\Repositories\PrestadorRepository;
use DB;
use Illuminate\Database\QueryException;

class PrestadorController extends Controller
{
 
    public function pesquisar(Request $request, PrestadorRepository $repository)
    {
        $totalPage = 10;
        $prestadores = $repository->pesquisar($request->nome, $request->cpf, $totalPage);
        return view('cruds/prestadores/index')->with('prestadores', $prestadores);
    }

    public function index(PrestadorRepository $repository)
    {
        $prestadores = $repository->paginacao(10);
        return view('cruds/prestadores/index')->with('prestadores', $prestadores);
    }
    public function create()
    {
        $conselhos = Conselho::all();
        $users = User::all();
        return view('cruds/prestadores/create',compact('users','conselhos',$users,$conselhos));
    }

    public function store(PrestadorRequest $request, PrestadorRepository $repository)
    {
        $repository->salvar($request);
        return redirect()
            ->action('PrestadorController@index')
            ->with('mensagem', 'Registro criado');
    }
    public function show($id)
    {
      /* $prestadores = Prestador::find($id);
       echo "<h1> Dados do Prestador</h1>";
       echo "Nome do Prestador: {$prestadores->nome}<br>";
       echo "Nome do Prestador: {$prestadores->cpf}<br>";
       $prestador_conselho = $prestadores->conselho()->get()->first();
       echo "Conselho: {$prestador_conselho->sigla}<br>";*/
    }

    public function edit($id, PrestadorRepository $repository)
    {
        $prestadores = $repository->buscarid($id);
        //Tratando Selects 
        $prestadores_select = Prestador::find($id);
        $user_atual = User::find($prestadores_select->users_id);
           //Usuarios  
        $users= DB::table('users')
           ->select('id','email')
           ->where('id','<>',$prestadores_select->users_id)->get();
           //Conselho  
        $conselho_atual = Conselho::find($prestadores_select->conselhos_id);
        $conselhos = DB::table('conselhos')
           ->select('id','sigla')
           ->where('id','<>',$prestadores_select->conselhos_id)->get();

        return view('cruds/prestadores/edit', compact('conselhos','prestadores','conselho_atual','users', 'user_atual', $conselhos, $prestadores, $conselho_atual, $users, $user_atual));
    }

    public function update(Request $request, PrestadorRepository $repository, $id)
    {
        $prestador = $repository->atualizar($id, $request);
        return redirect()->action('PrestadorController@index')->with('mensagem', 'O registro foi atualizado');
    }

    public function destroy($id, PrestadorRepository $repository)
    {
        $repository->buscarid($id);
        try {
        $repository->deletar($id);
        return redirect()->action('PrestadorController@index')->with('mensagem', 'O registro foi excluido');  
          } catch (QueryException $e) {
            return redirect()->action('PrestadorController@index')->with('warning', 'O registro pois possui relacionamentos!');
          }        
        
    }

    public function pdf(PrestadorRepository $repository){
        $prestadores = Prestador::all();
        return \PDF::loadView('cruds.prestadores.pdf', compact('prestadores'))->stream('cadastro_prestadores.pdf');
    }
   
}
