<?php
namespace App\Http\Controllers;
use App\Paciente;
use App\Atendimento;
use App\Anamnese;
use App\Evolucao;
use App\Diagnostico;
use App\PedidoExame;
use App\Atestado;
use Illuminate\Http\Request;
use DB;

class ProntuarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id = null, $idAtendimento = null, Request $request)
    {
        //Buscar CIDS
        $cids = DB::select('select * from cids');
        $tipoexames = DB::select('select * from tipo_exames');
        if($id) {
        $SelectPaciente = DB::select('select id, nome from pacientes where id = ?',[$id]);
        $Pacientes =  DB::select('select id, nome from pacientes where id <> ? order by nome asc',[$id]);
        return view('functions/prontuario/index',compact('SelectPaciente','Pacientes','cids', 'tipoexames',$SelectPaciente, $Pacientes,$cids, $tipoexames));
        } else {
            $Pacientes =  DB::select('select id, nome from pacientes order by nome asc'); 
            return view('functions/prontuario/index',compact('Pacientes','cids', 'tipoexames',$Pacientes,$cids, $tipoexames)); 
        }

        
    }

    public function iniciar(Request $request){
       $id = $request->idAtendimento;
       $inicioatendimento = date("Y-m-d H:i:s");
       $update = DB::update("UPDATE atendimentos SET status = 'aberto', inicioatendimento = ? where id = ?", [$inicioatendimento, $id]);
    }

    public function finalizar(Request $request){
        $id = $request->idAtendimento;
        $fimatendimento = date("Y-m-d H:i:s");
        $update = DB::update("UPDATE atendimentos SET status = 'finalizado', fimatendimento = ? where id = ?", [$fimatendimento, $id]);
     }

     public function getanamnese($idPaciente) {
        $getanamnese = DB::select('SELECT pacientes.id, anamnese.id, anamnese.descricao from atendimentos 
        join anamnese on anamnese.atendimentos_id = atendimentos.id 
        join convenio_pacientes on convenio_pacientes.id = atendimentos.convenio_pacientes_id
        join pacientes on pacientes.id = convenio_pacientes.pacientes_id
        where pacientes.id = ?',[$idPaciente]);
        return $getanamnese; 
     }

     public function salvaranamnese(Request $request) {
        $id = $request->idAtendimento;
      $contador = DB::select('select count(*) as cont from anamnese where atendimentos_id = ?',[$id]);
        $contador = $contador[0]->cont; 
        if ($contador >= 1) {   
        $anamneseForm = $request->anamneseForm;
            DB::update("UPDATE anamnese SET descricao = ? WHERE atendimentos_id = ?", [$anamneseForm, $id]);
        } else { 
        $anamneseForm = $request->anamneseForm;
        $salvar = [
                'descricao' => $anamneseForm,
                'atendimentos_id' => $id
            ];
         Anamnese::Create($salvar);
        } 

     }
    
public function getevolucaos($idPaciente) {        
    $evolucaos = DB::select('SELECT pacientes.id, evolucaos.id, evolucaos.descricao from atendimentos 
    join evolucaos on evolucaos.atendimentos_id = atendimentos.id 
    join convenio_pacientes on convenio_pacientes.id = atendimentos.convenio_pacientes_id
    join pacientes on pacientes.id = convenio_pacientes.pacientes_id
    where pacientes.id = ?',[$idPaciente]);
    return $evolucaos;
     }

public function salvarevolucaos(Request $request) {
        $id = $request->idAtendimento;
        $contador = DB::select('select count(*) as cont from evolucaos where atendimentos_id = ?',[$id]);
        $contador = $contador[0]->cont;
        if ($contador >= 1) {   
        $evolucaosForm = $request->evolucaosForm;
            DB::update("UPDATE evolucaos SET descricao = ? WHERE atendimentos_id = ?", [$evolucaosForm, $id]);
        } else {
        $evolucaosForm = $request->evolucaosForm;
        $salvar = [
                'descricao' => $evolucaosForm,
                'atendimentos_id' => $id
            ];
         Evolucao::Create($salvar);
        }

     }

     public function getdiagnostico($idPaciente) {        
        $diagnostico = DB::select('SELECT pacientes.id, cids.codigo, cids.descricao from atendimentos 
        join diagnosticos on diagnosticos.atendimentos_id = atendimentos.id 
        join convenio_pacientes on convenio_pacientes.id = atendimentos.convenio_pacientes_id
        join pacientes on pacientes.id = convenio_pacientes.pacientes_id
        join cids on cids.id = diagnosticos.cids_id
        where pacientes.id = ?',[$idPaciente]);
        return $diagnostico;
         }

    public function getexames($idtipoexames){
        $exames = DB::select('select * from exames where tipo_exames_id = ? order by nome asc',[$idtipoexames]);
        return $exames;
    }

    public function salvarexames(Request $request){
        $idPaciente = $request->idPaciente;
        $idAtendimento = $request->idAtendimento;
        $observacao = $request->observacaoExame;
        $exames_id = $request->exames_id;
        $dataExame = $request->dataExame;
        $idAtendimento = $request->idAtendimento;
        $prestador = DB::select('select prestadors.id as prestador from atendimentos 
        join alocacaos on alocacaos.id = atendimentos.alocacaos_id
        join prestadors on prestadors.id = alocacaos.prestadors_id 
        where atendimentos.id = ?',[$idAtendimento]);
        $idPrestador = $prestador[0]->prestador;
       
        $salvar = [
            'observacao' => $observacao,
            'dataExame' => $dataExame,
            'exames_id' => $exames_id,
            'prestadors_id' => $idPrestador,
            'atendimentos_id' => $idAtendimento,
            'pacientes_id' => $idPaciente
        ];
        PedidoExame::UpdateOrCreate($salvar);
        return $salvar;
    }

    public function listaexames($idPaciente){
        $listar = DB::select("SELECT pedido_exames.id as id, prestadors.nome as prestador, DATE_FORMAT(pedido_exames.dataExame, '%d/%m/%Y') as data, exames.nome as exame FROM pedido_exames
        join prestadors on prestadors.id = pedido_exames.prestadors_id
        join exames on exames.id = pedido_exames.exames_id where pacientes_id = ?",[$idPaciente]);
        return $listar;

    }

         public function salvardiagnostico(Request $request) {
            $id = $request->idAtendimento;
            $cid = $request->cid;
        
            $salvar = [
                    'cids_id' => $cid,
                    'atendimentos_id' => $id
                ];
             Diagnostico::UpdateOrCreate($salvar);

         }     
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function dados($id) {
        $dadosPaciente =  DB::select("select 
        id,
        nome,
        case sexo
        when 'masculino' then 'Masculino'
        when 'feminino' then 'Feminino'
        end as sexo, 
        DATE_FORMAT(dtnascimento, '%d/%m/%Y') as dtnascimento 
        from pacientes where id =?",[$id]); 
        return $dadosPaciente;
    }

    public function atendimentos($idPaciente){
        $atendimentos = DB::select("select atendimentos.id, 
        atendimentos.tempoespera, 
        atendimentos.inicioatendimento, 
        atendimentos.fimatendimento, 
        atendimentos.status 
        from atendimentos 
        join convenio_pacientes on convenio_pacientes.id = atendimentos.convenio_pacientes_id
        join pacientes on pacientes.id = convenio_pacientes.pacientes_id
        where pacientes.id = ? order by atendimentos.id desc limit 0,1",[$idPaciente]);
        return $atendimentos;
    }   

    public function pedidoexamepdf($idexame){
            $exames = DB::select("SELECT 
                pacientes.nome as paciente,
                exames.nome as exame, 
                prestadors.nome as prestador, 
                prestadors.nrconselho as crm,
                pedido_exames.observacao as observacao,
                DATE_FORMAT(pedido_exames.dataExame, '%d/%m/%Y') as data 
            FROM pedido_exames
                join pacientes on pacientes.id = pedido_exames.pacientes_id
                join prestadors on prestadors.id = pedido_exames.prestadors_id
                join exames on exames.id = pedido_exames.exames_id 
            where pedido_exames.id = ?",[$idexame]);
            return \PDF::loadView('functions.prontuario.pedidoexame', compact('exames'))->stream('pedidoExame.pdf');
    }

    public function atestadopdf(Request $request) {
       $tipo = $request->atestado;
       //Atestado
       if ($tipo == 1) { 
       $atendimentos_id = $request->atendimentoID;
       $hinicio = $request->atestadoHorainicio;
       $hinfim = $request->atestadoHorafim;
       $data = $request->atestadoData;
       $observacao = $request->atestadoDias;
       $cids_id = $request->CIDAtestado;
       
       $salvar = [
        'atendimentos_id' => $atendimentos_id,
        'tipo' => $tipo,
        'hinicio' => $hinicio,
        'hfim' => $hinfim,
        'data' => $data,
        'observacao' => $observacao,
        'cids_id' => $cids_id
         ];
        $atestado = Atestado::UpdateOrCreate($salvar);
        $lastId = $atestado->id;  //PEGAR ULTIMO ID
        $atestados = DB::select("SELECT 
        cids.codigo as cid,
        conselhos.sigla as conselho,
        pacientes.nome as paciente,
        prestadors.nome as prestador,
        prestadors.nrconselho as crm,
        DATE_FORMAT(atestados.data, '%d/%m/%Y') as data, 
        atestados.hinicio as hinicio, 
        atestados.observacao as observacao,
        atestados.hfim as hfim 
           FROM atestados 
                   join atendimentos on atendimentos.id = atestados.atendimentos_id
                   join alocacaos on alocacaos.id = atendimentos.alocacaos_id
                   join prestadors on prestadors.id = alocacaos.prestadors_id
                   join convenio_pacientes on convenio_pacientes.id = atendimentos.convenio_pacientes_id
                   join pacientes on pacientes.id = convenio_pacientes.pacientes_id
                   join conselhos on conselhos.id = prestadors.conselhos_id
                   join cids on cids.id = atestados.cids_id
           WHERE
        atestados.id = ?",[$lastId]); 
        return \PDF::loadView('functions.prontuario.atestado', compact('atestados'))->stream('atestado.pdf');
       
      } else  if ($tipo == 2) { 
        //Comparecimento
        $atendimentos_id = $request->atendimentoCompID;
        $hinicio = $request->comparecimentoHorainicio;
        $hinfim = $request->comparecimentoHorafim;
        $data = $request->comparecimentoData;
        $salvar = [
            'atendimentos_id' => $atendimentos_id,
            'tipo' => $tipo,
            'hinicio' => $hinicio,
            'hfim' => $hinfim,
            'data' => $data
             ];
         $atestado = Atestado::UpdateOrCreate($salvar);
         $lastId = $atestado->id;  //PEGAR ULTIMO ID
         $comparecimentos = DB::select("SELECT 
         conselhos.sigla as conselho,
         pacientes.nome as paciente,
         prestadors.nome as prestador,
         prestadors.nrconselho as crm,
         DATE_FORMAT(atestados.data, '%d/%m/%Y') as data, 
         atestados.hinicio as hinicio, 
         atestados.hfim as hfim 
            FROM atestados 
                    join atendimentos on atendimentos.id = atestados.atendimentos_id
                    join alocacaos on alocacaos.id = atendimentos.alocacaos_id
                    join prestadors on prestadors.id = alocacaos.prestadors_id
                    join convenio_pacientes on convenio_pacientes.id = atendimentos.convenio_pacientes_id
                    join pacientes on pacientes.id = convenio_pacientes.pacientes_id
                    join conselhos on conselhos.id = prestadors.conselhos_id
            WHERE
         atestados.id = ?",[$lastId]); 
         return \PDF::loadView('functions.prontuario.comparecimento', compact('comparecimentos'))->stream('comparecimento.pdf');
       }

    }


    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
