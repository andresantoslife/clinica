<?php

namespace App\Http\Controllers;
use App\IndicacaoAcidente;
use Illuminate\Http\Request;
use App\Http\Requests\IndicacaoAcidenteRequest;
use App\Repositories\IndicacaoAcidenteRepository;
use Illuminate\Database\QueryException;
class IndicacaoAcidenteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function pesquisar(Request $request, IndicacaoAcidenteRepository $repository)
    {
        $totalPage = 10;
        $indicacaoacidentes = $repository->pesquisar($request->nome, $totalPage);
        return view('cruds/indicacaoacidente/index')->with('indicacaoacidentes', $indicacaoacidentes);
    }

    public function index(IndicacaoAcidenteRepository $repository)
    {
        $indicacaoacidentes = $repository->paginacao(10);
        return view('cruds/indicacaoacidente/index')->with('indicacaoacidentes', $indicacaoacidentes);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('cruds/indicacaoacidente/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(IndicacaoAcidenteRequest $request, IndicacaoAcidenteRepository $repository)
    {
        $repository->salvar($request);
        return redirect()
            ->action('IndicacaoAcidenteController@index')
            ->with('mensagem', 'Registro criado');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, IndicacaoAcidenteRepository $repository)
    {
        $indicacaoacidentes = $repository->buscarid($id);
        if(!empty($indicacaoacidentes)) {
            return view('cruds/indicacaoacidente/edit')->with('indicacaoacidentes',$indicacaoacidentes);
        } else {
            return redirect()->action('IndicacaoAcidenteController@index'); 
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request, IndicacaoAcidenteRepository $repository)
    {
        $repository->buscarid($id);
        $repository->update($id, $request);
        return redirect()->action('IndicacaoAcidenteController@index')->with('mensagem', 'O registro foi atualizado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, IndicacaoAcidenteRepository $repository)
    {
        $repository->buscarid($id);
        try {
        $repository->deletar($id);
        return redirect()->action('IndicacaoAcidenteController@index')->with('mensagem', 'O registro foi excluido');  
          } catch (QueryException $e) {
            return redirect()->action('IndicacaoAcidenteController@index')->with('warning', 'O registro pois possui relacionamentos!');
          }  
    }

    public function pdf(IndicacaoAcidenteRepository $repository){
        $indicacaoacidentes = IndicacaoAcidente::all();
        return \PDF::loadView('cruds.indicacaoacidente.pdf', compact('indicacaoacidentes'))->stream('cadastro_indicacaoacidente.pdf');
    }
}
