<?php

namespace App\Http\Controllers;
use App\TipoAgenda;
use Illuminate\Http\Request;
use App\Http\Requests\TipoAgendaRequest;
use App\Repositories\TipoAgendaRepository;
use Illuminate\Database\QueryException;
class TipoAgendaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function pesquisar(Request $request, TipoAgendaRepository $repository)
    {
        $totalPage = 10;
        $tipoagendas = $repository->pesquisar($request->nome, $totalPage);
        return view('cruds/tipoagenda/index')->with('tipoagendas', $tipoagendas);
    }

    public function index(TipoAgendaRepository $repository)
    {
        $tipoagendas = $repository->paginacao(10);
        return view('cruds/tipoagenda/index')->with('tipoagendas', $tipoagendas);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('cruds/tipoagenda/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TipoAgendaRequest $request, TipoAgendaRepository $repository)
    {
        $repository->salvar($request);
        return redirect()
            ->action('TipoAgendaController@index')
            ->with('mensagem', 'Registro criado');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, TipoAgendaRepository $repository)
    {
        $tipoagendas = $repository->buscarid($id);
        if(!empty($tipoagendas)) {
            return view('cruds/tipoagenda/edit')->with('tipoagendas',$tipoagendas);
        } else {
            return redirect()->action('TipoAgendaController@index'); 
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request, TipoAgendaRepository $repository)
    {
        $repository->buscarid($id);
        $repository->update($id, $request);
        return redirect()->action('TipoAgendaController@index')->with('mensagem', 'O registro foi atualizado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, TipoAgendaRepository $repository)
    {
        $repository->buscarid($id);
        try {
        $repository->deletar($id);
        return redirect()->action('TipoAgendaController@index')->with('mensagem', 'O registro foi excluido');  
          } catch (QueryException $e) {
            return redirect()->action('TipoAgendaController@index')->with('warning', 'O registro pois possui relacionamentos!');
          }
    }

    public function pdf(TipoAgendaRepository $repository){
        $tipoagendas = TipoAgenda::all();
        return \PDF::loadView('cruds.tipoagenda.pdf', compact('tipoagendas'))->stream('cadastro_tipoagenda.pdf');
    }
}
