<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Atestado extends Model
{
    protected $fillable = [
        'tipo',
        'data',
        'hinicio',
        'hfim',
        'observacao',
        'cids_id',
        'atendimentos_id'
    ];
}
