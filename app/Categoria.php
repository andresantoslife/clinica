<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    protected $fillable = [
        'nome'
    ];

    public function pacientes()
       {
           return $this->belongsToMany(Paciente::class, 'paciente_categorias', 'categorias_id', 'pacientes_id');
       }
}
