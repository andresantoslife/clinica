<?php
namespace App\Repositories;
use App\Unidade;
use DB;
class UnidadeRepository 
{
	private $model;

	public function __construct(Unidade $model)
	{
		$this->model = $model;
	}

	public function buscartodos()
	{
		return $this->model->all();
    }    
    
    public function pesquisar($nome, $regans, $totalPage) {
        if ((isset($nome)) || (isset($regans))) {
            $unidades = $this->model->where('nome','like','%'.$nome.'%')
              ->where('regans','like','%'.$regans.'%')
              ->paginate($totalPage);
        } else {
            $unidades = $this->model->paginate($totalPage);    
        }
        return $unidades;
    }

    public function buscarid($id) {
        return DB::select("SELECT * from unidades where id = ?",[$id]);
    }

    public function deletar($id) {
        return $this->model->destroy($id);
    }

    public function paginacao($totalPage)
    {
        return $this->model->paginate($totalPage);
    }

    public function salvar($request) {
        $unidades = [
            'nome' => $request->nome,
            'regans' => $request->regans
        ];
        return $this->model->create($unidades);
    }

    public function update($id, $request) {
        $unidades = [
            $request->nome,
            $request->regans,
            $id
        ];
        return DB::update("UPDATE unidades SET nome = ?, regans = ? WHERE id = ?", $unidades);
    }
}