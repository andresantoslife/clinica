<?php
namespace App\Repositories;
use App\Conselho;
use DB;
class ConselhoRepository 
{
	private $model;

	public function __construct(Conselho $model)
	{
		$this->model = $model;
	}

	public function buscartodos()
	{
		return $this->model->all();
    }    
    
    public function pesquisar($numero, $sigla, $nome, $totalPage) {
        if ((isset($numero)) || (isset($sigla)) || (isset($nome))  ) {
            $conselhos = $this->model->where('numero','like','%'.$numero.'%')
              ->where('sigla','like','%'.$sigla.'%')
              ->where('nome','like','%'.$nome.'%')
              ->paginate($totalPage);
        } else {
            $conselhos = $this->model->paginate($totalPage);    
        }
        return $conselhos;
    }

    public function buscarid($id) {
        return DB::select("SELECT * from conselhos where id = ?",[$id]);
    }

    public function deletar($id) {
        return $this->model->destroy($id);
    }

    public function paginacao($totalPage)
    {
        return $this->model->paginate($totalPage);
    }

    public function salvar($request) {
        $conselhos = [
            'numero' => $request->numero,
            'sigla' => $request->sigla,
            'nome' => $request->nome
        ];
        return $this->model->create($conselhos);
    }

    public function update($id, $request) {
        $conselhos = [
            $request->numero,
            $request->sigla,
            $request->nome,
            $id
        ];
        return DB::update("UPDATE conselhos SET numero = ?, sigla = ?, nome = ? WHERE id = ?", $conselhos);
    }
}