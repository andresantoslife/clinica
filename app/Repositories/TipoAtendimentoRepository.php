<?php
namespace App\Repositories;
use App\TipoAtendimento;
use DB;
class TipoAtendimentoRepository 
{
	private $model;

	public function __construct(TipoAtendimento $model)
	{
		$this->model = $model;
	}

	public function buscartodos()
	{
		return $this->model->all();
    }    
    
    public function pesquisar($nome, $totalPage) {
        if (isset($nome)) {
            $tipoatendimentos = $this->model->where('nome','like','%'.$nome.'%')
              ->paginate($totalPage);
        } else {
            $tipoatendimentos = $this->model->paginate($totalPage);    
        }
        return $tipoatendimentos;
    }

    public function buscarid($id) {
        return DB::select("SELECT * from tipo_atendimentos where id = ?",[$id]);
    }

    public function deletar($id) {
        return $this->model->destroy($id);
    }

    public function paginacao($totalPage)
    {
        return $this->model->paginate($totalPage);
    }

    public function salvar($request) {
        $tipoatendimentos = [
            'nome' => $request->nome
        ];
        return $this->model->create($tipoatendimentos);
    }

    public function update($id, $request) {
        $tipoatendimentos = [
            $request->nome,
            $id
        ];
        return DB::update("UPDATE tipo_atendimentos SET nome = ? WHERE id = ?", $tipoatendimentos);
    }
}