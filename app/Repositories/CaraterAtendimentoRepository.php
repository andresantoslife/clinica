<?php
namespace App\Repositories;
use App\CaraterAtendimento;
use DB;
class CaraterAtendimentoRepository 
{
	private $model;

	public function __construct(CaraterAtendimento $model)
	{
		$this->model = $model;
	}

	public function buscartodos()
	{
		return $this->model->all();
    }    
    
    public function pesquisar($nome, $totalPage) {
        if (isset($nome)) {
            $carateratendimentos = $this->model->where('nome','like','%'.$nome.'%')
              ->paginate($totalPage);
        } else {
            $carateratendimentos = $this->model->paginate($totalPage);    
        }
        return $carateratendimentos;
    }

    public function buscarid($id) {
        return DB::select("SELECT * from carater_atendimentos where id = ?",[$id]);
    }

    public function deletar($id) {
        return $this->model->destroy($id);
    }

    public function paginacao($totalPage)
    {
        return $this->model->paginate($totalPage);
    }

    public function salvar($request) {
        $carateratendimentos = [
            'nome' => $request->nome
        ];
        return $this->model->create($carateratendimentos);
    }

    public function update($id, $request) {
        $carateratendimentos = [
            $request->nome,
            $id
        ];
        return DB::update("UPDATE carater_atendimentos SET nome = ? WHERE id = ?", $carateratendimentos);
    }
}