<?php
namespace App\Repositories;
use App\Categoria;
use DB;
class CategoriaRepository 
{
	private $model;

	public function __construct(Categoria $model)
	{
		$this->model = $model;
	}

	public function buscartodos()
	{
		return $this->model->all();
    }    
    
    public function pesquisar($nome, $totalPage) {
        if (isset($nome)) {
            $categorias = $this->model->where('nome','like','%'.$nome.'%')
              ->paginate($totalPage);
        } else {
            $categorias = $this->model->paginate($totalPage);    
        }
        return $categorias;
    }

    public function buscarid($id) {
        return DB::select("SELECT * from categorias where id = ?",[$id]);
    }

    public function deletar($id) {
        return $this->model->destroy($id);
    }

    public function paginacao($totalPage)
    {
        return $this->model->paginate($totalPage);
    }

    public function salvar($request) {
        $categorias = [
            'nome' => $request->nome
        ];
        return $this->model->create($categorias);
    }

    public function update($id, $request) {
        $categorias = [
            $request->nome,
            $id
        ];
        return DB::update("UPDATE categorias SET nome = ? WHERE id = ?", $categorias);
    }
}