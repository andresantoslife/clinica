<?php
namespace App\Repositories;
use App\TipoBloqueio;
use DB;
class TipoBloqueioRepository 
{
	private $model;

	public function __construct(TipoBloqueio $model)
	{
		$this->model = $model;
	}

	public function buscartodos()
	{
		return $this->model->all();
    }    
    
    public function pesquisar($nome, $totalPage) {
        if (isset($nome)) {
            $tipobloqueios = $this->model->where('nome','like','%'.$nome.'%')
              ->paginate($totalPage);
        } else {
            $tipobloqueios = $this->model->paginate($totalPage);    
        }
        return $tipobloqueios;
    }

    public function buscarid($id) {
        return DB::select("SELECT * from tipo_bloqueios where id = ?",[$id]);
    }

    public function deletar($id) {
        return $this->model->destroy($id);
    }

    public function paginacao($totalPage)
    {
        return $this->model->paginate($totalPage);
    }

    public function salvar($request) {
        $tipobloqueios = [
            'nome' => $request->nome
        ];
        return $this->model->create($tipobloqueios);
    }

    public function update($id, $request) {
        $tipobloqueios = [
            $request->nome,
            $id
        ];
        return DB::update("UPDATE tipo_bloqueios SET nome = ? WHERE id = ?", $tipobloqueios);
    }
}