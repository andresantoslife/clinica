<?php
namespace App\Repositories;
use App\Paciente;
use App\ConvenioPaciente;
use DB;
class PacienteRepository 
{
	private $model;

	public function __construct(Paciente $model)
	{
		$this->model = $model;
	}

	public function buscartodos()
	{
		return $this->model->all();
    }    
    
    public function pesquisar($nome, $cpf, $totalPage) {
        if ((isset($nome)) || (isset($cpf))) {
            $pacientes = $this->model->where('nome','like','%'.$nome.'%')
              ->where('cpf','like','%'.$cpf.'%')
              ->paginate($totalPage);
        } else {
            $pacientes = $this->model->paginate($totalPage);    
        }
        return $pacientes;
    }

    public function buscarid($id) {
        return DB::select("SELECT * from pacientes where id = ?",[$id]);
    }

    public function deletar($id) {
        DB::delete('delete from convenio_pacientes where pacientes_id = ?',[$id]);
        DB::delete('delete from paciente_categorias where pacientes_id = ?',[$id]);
        return $this->model->destroy($id);
    }
    public function deletarRelacionamento($request){
        $paciente = $request->paciente;
        $convenio = $request->convenio;
        DB::delete('delete from convenio_pacientes where pacientes_id = ? and convenios_id = ?',[$paciente,$convenio]);
    }

    public function paginacao($totalPage)
    {
        return $this->model->paginate($totalPage);
    }

    public function salvar($request) {
        $pacientes = Paciente::create($request->all());
        //INSERINDO CATEGORIA E RESGATANDO ID CRIADO
         if (!empty($request->categoria)) {
              $qtCategoria = count($request->categoria);
              for($i=0; $i<$qtCategoria; $i++) {
                  DB::table('paciente_categorias')->insert([
                      'pacientes_id' => $pacientes->id,
                      'categorias_id' => $request->categoria[$i]
                  ]);
              }  
          }
       //FIM CATEGORIA
         //INSERINDO CONVENIO
         if (!empty($request->convenio)) {
          $qtConvenio = count($request->convenio);
          for($i=0; $i<$qtConvenio; $i++) {
              DB::table('convenio_pacientes')->insert([
                  'pacientes_id' => $pacientes->id,
                  'convenios_id' => $request->convenio[$i]
              ]);
          }  
      }
    }

    public function addselect2Convenio($paciente, $convenio) {
        $pacientesConvenio = [
            'pacientes_id' => $paciente,
            'convenios_id' => $convenio
        ];
         ConvenioPaciente::updateOrCreate($pacientesConvenio);    
        return $pacientesConvenio;
    }
    public function atualizar($id, $request){
        $pacientes = $this->model->find($id);
        return $pacientes->update($request->all());
    }

    public function select2Convenio($request, $paciente) {
        $listaFull = DB::select("SELECT convenios.id,convenios.nome FROM convenios WHERE status = 'ativo'");
        $listaSelect = DB::select("SELECT convenios.id,convenios.nome FROM convenios join convenio_pacientes on convenio_pacientes.convenios_id = convenios.id WHERE convenio_pacientes.pacientes_id = ?",[$paciente]);
        
        if((!empty($listaFull)) || (!empty($listaSelect))) {
           $convenios = [
            'convenioSelect' => $listaSelect,
            'convenioAll' => $listaFull
        ]; 

             return $convenios;
          } 
         
        
    }
    
  }