<?php
namespace App\Repositories;
use App\User;
use DB;
class UsuariosRepository 
{
	private $model;

	public function __construct(User $model)
	{
		$this->model = $model;
	}

	public function buscartodos()
	{
		return $this->model->all();
    }    
    
    public function pesquisar($email, $totalPage) {
        if (isset($email)) {
            $usuarios = DB::select("select grupos.nome as grupos_nome, users.* from users 
            join grupos on grupos.id = users.grupos_id where email like %'.$email.'%")
              ->paginate($totalPage);
        } else {
            $usuarios = $this->model->paginate($totalPage);    
        }
        return $usuarios;
    }

    public function buscarid($id) {
        return DB::select("SELECT * from users where id = ?",[$id]);
    }

    public function deletar($id) {
        return $this->model->destroy($id);
    }

    public function paginacao($totalPage)
    {
        return $this->model->paginate($totalPage);
    }

    public function salvar($request) {
        $usuarios = [
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'grupos_id' => $request->grupos_id
        ];
        return $this->model->create($usuarios);
    }

    public function update($id, $request) {
        if (!empty($request->password)) {
            $usuarios = [
                $request->email,
                bcrypt($request->password),
                $request->grupos_id,
                $id
            ];
            return DB::update("UPDATE users SET email = ?, password = ?, grupos_id =? WHERE id = ?", $usuarios);
        } else {
            $usuarios = [
                $request->email,
                $request->grupos_id,
                $id
            ];
            return DB::update("UPDATE users SET email = ?, grupos_id =? WHERE id = ?", $usuarios);
        }
        
       
    }
}