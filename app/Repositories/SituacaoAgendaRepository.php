<?php
namespace App\Repositories;
use App\SituacaoAgenda;
use DB;
class SituacaoAgendaRepository 

{
	private $model;

	public function __construct(SituacaoAgenda $model)
	{
		$this->model = $model;
	}

	public function buscartodos()
	{
		return $this->model->all();
    }    
    
    public function pesquisar($nome, $totalPage) {
        if (isset($nome)) {
            $situacaoagenda = $this->model->where('nome','like','%'.$nome.'%')
              ->paginate($totalPage);
        } else {
            $situacaoagenda = $this->model->paginate($totalPage);    
        }
        return $situacaoagenda;
    }

    public function buscarid($id) {
        return DB::select("SELECT * from situacao_agendas where id = ?",[$id]);
    }

    public function deletar($id) {
        return $this->model->destroy($id);
    }

    public function paginacao($totalPage)
    {
        return $this->model->paginate($totalPage);
    }

    public function salvar($request) {
        $situacaoagenda = [
            'nome' => $request->nome
        ];
        return $this->model->create($situacaoagenda);
    }

    public function update($id, $request) {
        $situacaoagenda = [
            $request->nome,
            $id
        ];
        return DB::update("UPDATE situacao_agendas SET nome = ? WHERE id = ?", $situacaoagenda);
    }
}