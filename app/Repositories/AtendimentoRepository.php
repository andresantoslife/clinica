<?php
namespace App\Repositories;
use App\Atendimento;
use App\Agenda;
use App\TipoAtendimento;
use App\ContaPaciente;
use App\ItensConta;
use DB;
class AtendimentoRepository 
{
	private $model;

	public function __construct(Atendimento $model)
	{
		$this->model = $model;
    }
    public function tipoatendimento(){
        $tiposdeatendimentos = DB::select('SELECT id, nome from tipo_atendimentos order by nome asc');
    return $tiposdeatendimentos;

    }
    public function procedimentos($agenda,$convenio){  
           /*  $unidade = DB::select('select 
            alocacaos.unidades_id as unidade
            from agendas
            join expedientes on expedientes.id = agendas.expedientes_id
            join alocacaos on alocacaos.id = expedientes.alocacaos_id
            where agendas.id = ?',[$agenda]);
            $unidade = $unidade[0]->unidade; */
                  
            $tabela = DB::select("select 
            convenios.tabela_faturamentos_id as tabelafaturamento
            from convenios
            WHERE convenios.id = ?",[$convenio]);
    
             $tabela = $tabela[0]->tabelafaturamento;
     
             $procedimentos = DB::select("select 
             procedimentos.id as id, procedimentos.procedimento as nome
             from valor_procedimentos
             join procedimentos on procedimentos.id = valor_procedimentos.procedimentos_id
             where valor_procedimentos.tabela_faturamentos_id = ?",[$tabela]); 
    return $procedimentos;         
    }

    public function convenio($id){
        $convenios = DB::select("select 
        convenios.id as id,
        convenios.nome as nome
        from 
        convenio_pacientes 
        join convenios on convenios.id = convenio_pacientes.convenios_id 
        where convenio_pacientes.pacientes_id = ?",[$id]);
    return $convenios;
    }

    public function consultaagenda($id) {
        $agenda = DB::select("SELECT 
        pacientes.id as id,
        agendas.id as agenda_id,
        DATE_FORMAT(agendas.dtagenda, '%H:%i') as hora,
        pacientes.nome as nome
        from agendas 
        join pacientes on pacientes.id = agendas.pacientes_id  
        where agendas.id = ?",[$id]);
    return $agenda;    
    }

	public function atender($request) {  
            $convenioID = $request->convenio_atender;
            $pacienteID = $request->paciente_id;
            $agendasAtender_id = $request->agendasAtender_id;
            $tempoespera =  date('Y-m-d H:i:s');
    
            $validadecarteirinha_atender = str_replace("/", "-", $request->validadecarteirinha_atender);
            $validadecarteirinha_atender = date('Y-m-d', strtotime($validadecarteirinha_atender));
          
            $convenio_pacientes = $unidade = DB::select('select 
            convenio_pacientes.id as id 
            from convenio_pacientes 
            where convenio_pacientes.convenios_id = ? 
            and convenio_pacientes.pacientes_id = ?',[$convenioID, $pacienteID]);
            $convenio_pacientes_id = $convenio_pacientes[0]->id;
          
            $alocacao = DB::select('select 
            alocacaos.id as id
            from agendas
            join expedientes on expedientes.id = agendas.expedientes_id
            join alocacaos on alocacaos.id = expedientes.alocacaos_id
            where agendas.id = ?',[$agendasAtender_id]);
            $alocacao = $alocacao[0]->id;
            
            $atendimentos = [       
                'tempoespera' => $tempoespera,    
                'carteirinha' => $request->carteirinha_atender,
                'validadecarteirinha' => $validadecarteirinha_atender,
                'senhacarteirinha' => $request->senhacarteirinha_atender,
                'nrguia' => $request->nrguia_atender,
                'status' => 'aguardando',
                'rn' => $request->rn_atender,
                'alocacaos_id' => $alocacao,
                'convenio_pacientes_id' => $convenio_pacientes_id,
                'tipo_atendimentos_id' => $request->tipoatendimento_atender,
                'indicacao_acidentes_id' => $request->indicacaoacidente_atender,
                'carater_atendimentos_id' => $request->carateratendimento_atender,
                'agendas_id' => $request->agendasAtender_id
            ];
            //var_dump($atendimentos);
            $atendimento = Atendimento::Create($atendimentos);  
            //ABRIR CONTA
            $idAgenda = $request->agendasAtender_id;
            $idAtendimentos = DB::select("select id from atendimentos where agendas_id = ?",[$idAgenda]);
            $idAtendimento = $idAtendimentos[0]->id;
            $contapacientes = [
                'pacientes_id' => $pacienteID,
                'dtfechamento' => null,
                'status' => 'aberta',
                'atendimentos_id' => $idAtendimento
            ];
            ContaPaciente::Create($contapacientes); 
            //FIM ABERTURA DE CONTA
            //INICIO CADASTRO DE ITENS
            $idcontas = DB::select("select id as idconta from conta_pacientes where atendimentos_id = ?",[$idAtendimento]);
            $idconta = $idcontas[0]->idconta;
            //LAÇO DE REPETIÇÂO DO PROCEDIMENTO
            $procedimentos = $request->procedimentos_atender;
            $idproc = $procedimentos[0];

            $tabela = DB::select("select 
            convenios.tabela_faturamentos_id as tabelafaturamento
            from convenios
            WHERE convenios.id = ?",[$convenioID]);
            $tabela = $tabela[0]->tabelafaturamento;
            
            foreach ($procedimentos as $procedimento) {
                $valorProcedimentos = DB::select('select id as idvalor from valor_procedimentos
                where tabela_faturamentos_id = ? and procedimentos_id =?',[$tabela, $procedimento]);
                $idvlr = $valorProcedimentos[0]->idvalor;
               
                $itens = [
                    'valor_procedimentos_id' => $idvlr,
                    'conta_pacientes_id' => $idconta
                ];
                ItensConta::Create($itens); 
         }

            //FIM CADASTRO DE ITENS
            //ATUALIZAR STATUS AGENDA 
            DB::update("UPDATE agendas SET situacao_agendas_id = '13' where id = ?", [$agendasAtender_id]);     
    }
}