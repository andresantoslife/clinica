<?php
namespace App\Repositories;
use App\TipoConsulta;
use DB;
class TipoConsultaRepository 
{
	private $model;

	public function __construct(TipoConsulta $model)
	{
		$this->model = $model;
	}

	public function buscartodos()
	{
		return $this->model->all();
    }    
    
    public function pesquisar($nome, $totalPage) {
        if (isset($nome)) {
            $tipoconsultas = $this->model->where('nome','like','%'.$nome.'%')
              ->paginate($totalPage);
        } else {
            $tipoconsultas = $this->model->paginate($totalPage);    
        }
        return $tipoconsultas;
    }

    public function buscarid($id) {
        return DB::select("SELECT * from tipo_consultas where id = ?",[$id]);
    }

    public function deletar($id) {
        return $this->model->destroy($id);
    }

    public function paginacao($totalPage)
    {
        return $this->model->paginate($totalPage);
    }

    public function salvar($request) {
        $tipoconsultas = [
            'nome' => $request->nome
        ];
        return $this->model->create($tipoconsultas);
    }

    public function update($id, $request) {
        $tipoconsultas = [
            $request->nome,
            $id
        ];
        return DB::update("UPDATE tipo_consultas SET nome = ? WHERE id = ?", $tipoconsultas);
    }
}