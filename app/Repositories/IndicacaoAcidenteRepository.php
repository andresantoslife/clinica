<?php
namespace App\Repositories;
use App\IndicacaoAcidente;
use DB;
class IndicacaoAcidenteRepository 
{
	private $model;

	public function __construct(IndicacaoAcidente $model)
	{
		$this->model = $model;
	}

	public function buscartodos()
	{
		return $this->model->all();
    }    
    
    public function pesquisar($nome, $totalPage) {
        if (isset($nome)) {
            $indicacaoacidentes = $this->model->where('nome','like','%'.$nome.'%')
              ->paginate($totalPage);
        } else {
            $indicacaoacidentes = $this->model->paginate($totalPage);    
        }
        return $indicacaoacidentes;
    }

    public function buscarid($id) {
        return DB::select("SELECT * from indicacao_acidentes where id = ?",[$id]);
    }

    public function deletar($id) {
        return $this->model->destroy($id);
    }

    public function paginacao($totalPage)
    {
        return $this->model->paginate($totalPage);
    }

    public function salvar($request) {
        $indicacaoacidentes = [
            'nome' => $request->nome
        ];
        return $this->model->create($indicacaoacidentes);
    }

    public function update($id, $request) {
        $indicacaoacidentes = [
            $request->nome,
            $id
        ];
        return DB::update("UPDATE indicacao_acidentes SET nome = ? WHERE id = ?", $indicacaoacidentes);
    }
}