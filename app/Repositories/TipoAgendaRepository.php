<?php
namespace App\Repositories;
use App\TipoAgenda;
use DB;
class TipoAgendaRepository 
{
	private $model;

	public function __construct(TipoAgenda $model)
	{
		$this->model = $model;
	}

	public function buscartodos()
	{
		return $this->model->all();
    }    
    
    public function pesquisar($nome, $totalPage) {
        if (isset($nome)) {
            $tipoagendas = $this->model->where('nome','like','%'.$nome.'%')
              ->paginate($totalPage);
        } else {
            $tipoagendas = $this->model->paginate($totalPage);    
        }
        return $tipoagendas;
    }

    public function buscarid($id) {
        return DB::select("SELECT * from tipo_agendas where id = ?",[$id]);
    }

    public function deletar($id) {
        return $this->model->destroy($id);
    }

    public function paginacao($totalPage)
    {
        return $this->model->paginate($totalPage);
    }

    public function salvar($request) {
        $tipoagendas = [
            'nome' => $request->nome
        ];
        return $this->model->create($tipoagendas);
    }

    public function update($id, $request) {
        $tipoagendas = [
            $request->nome,
            $id
        ];
        return DB::update("UPDATE tipo_agendas SET nome = ? WHERE id = ?", $tipoagendas);
    }
}