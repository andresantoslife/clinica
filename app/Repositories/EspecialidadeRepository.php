<?php
namespace App\Repositories;
use App\Especialidade;
use DB;
class EspecialidadeRepository 
{
	private $model;

	public function __construct(Especialidade $model)
	{
		$this->model = $model;
	}

	public function buscartodos()
	{
		return $this->model->all();
    }    
    
    public function pesquisar($codigo, $nome, $totalPage) {
        if ((isset($codigo)) || (isset($nome))) {
            $especialidades = $this->model->where('codigo','like','%'.$codigo.'%')
              ->where('nome','like','%'.$nome.'%')
              ->paginate($totalPage);
        } else {
            $especialidades = $this->model->paginate($totalPage);    
        }
        return $especialidades;
    }

    public function buscarid($id) {
        return DB::select("SELECT * from especialidades where id = ?",[$id]);
    }

    public function deletar($id) {
        return $this->model->destroy($id);
    }

    public function paginacao($totalPage)
    {
        return $this->model->paginate($totalPage);
    }

    public function salvar($request) {
        $especialidade = [
            'codigo' => $request->codigo,
            'nome' => $request->nome
        ];
        return $this->model->create($especialidade);
    }

    public function update($id, $request) {
        $especialidade = [
            $request->codigo,
            $request->nome,
            $id
        ];
        return DB::update("UPDATE especialidades SET codigo = ?, nome = ? WHERE id = ?", $especialidade);
    }
}