<?php
namespace App\Repositories;
use App\Prestador;
use DB;
class PrestadorRepository 
{
	private $model;

	public function __construct(Prestador $model)
	{
		$this->model = $model;
	}

	public function buscartodos()
	{
		return $this->model->all();
    }    
    
    public function pesquisar($nome, $cpf, $totalPage) {
        if ((isset($nome)) || (isset($cpf))) {
            $prestadores = $this->model->where('nome','like','%'.$nome.'%')
              ->where('cpf','like','%'.$cpf.'%')
              ->paginate($totalPage);
        } else {
            $prestadores = $this->model->paginate($totalPage);    
        }
        return $prestadores;
    }

    public function buscarid($id) {
        return DB::select("SELECT * from prestadors where id = ?",[$id]);
    }

    public function deletar($id) {
        return $this->model->destroy($id);
    }

    public function paginacao($totalPage)
    {
        return $this->model->paginate($totalPage);
    }

    public function salvar($request) {
        return $this->model->create($request->all());
    }

    public function atualizar($id, $request){
        $prestadores = $this->model->find($id);
        return $prestadores->update($request->all());
    }
    
  }