<?php
namespace App\Repositories;
use App\Expediente;
use DB;
use App\Alocacao;
use App\Agenda;
class ExpedienteRepository 
{
	private $model;

	public function __construct(Expediente $model)
	{
		$this->model = $model;
	}

    public function deletar($id) {
        return $this->model->destroy($id);
    }

    public function salvar($request) {
        $dtinicio = str_replace("/", "-", $request->dtinicio);
        $dtinicio = date('Y-m-d', strtotime($dtinicio));
        $dtfinal = str_replace("/", "-", $request->dtfinal);
        $dtfinal = date('Y-m-d', strtotime($dtfinal));
        $unidade = $request->unidade;
        $prestador = $request->prestador;
        $especialidade = $request->especialidade;
        
        $alocacaos = DB::table('alocacaos')
        ->where('unidades_id', '=', $unidade)
        ->where('prestadors_id', '=', $prestador)
        ->where('especialidades_id', '=', $especialidade)
        ->get();    

        $array = $alocacaos->toArray();
        $alocacao = $array[0]->id;

        $expediente = [
            'dtinicio' => $dtinicio,
            'dtfinal' => $dtfinal,
            'hinicio' => $request->hinicio,
            'hfinal' => $request->hfinal,
            'intervalo' => $request->intervalo,
            'semana' => $request->semana,
            'alocacaos_id' => $alocacao
        ];
        $this->model->updateOrCreate($expediente);  

        $ExpedientesSelect = DB::select("select id from expedientes order by created_at desc limit 0,1");
        //$Expedientearray = $ExpedientesSelect->toArray();
        $idExpediente =  $ExpedientesSelect[0]->id;
        //agendagenerator($ultimoid, $dtinicio, $dtfinal, $request->intervalo, $request->semana);
  
        /*
        Script v.02
        Valmeida@idea-planejamento.com
        URI /agendagenerator/dtinicio/dtfinal/intervalo/semana
        EXEMPLO: /agendagenerator/2020-02-19/2020-02-29/120/0
        DIASEMANA = array('Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sabado');
        SEMANAS: 0,1,2,3,4,5,6
        */
        //RECUPERA ID GERADO
        //-- Datas de Entrada
        $dateStart 	= new \DateTime($dtinicio);
        $dateEnd = new \DateTime($dtfinal);

        //-- Intervalo de Entrada
        $dateRanges = array();
         while($dateStart <= $dateEnd){
                $datatoLoop = $dateStart->format('Y-m-d'); 
                $dataFormatada = date("w", strtotime($datatoLoop));
                    if ($dataFormatada == $request->semana) {
                    $dateRanges[] = $dateStart->format('Y-m-d');
                        } 
                $dateStart = $dateStart->modify('+1day');
            }
            
        //--Trabalhando no LOOP de DATAS
        $dataehoraGeradas = [];
        foreach ($dateRanges as $dateRange) {
            $range=range(strtotime($request->hinicio),strtotime($request->hfinal),$request->intervalo*60);
                foreach($range as $time){
                    array_push($dataehoraGeradas,
                        array(
                            'data' => $dateRange,
                            'hora' => date("H:i",$time)
                            )
                    );       
                }
            } 
      //Tratar inserts no banco
      foreach ($dataehoraGeradas as $dataehoraGerada) {
        $dtagenda = $dataehoraGerada['data']." ".$dataehoraGerada['hora'];
        $agendas = [
            'dtagenda' => $dtagenda,
            'observacao' => '',
            'pacientes_id' => null,
            'expedientes_id' => $idExpediente,
            'situacao_agendas_id' => '4',
            'tipo_agendas_id' => null,
            'bloqueio_agendas_id' => null
        ];
            Agenda::updateOrCreate($agendas);   
       }

    } //Fim da function salvar

 } //FIM DA CLASSE