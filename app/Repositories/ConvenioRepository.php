<?php
namespace App\Repositories;
use App\Convenio;
use DB;
class ConvenioRepository 
{
	private $model;

	public function __construct(Convenio $model)
	{
		$this->model = $model;
	}

	public function buscartodos()
	{
		return $this->model->all();
    }    
    
    public function pesquisar($nome, $regans, $totalPage) {
        if ((isset($nome)) || (isset($regans))) {
            $convenios = $this->model->where('nome','like','%'.$nome.'%')
              ->where('regans','like','%'.$regans.'%')
              ->paginate($totalPage);
        } else {
            $convenios = $this->model->paginate($totalPage);    
        }
        return $convenios;
    }

    public function buscarid($id) {
        return DB::select("SELECT * from convenios where id = ?",[$id]);
    }

    public function deletar($id) {
        return $this->model->destroy($id);
    }

    public function paginacao($totalPage)
    {
        return $this->model->paginate($totalPage);
    }

    public function salvar($request) {
        $convenios = [
            'nome' => $request->nome,
            'regans' => $request->regans,
            'status' => $request->status,
            'tabela_faturamentos_id' => $request->tabela_faturamentos_id
        ];
        return $this->model->create($convenios);
    }

    public function update($id, $request) {
        $convenios = [
            $request->nome,
            $request->regans,
            $request->status,
            $request->tabela_faturamentos_id,
            $id
        ];
        return DB::update("UPDATE convenios SET nome = ?, regans = ?, status = ?, tabela_faturamentos_id = ? WHERE id = ?", $convenios);
    }
}