<?php
namespace App\Repositories;
use App\Alocacao;
use DB;
class AlocacaoRepository 
{
	private $model;

	public function __construct(Alocacao $model)
	{
    $this->model = $model;

	}

    public function deletar($id) {
    return $this->model->destroy($id);
    }

    public function salvar($request) {
        /*$especialidades = DB::table('alocacaos')
           ->select('especialidades_id')
           ->where('unidades_id','=',$request->unidade)
           ->where('prestadors_id','=',$request->prestador)->get();
        dd($especialidades);
        dd($request->especialidade);
        //var_dump($request->especialidade);
        //print_r(array_diff($lista1, $lista2));*/

          $especialidade = [
            'unidades_id' => $request->unidade,
            'prestadors_id' => $request->prestador,
            'especialidades_id' => $request->especialidade
        ];
         $this->model->updateOrCreate($especialidade);    
     
        
    } //Fim da function salvar
}