<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(GrupoTableSeeder::class); 
         $this->call(UsersTableSeeder::class);
         $this->call(ConselhosTableSeeder::class);
         $this->call(EspecialidadesTableSeeder::class);
         $this->call(GrupoProcedimentosTableSeeder::class);
         $this->call(ProcedimentosTableSeeder::class);
         $this->call(SituacaoAgendaTableSeeder::class);
         $this->call(TipoAtendimentoTableSeeder::class);
         $this->call(IndicacaoAcidenteTableSeeder::class);
         $this->call(TipoConsultaTableSeeder::class);
         $this->call(TabelaFaturamentoTableSeeder::class);
         $this->call(CaraterAtendimentoTableSeeder::class);
         $this->call(TipoExameTableSeeder::class);
        
    }
}
