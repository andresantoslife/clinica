<?php


use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;


class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $permissions = [
           'role-list',
           'role-create',
           'role-edit',
           'role-delete',
           'ag-agenda',
           'ag-expediente',
           'ag-situacao',
           'ag-tipo-bloqueio',
           'ag-tipo-agenda',
           'at-lista-espera',
           'at-prontuario',
           'at-tipo-atendimento',
           'at-carater-atendimento',
           'at-indicacao-acidente',
           'at-tipo-consulta',
           'fu-alocacao',
           'cad-unidades',
           'cad-prestadores',
           'cad-pacientes',
           'cad-especialidades',
           'cad-convenios',
           'cad-conselhos',
           'cad-categorias',
           'agenda',
           'atendimento',
           'funcionalidades',
           'cadastros'
        ];


        foreach ($permissions as $permission) {
             Permission::create(['name' => $permission]);
        }
    }
}