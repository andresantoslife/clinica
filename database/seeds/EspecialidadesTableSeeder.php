<?php
use App\Especialidade;
use Illuminate\Database\Seeder;

class EspecialidadesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $especialidades = [
            ['codigo' => '201115', 'nome' => 'Geneticista'],
            ['codigo' => '203015', 'nome' => 'Pesquisador em biologia de microorganismos e parasitas'],
            ['codigo' => '213150', 'nome' => 'Físico médico'],
            ['codigo' => '221105', 'nome' => 'Biólogo'],
            ['codigo' => '223505', 'nome' => 'Enfermeiro'],
            ['codigo' => '223605', 'nome' => 'Fisioterapeuta geral'],
            ['codigo' => '223710', 'nome' => 'Nutricionista'],
            ['codigo' => '223810', 'nome' => 'Fonoaudiólogo'],
            ['codigo' => '223905', 'nome' => 'Terapeuta ocupacional'],
            ['codigo' => '223910', 'nome' => 'Ortoptista'],
            ['codigo' => '225103', 'nome' => 'Médico infectologista'],
            ['codigo' => '225105', 'nome' => 'Médico acupunturista'],
            ['codigo' => '225106', 'nome' => 'Médico legista'],
            ['codigo' => '225109', 'nome' => 'Médico Nefrologista'],
            ['codigo' => '225110', 'nome' => 'Médico alergista e imunologista'],
            ['codigo' => '225112', 'nome' => 'Médico neurologista'],
            ['codigo' => '225115', 'nome' => 'Médico angiologista'],
            ['codigo' => '225118', 'nome' => 'Médico nutrologista'],
            ['codigo' => '225120', 'nome' => 'Médico cardiologista'],
            ['codigo' => '225121', 'nome' => 'Médico oncologista clínico'],
            ['codigo' => '225122', 'nome' => 'Médico cancerologista pediátrico'],
            ['codigo' => '225124', 'nome' => 'Médico pediatra'],
            ['codigo' => '225125', 'nome' => 'Médico clínico'],
            ['codigo' => '225127', 'nome' => 'Médico pneumologista'],
        ];
        foreach($especialidades as $especialidade){
            Especialidade::create($especialidade);
        }
       
        
    }
}
