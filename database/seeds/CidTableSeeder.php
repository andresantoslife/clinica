<?php

use Illuminate\Database\Seeder;
use App\Cid;
class CidTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cids = [
            ['id' => '1','codigo' => 'A000','descricao' => 'Cólera devida a Vibrio cholerae 01, biótipo cholerae'],
            
        ];
        foreach($cids as $cid){
            Cid::create($cid);
        }
    }
}
