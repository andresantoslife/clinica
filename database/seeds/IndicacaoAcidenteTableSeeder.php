<?php

use Illuminate\Database\Seeder;
use App\IndicacaoAcidente;  
class IndicacaoAcidenteTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $indicacaoacidentes = [
            ['id' => '1', 'nome' => 'Trânsito'],
            ['id' => '2', 'nome' => 'Outros Acidentes'],
            ['id' => '9', 'nome' => 'Nâo Acidentes'],
            ['id' => '10', 'nome' => 'Trabalho'],
        ];
        foreach($indicacaoacidentes as $indicacaoacidente){
            IndicacaoAcidente::create($indicacaoacidente);
        }
    }
}
