<?php

use Illuminate\Database\Seeder;
use App\SituacaoAgenda;
class SituacaoAgendaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
            $situacaoagendas = [
            ['id' => '1', 'nome' => 'Agendado'],
            ['id' => '2', 'nome' => 'Ausente'],
            ['id' => '3', 'nome' => 'Finalizado'],
            ['id' => '4', 'nome' => 'Livre'],
            ['id' => '5', 'nome' => 'Inativo'],
            ['id' => '6', 'nome' => 'Bloqueado'],
            ['id' => '7', 'nome' => 'Confirmado'],
            ['id' => '8', 'nome' => 'Em Atendimento'],
            ['id' => '9', 'nome' => 'Compromisso'],
            ['id' => '10', 'nome' => 'Ausencia Prolongada'],
            ['id' => '11', 'nome' => 'Férias'],
            ['id' => '12', 'nome' => 'Desligamento'],
            ['id' => '13', 'nome' => 'Fila de Espera'],
        ];
        foreach($situacaoagendas as $situacaoagenda){
            SituacaoAgenda::create($situacaoagenda);
        }
    }
}
