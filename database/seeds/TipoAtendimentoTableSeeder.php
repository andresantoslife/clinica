<?php

use Illuminate\Database\Seeder;
use App\TipoAtendimento;
class TipoAtendimentoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tipoatendimentos = [
            ['id' => '1', 'nome' => 'Remoção'],
            ['id' => '2', 'nome' => 'Pequena Cirurgia'],
            ['id' => '3', 'nome' => 'Terapias'],
            ['id' => '4', 'nome' => 'Consulta'],
            ['id' => '5', 'nome' => 'Exames (englobando exame radiológico)'],
            ['id' => '6', 'nome' => 'Atendimento Domiciliar'],
            ['id' => '7', 'nome' => 'Internação'],
            ['id' => '8', 'nome' => 'Quimioterapia'],
            ['id' => '9', 'nome' => 'Radioterapia'],
            ['id' => '10', 'nome' => 'Terapia Renal Substitutiva (TRS)'],
            ['id' => '11', 'nome' => 'Pronto Socorro'],
            ['id' => '12', 'nome' => 'Ocupacional'],
            ['id' => '13', 'nome' => 'Pequenos atendimentos'],
            ['id' => '14', 'nome' => 'Admissional'],
            ['id' => '15', 'nome' => 'Demissional'],
            ['id' => '16', 'nome' => 'Periódico'],
            ['id' => '17', 'nome' => 'Retorno ao trabalho'],
            ['id' => '18', 'nome' => 'Mudança de função'],

        ];
        foreach($tipoatendimentos as $tipoatendimento){
            TipoAtendimento::create($tipoatendimento);
        }
    }
}
