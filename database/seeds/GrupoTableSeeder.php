<?php

use Illuminate\Database\Seeder;
use App\Grupo;

class GrupoTableSeeder extends Seeder
{ 
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Grupo::create([ 
            'nome' => 'Administrador'
        ]); 
        Grupo::create([
            'nome' => 'Médico'
        ]);
        Grupo::create([
            'nome' => 'Recepção'
        ]);
        Grupo::create([
            'nome' => 'Faturamento'
        ]);
       
    }
}
