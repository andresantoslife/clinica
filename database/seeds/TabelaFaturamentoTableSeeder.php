<?php

use Illuminate\Database\Seeder;
use App\TabelaFaturamento;
class TabelaFaturamentoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tabelaFaturamentos = [
            ['id' => '1', 'nome' => 'Particular'],
            ['id' => '2', 'nome' => 'AMB92']
        ];
        foreach($tabelaFaturamentos as $tabelaFaturamento){
            TabelaFaturamento::create($tabelaFaturamento);
        }
    }
}
