<?php

use Illuminate\Database\Seeder;
use App\CaraterAtendimento;
class CaraterAtendimentoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $carateratendimentos = [
            ['id' => '1', 'nome' => 'Eletiva'],
            ['id' => '2', 'nome' => 'Urgência/Emergência'],
        ];
        foreach($carateratendimentos as $carateratendimento){
            CaraterAtendimento::create($carateratendimento);
        }
    }
}
