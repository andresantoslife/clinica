<?php

use Illuminate\Database\Seeder;
use App\TipoExame;
class TipoExameTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tipoexames = [
            ['id' => '1', 'nome' => 'Laboratoriais'],
            ['id' => '2', 'nome' => 'Imagens'],
            ['id' => '3', 'nome' => 'Gráficos']
        ];
        foreach($tipoexames as $tipoexame){
            TipoExame::create($tipoexame);
        }
    }
}
