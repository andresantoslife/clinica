<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAtendimentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('atendimentos', function (Blueprint $table) {
            $table->bigIncrements('id');            
            $table->datetime('inicioatendimento')->nullable();
            $table->datetime('tempoespera')->nullable();
            $table->datetime('fimatendimento')->nullable();
            $table->string('carteirinha')->nullable();
            $table->date('validadecarteirinha')->nullable();
            $table->string('senhacarteirinha')->nullable();
            $table->string('nrguia')->nullable();
            $table->enum('status', ['aberto', 'aguardando', 'pausado', 'finalizado'])->nullable();
            $table->enum('rn', ['sim', 'nao'])->nullable();
            //CHAVE ESTRANGEIRAS
            $table->unsignedBigInteger('alocacaos_id')->nullable();
            $table->foreign('alocacaos_id')->references('id')->on('alocacaos');
            $table->unsignedBigInteger('convenio_pacientes_id')->nullable();
            $table->foreign('convenio_pacientes_id')->references('id')->on('convenio_pacientes');   
            $table->unsignedBigInteger('tipo_atendimentos_id')->nullable();
            $table->foreign('tipo_atendimentos_id')->references('id')->on('tipo_atendimentos');
            $table->unsignedBigInteger('indicacao_acidentes_id')->nullable();
            $table->foreign('indicacao_acidentes_id')->references('id')->on('indicacao_acidentes');
            $table->unsignedBigInteger('carater_atendimentos_id')->nullable();
            $table->foreign('carater_atendimentos_id')->references('id')->on('carater_atendimentos');
            $table->unsignedBigInteger('tipo_consultas_id')->nullable();
            $table->foreign('tipo_consultas_id')->references('id')->on('tipo_consultas');
            $table->unsignedBigInteger('agendas_id')->nullable();
            $table->foreign('agendas_id')->references('id')->on('agendas');     
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('atendimentos');
    }
}
