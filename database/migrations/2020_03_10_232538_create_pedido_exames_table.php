<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePedidoexamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pedido_exames', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->String('observacao')->nullable();
            $table->Date('dataExame')->nullable();
            $table->unsignedBigInteger('exames_id')->nullable();
            $table->foreign('exames_id')->references('id')->on('exames');
            $table->unsignedBigInteger('prestadors_id')->nullable();
            $table->foreign('prestadors_id')->references('id')->on('prestadors');
            $table->unsignedBigInteger('atendimentos_id')->nullable();
            $table->foreign('atendimentos_id')->references('id')->on('atendimentos');
            $table->unsignedBigInteger('pacientes_id')->nullable();
            $table->foreign('pacientes_id')->references('id')->on('pacientes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pedido_exames');
    }
}
