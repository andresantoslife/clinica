<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgendasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agendas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->datetime('dtagenda');
            $table->longText('observacao')->nullable();;
            //Chave Estrangeiras
            $table->unsignedBigInteger('pacientes_id')->nullable();
            $table->foreign('pacientes_id')->references('id')->on('pacientes');
            $table->unsignedBigInteger('expedientes_id')->nullable();
            $table->foreign('expedientes_id')->references('id')->on('expedientes'); 
            $table->unsignedBigInteger('situacao_agendas_id')->nullable();
            $table->foreign('situacao_agendas_id')->references('id')->on('situacao_agendas'); 
            $table->unsignedBigInteger('tipo_agendas_id')->nullable();
            $table->foreign('tipo_agendas_id')->references('id')->on('tipo_agendas');
            $table->unsignedBigInteger('bloqueio_agendas_id')->nullable();
            $table->foreign('bloqueio_agendas_id')->references('id')->on('bloqueio_agendas');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agendas');
    }
}
