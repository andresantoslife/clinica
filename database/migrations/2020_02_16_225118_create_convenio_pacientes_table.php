<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConvenioPacientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('convenio_pacientes', function (Blueprint $table) {
        $table->bigIncrements('id');
        $table->unsignedBigInteger('convenios_id');
            $table->foreign('convenios_id')->references('id')->on('convenios');  
        $table->unsignedBigInteger('pacientes_id');
            $table->foreign('pacientes_id')->references('id')->on('pacientes'); 
        $table->string('carteirinha')->nullable();
        $table->date('validade')->nullable();
        $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('convenio_pacientes');
    }
}
