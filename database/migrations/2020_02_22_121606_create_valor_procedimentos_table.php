<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateValorProcedimentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('valor_procedimentos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->float('valor', 8, 2);
            $table->unsignedBigInteger('tabela_faturamentos_id')->nullable();
            $table->foreign('tabela_faturamentos_id')->references('id')->on('tabela_faturamentos');
            $table->unsignedBigInteger('procedimentos_id')->nullable();
            $table->foreign('procedimentos_id')->references('id')->on('procedimentos');
            $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('valor_procedimentos');
    }
}
