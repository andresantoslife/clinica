<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrestadorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prestadors', function (Blueprint $table) {
            //Dados Pessoais
            $table->bigIncrements('id');
            $table->string('nome');
            $table->string('cpf');
            $table->string('rg');
            $table->date('dtnascimento');
            //Endereço   
            $table->string('cep')->nullable();
            $table->string('logradouro')->nullable();
            $table->string('numero')->nullable();
            $table->string('bairro')->nullable();
            $table->string('cidade')->nullable();
            $table->string('complemento')->nullable();
            $table->string('uf')->nullable();
            //Contatos
            $table->string('telefone')->nullable();
            $table->string('celular')->nullable();
            //Dados Essenciais 
            $table->unsignedBigInteger('conselhos_id');
              $table->foreign('conselhos_id')->references('id')->on('conselhos');
            $table->string('nrconselho');    
            $table->unsignedBigInteger('users_id');
              $table->foreign('users_id')->references('id')->on('users');             
            $table->enum('status', ['ativo', 'inativo']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prestadors');
    }
}
