<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItensContasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('itens_contas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('valor_procedimentos_id');
            $table->foreign('valor_procedimentos_id')->references('id')->on('valor_procedimentos');
            $table->unsignedBigInteger('conta_pacientes_id');
            $table->foreign('conta_pacientes_id')->references('id')->on('conta_pacientes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('itens_contas');
    }
}
