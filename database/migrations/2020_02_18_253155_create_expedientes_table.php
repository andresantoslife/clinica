<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExpedientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expedientes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('dtinicio');
            $table->date('dtfinal');
            $table->time('hinicio');
            $table->time('hfinal');
            $table->integer('intervalo');
            $table->string('semana');
            $table->unsignedBigInteger('alocacaos_id');
            $table->foreign('alocacaos_id')->references('id')->on('alocacaos');     
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expedientes');
    }
}
