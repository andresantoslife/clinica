<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBloqueioAgendasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bloqueio_agendas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('dtinicio');
            $table->date('dtfinal');
            $table->time('hinicio');
            $table->time('hfinal');
            $table->string('semana');
            $table->longText('justificativa')->nullable();
            //Chave Estrangeiras
            $table->unsignedBigInteger('alocacaos_id');
            $table->foreign('alocacaos_id')->references('id')->on('alocacaos');
            $table->unsignedBigInteger('tipo_bloqueios_id');
            $table->foreign('tipo_bloqueios_id')->references('id')->on('tipo_bloqueios');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bloqueio_agendas');
    }
}
