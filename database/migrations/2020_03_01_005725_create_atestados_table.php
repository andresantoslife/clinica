<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAtestadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('atestados', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('tipo')->nullable();
            $table->date('data')->nullable();
            $table->time('hinicio')->nullable();
            $table->time('hfim')->nullable();
            $table->string('observacao')->nullable();
            $table->unsignedBigInteger('cids_id')->nullable();
            $table->foreign('cids_id')->references('id')->on('cids')->nullable();;
            $table->unsignedBigInteger('atendimentos_id')->nullable();
            $table->foreign('atendimentos_id')->references('id')->on('atendimentos');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('atestados');
    }
}
