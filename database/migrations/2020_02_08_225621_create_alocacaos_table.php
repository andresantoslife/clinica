<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlocacaosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alocacaos', function (Blueprint $table) {
        $table->bigIncrements('id');
        $table->unsignedBigInteger('unidades_id');
            $table->foreign('unidades_id')->references('id')->on('unidades');  
        $table->unsignedBigInteger('prestadors_id');
            $table->foreign('prestadors_id')->references('id')->on('prestadors'); 
        $table->unsignedBigInteger('especialidades_id');
            $table->foreign('especialidades_id')->references('id')->on('especialidades');     
        $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alocacaos');
    }
}
