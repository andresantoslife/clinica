<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConveniosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('convenios', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nome');
            $table->string('regans')->nullable();
            $table->enum('status', ['ativo', 'inativo']);
            $table->unsignedBigInteger('tabela_faturamentos_id');
            $table->foreign('tabela_faturamentos_id')->references('id')->on('tabela_faturamentos');     
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('convenios');
    }
}
